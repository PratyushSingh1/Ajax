var showHide = "false";
function chartsDataFun(chartsData){
	chartsData.forEach(function(d){
		frameChartsDefult(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9]);
		})	
}
function slideMe(divid, imgid, imgtitle) {
	if (document.getElementById(divid).style.display == ""
			|| document.getElementById(divid).style.display == "block"){
		$("#" + imgid).attr("src", "images/" + imgtitle + "_close_arrow.png");
		$("#" + divid).slideUp('slow');
	} else {
		$("#" + imgid).attr("src", "images/" + imgtitle + "_open_arrow.png");
		$("#" + divid).slideDown('slow');

	}
}

function export2Image() {
	 var img = $('#' +'chart1').jqplotToImage(20, 20);
		if (img) { 
			 var imageContent = img.toDataURL("image/png");
			 var realpath = "";
			$.ajax({
               type: "POST",
               url : 'imgDownload.action',
               cache: false,
               timeout: 60000,
               processData: false,
               data: "imageContent=" + unescape(encodeURIComponent(imageContent)),
               dataType: "text",
               success: function(result) {
                   window.location.href="imagedownload.jsp";
                   //alert("Success");
               },
               error: function (data, status, e)
               {
                   if(status == "timeout"){
                	   bootbox.alert({ 
       					title: 'Alert',
       		    	    size: 'small',
       		    	    message: "timeout", 
       		    	});
                   }else{
                       alert(status);
                   }
               }
       }); 
			
		}
	}


function frameBarChartForDrillDown(id,color)
	 {
		 		
			showbarsimplefordrilldown(id,color);
			
	 }
	 
	 
	 function showbarsimplefordrilldown(id,color)
	 {
		
		 var c = document.getElementById(id); 
		 while (c.firstChild) { //$('#'+id).siblings().css("display","none");
		$('#'+id).attr("class", "");
			c.removeChild(c.firstChild); }
		var yval=new Array();
		var xval=new Array();
		var str=new Array();
		var keys=new Array();
		keys[0]="";
		var jsonstring = $('#data_'+id).val();
	str=jsonstring.split(",");
		for(i in str)
		  {
		  
		  
		  str[i]= str[i].substring(str[i].indexOf("{")+1,str[i].lastIndexOf("}"));
		
		  xval[i]=str[i].split(":")[0].trim();
		 yval[i]=parseFloat(str[i].split(":")[1]);

		
		// alert("Array[" + i + " ]= " + yval[i]);
		  }
		var barchartdata=[];
		var k=0;
		// alert(values[i])
		// alert("keys"+keys[i]+" "+"values"+values[i]);
		for (var i=0; i < keys.length;i++ ) {
		    barchartdata.push({
		      key: keys[i],
		      values: []
		    });

		    for (var j=0; j < xval.length;j++) {

	// alert("xval length"+xval.length+"data"+xval[j]);
			
		    	xval[j] = xval[j].replace(/amp;/g, '');
		      barchartdata[i].values.push({
		        label: xval[j],
				value:yval[k]
		     
		     // , size: Math.random()
		// shape: shapes[j % 6]
		      });
			  k++;

		    }
			
		  }
		 var container= d3.select('#'+id).append("svg").style('height','440px');
		  var wrap = container.selectAll('g.nv-wrap.nv-discretebar');
		  var groups = wrap.select('.nv-groups').selectAll('.nv-group');
		 var bars = groups.selectAll('g.nv-bar');
		// alert(id);
		nv.addGraph(function() {  
			  var chart = nv.models.discreteBarChart()
			      .x(function(d) { return d.label; })
			      .y(function(d) { return d.value; })
			      .staggerLabels(true)
			     // .staggerLabels(barchartdata[0].values.length > 8)
			      .tooltips(true)
			      .showValues(true);
			 
			 

			  d3.select('#'+id).append("svg").style('height','440px')
			      .datum(barchartdata)
			    .transition().duration(500)
			      .call(chart)
			      	   bars.style('fill', color);

			  nv.utils.windowResize(chart.update);
			  return chart;
			});
	}	

	 
	 
	 function updateBarChart(group, colorChosen) {
			//alert("group"+group);
			var dsblayoutId = document.getElementById('dsblayoutId').value;
			var color = "red";
			//alert("Action?&drillTime=" + group + "&chartid=" + chartId+ "&drillValue=" + colorChosen);
			/* $.post("dataDrillDown.action?&selgroup=" + group + "&dsblayoutId="+ dsblayoutId, "&color=" + colorChosen, function(result) {
				document.getElementById('DivId_ChartDisplay').innerHTML = result;
				$.blockUI({
					message : $('#DivId_ChartDisplay'),
					css : {
						top : ($(window).height() - 900) / 3 + 'px',
						left : ($(window).width() - 400) / 3 + 'px',
						border : 'none',
						width : '0px',
						height : '0px'
					}
				});

			}); */
		}

		
	 function drillDownMultiBar(key, group,chartId) {
			var dsblayoutId = document.getElementById('dsblayoutId').value;
			//This is for separating number number from text
			chartId = parseInt(chartId.replace(/[^0-9\.]/g, ''), 10);
			//group = drillTime
			//key = drillValue
			//alert("Action?&drillTime=" + group + "&chartid=" + chartId+ "&drillValue=" + key);
			//$.post("dataDrillDown.action?&selgroup=" + group + "&dsblayoutId="+ dsblayoutId+ "&KeyElem=" + key,function(result) {
			var year = group.substr(group.lastIndexOf('-')+1).trim();
			if(isNaN(year)){
				//alert("Not a time column....");
			}else{
				if (group.indexOf('From') > -1) {
					//alert("drillTime Contains From");
				}else if(group.indexOf('To') > -1){
					//alert("drillTime Contains to");
				}else{
					jQuery("body").mask("");$.post("dataDrillDown.action?&drillTime=" + group + "&chartId=" + chartId+ "&drillValue=" + key, function(result) {
					
					document.getElementById('DivId_ChartDisplay').innerHTML = result;
					jQuery("body").unmask();
					$.blockUI({
						message : $('#DivId_ChartDisplay'),
						css : {
							top : ($(window).height() - 900) / 3 + 'px',
							left : ($(window).width() - 400) / 3 + 'px',
							border : 'none',
							width : '0px',
							height : '0px'
							}
						});
					});  
			}
		}
	 }
	 
	 function drillDownSingleBar( drillName,chartId) {
		 
			
			var drill = document.getElementById("drillValue1");
			var checkTime = checkTimeValue(drillName);
			//var year = drillName.substr(drillName.lastIndexOf('-')+1).trim();
			var action;
			/*if(checkTime == false){
				//alert("Not a time column....");
			}else{*/
				jQuery("body").mask("");
				if(drill == 'undefined' || drill == null){
					action ="dataDrillDownSimple.action?&drillTime=" + drillName + "&chartId=" + chartId;
				}else{
					drill = document.getElementById("drillValue1").value;
					action ="dataDrillDownSimple.action?&drillTime=" + drillName + "&chartId=" + chartId +"&drillValue="+drill;
				}
				//alert("Action : "+action);
				$.post(action, function(result) {
				document.getElementById('DivId_ChartDisplay').innerHTML = result;
				jQuery("body").unmask();
				$.blockUI({
					message : $('#DivId_ChartDisplay'),
					css : {
						top : ($(window).height() - 900) / 3 + 'px',
						left : ($(window).width() - 400) / 3 + 'px',
						border : 'none',
						width : '0px',
						height : '0px'
						}
					});
				}); 
			/*}*/
	 }
	 
	 
			//code for area chart

	function showAreaChart1(id) {
		//alert("area charty");
		

			nv.addGraph(function() {
			  var chart =nv.models.lineChart();


		chart.x(function(d,i) { return i; });
		chart.xAxis.tickFormat(function(d, i){
		     return xval[d]; //"Year1 Year2, etc depending on the tick value - 0,1,2,3,4"
		})
			  chart.yAxis
			      .axisLabel('Voltage (v)')
			      .tickFormat(d3.format(',.2f'));
			  
			  d3.select('#'+id).append("svg").style('height','440px')
			      .datum(Areachart())
			 	  .transition().duration(500)
			      .call(chart);
			  
			  nv.utils.windowResize(chart.update);
			
			  return chart;
				});

	}

	
	
	
	function  showFusionLine(divid,xname,yname,layoutchoice,userType)
	{
	//alert("fusion stacked line");
		var jsonstring = $('#data_'+divid).val();
		/*var state = document.getElementById(divid).style.display;
		if (state == 'none') {
            document.getElementById(divid).style.display = 'block';
        }*/
		$('#'+id).css('display','block');
		widthValue = getWidthValue(layoutchoice,userType);
		var yvalarea=new Array();
		
		var str=new Array();
		var xval=new Array();
		var earea = new Array();
		var arrarea = new Array();
		var testarea=new Array();
		
		
		var strxml="<graph  xAxisName="+"'"+xname+"'"+"  yAxisName="+"'"+yname+"'"+" yAxisMaxValue='1' showValues='0'>";
		//alert("strxml"+strxml);
		strxml+="<categories>";
		
		var xvalarea=new Array();
		
		var t=jsonstring.split("],");
 		
 		
		 var  f=t[0].split("~,");

		 for(var i=0;i<f.length;i++)
		 {

		xvalarea[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));
		//alert("categories"+xvalarea[i]);
		strxml+="<category name="+"'"+xvalarea[i]+"'"+"/>";
		 }
        strxml+="</categories>";
		 var k = 0;
		 for(var i=0;i<t.length;i++)
		 {

		  testarea[i]=t[i].substring(t[i].indexOf("{"),t[i].lastIndexOf("}")+1);
		 //alert("testing i::::::::"+test[i] );
		 earea[i] = t[i].substring(0,t[i].indexOf("["));
		 //alert(e[i]);

		 //e[i] = test[i];
		 //alert("eeeeeeeeee"+e[i]);
		 var x = testarea[i].split(",");
		 //alert("X LENGTH :"+x.length);
		     for(var j=0;j<x.length;j++){

		        arrarea[k++] = x[j];
		 	}

		 }

		 var month = "";
		 var day="";
		 var b = new Array();
		 for(var n=0; n<arrarea.length;n++){

		   
		 	b[n] =parseFloat( arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}")));
		 	
		 	
		 	
		    
		 }
		 var k=0;
		 
		 for (i = 0; i < earea.length;i++ ) {
			  
                 strxml+="<dataset seriesName="+"'"+earea[i]+"'"+" color="+"'"+get_random_color()+"'"+">";
			    for (j = 0; j < xvalarea.length;j++) {

	                   
				
strxml+="<set value="+"'"+b[k]+"'"+"/>";
k++;
			     
			    }
			    strxml+="</dataset>"; 
				
			  }
		 strxml+="</graph>";
		
		// alert("xml after final"+strxml);
		//var strxmls="<graph ><categories><category name='08/01' /><category name='08/02' /></categories> <dataset seriesname='Product A'><set value='36634' /> <set value='43653' /> <set value='55565' /> </dataset><dataset seriesname='Product B'><set value='12152' /> <set value='15349' /> <set value='16442' /> </dataset></graph>";
		// var strxmls="graph yAxisMaxValue='1'<categories><category name='08/01' /> <category name='08/02' /> </categories><dataset  seriesname='ProductA'><set value='36634' /> <set value='43653' /> <set value='55565' /> </dataset><dataset  seriesname='Product B' ><set value='12152' /> <set value='15349' /> <set value='16442' /> </dataset> </graph>";
		 if(layoutchoice==1)
		  {
	 
	  var chart= new FusionCharts("plugins/Charts/FCF_MSLine.swf", "mychart",widthValue, "350", "0", "0");
	 
		  }
	  
	  if(layoutchoice==2)
	  {
  
	var chart= new FusionCharts("plugins/Charts/FCF_MSLine.swf", "mychart", widthValue,"360", "0", "0");

	  }
	  
	  if(layoutchoice==3)
	  {

	var chart= new FusionCharts("plugins/Charts/FCF_MSLine.swf", "mychart",widthValue, "360", "0", "0");

	  }
	  chart.setDataXML(strxml);
      chart.setTransparent(true);
      
      chart.render(divid);
		 
	}
	
	
	

		

		function framelineChart(id,divid,xname,yname,layoutchoice,chartid,updateType,userType) {
			var currentDiv=$('div[id^='+id+']');
			var jsonstring="" ;
			id=currentDiv.attr('id').split('@')[0];
			var currentlevel=currentDiv.attr('id').split('@')[1];
			
			if(currentlevel!=null && currentlevel!='undefined'){
				
				if($('input[id^='+chartid+']'+'[id*='+currentlevel+']').length>0)
					{
					jsonstring=	$('input[id^='+chartid+']'+'[id*='+currentlevel+']').val();
					}else{
					jsonstring=	$('#data_divid'+chartid).val();
					}
			}else{
				jsonstring= $('#data_'+id).val();
			}
			currentDiv.empty();
			if(showHide == "true"){
				bootbox.alert({ 
   					title: 'Alert',
   		    	    size: 'small',
   		    	    message: "Please Maximize", 
   		    	});
			}else{
/*			var jsonstring = $('#data_'+id).val();*/
			var yvalarea=new Array();
			var str=new Array();
			var xval=new Array();
			var earea = new Array();
			var arrarea = new Array();
			var testarea=new Array();
			var xvalarea=new Array();
			var brwsrSupport = getInternetExplorerVersion();

			if (brwsrSupport=='fus') 
			{
				widthValue = getWidthValue(layoutchoice,userType);
				if((jsonstring=="NA1")||(jsonstring=="NA2") || (jsonstring == 'error'))
				{
					bootbox.alert({ 
	   					title: 'Alert',
	   		    	    size: 'small',
	   		    	    message: "chart can't be found for empty data", 
	   		    	});
				}
				else
					{
					if(updateType)
					{
					updateChartType(chartid,6);
					}
				//var state = document.getElementById(divid).style.display;
			var type=jsonstring.charAt(jsonstring.length-1);
			 if(type==2)
				 {
				 showFusionLine(divid,jsonstring,xname,yname,layoutchoice,userType);
				 }
			 else if(type==1)
			 {
	               
				 /*if (state == 'none') {
			            document.getElementById(divid).style.display = 'block';
			        } */
				 $('div[id^='+id+']').css('display','block');
			var yval=new Array();
			 var xval=new Array();
			 var str= jsonstring.split("~,");
			// alert("before"+str[0]);
			for(i in str)
	    	  {
	    	  //alert("string functions");
	    	  
	    	  str[i]= str[i].substring(str[i].indexOf("{")+1,str[i].lastIndexOf("}"));
	    	  //alert("string is"+str[i]);
	    	
	    	  xval[i]=str[i].split(":")[0];
	    	 
	    	 yval[i]=parseFloat(str[i].split(":")[1]);
	    	
	    	
	    	  }
			 
			var strxml="<graph  xAxisName="+"'"+xname+"'"+"  yAxisName="+"'"+yname+"'"+" yAxisMaxValue='1' showValues='0'>";
			for(var j=0;j<xval.length;j++)
				{
				strxml+="<set name="+"'"+xval[j]+"'"+" "+"value="+"'"+yval[j]+"'"+" "+"color='AFD8F8'/>";
				}
			var xml=strxml+"</graph>";
			 //var state = document.getElementById(id).style.display;
			
			 if(layoutchoice==1)
			  {
		 
		  var chart= new FusionCharts("plugins/Charts/FCF_Line.swf", "mychart",widthValue, "350", "0", "0");
		 
			  }
		  
		  if(layoutchoice==2)
		  {
	  //  alert("second layout");
		var chart= new FusionCharts("plugins/Charts/FCF_Line.swf", "mychart",widthValue,"360", "0", "0");

		  }
		  
		  if(layoutchoice==3)
		  {

		var chart= new FusionCharts("plugins/Charts/FCF_Line.swf", "mychart",widthValue, "360", "0", "0");

		  }
			      //document.getElementById(id).style.display = 'block';
			      var strXML = "<graph caption='Hours worked' showNames='1'><set name='Tom' value='32' color='AFD8F8'/><set name='Mary' value='16' color='F6BD0F'/><set name='Jane' value='42' color='8BBA00'/></graph>";
			    //  var chart= new FusionCharts("plugins/Charts/FCF_Area2D.swf", "chart1Id", "850", "350", "0", "0");
			      chart.setDataXML(xml);
			      chart.setTransparent(true);
			      
			      chart.render(divid);
			}
			
				}
				}
			else
				{
				/*var state = document.getElementById(id).style.display;
			        if (state == 'none') {
			            document.getElementById(id).style.display = 'block';		           
			        }*/
				$('div[id^='+id+']').css('display','block');
				if((jsonstring=="NA1")||(jsonstring=="NA2") || (jsonstring == 'error'))
				{
					bootbox.alert({ 
	   					title: 'Alert',
	   		    	    size: 'small',
	   		    	    message: "chart can't be found for empty data", 
	   		    	});
				}
				else
					{
					if(updateType)
					{
					updateChartType(chartid,6);
					}
				/*var state = document.getElementById(id).style.display;*/
				/*$('div[id^='+id+']').show()*/
					var state =$('div[id^='+id+']').attr("style", "display:block;");
		        if (state == 'none') {
		        	document.getElementById(id).style.display = 'block';
		        }
				if(jsonstring.length > 2){
					var type = jsonstring.charAt(jsonstring.length - 1);
					var yvalarea = new Array();
					var str = new Array();
					var xval = new Array();
					var earea = new Array();
					var arrarea = new Array();
					var testarea = new Array();
					var xvalarea = new Array();
					var colors = new Array('#ff7f0e','#2ca02c','#000080','#808000','#800080','#FF0000','#C0C0C0','#008080','#FFFFFF','#FFFF00');
					var c = $('div[id^='+id+']');
					while (c.firstChild) {
						$('div[id^='+id+']').attr("class", "");
						c.removeChild(c.firstChild);
					}
					var t = jsonstring.split("],");
					var f = t[0].split("~,");
					for ( var i = 0; i < f.length; i++) {
						xvalarea[i] = f[i].substring(f[i].indexOf("{") + 1, f[i]
								.indexOf(":"));
					}
					var k = 0;
					for ( var i = 0; i < t.length; i++) {
						testarea[i] = t[i].substring(t[i].indexOf("{"), t[i]
								.lastIndexOf("}") + 1);
						earea[i] = t[i].substring(0, t[i].indexOf("["));
						if(earea[i]=="")
						 {
						 earea[i]=yname;
						 }
						var x = testarea[i].split(",");
						for ( var j = 0; j < x.length; j++) {
							arrarea[k++] = x[j];
						}
					}
			         var yvalorig=new Array();
					var month = "";
					var day = "";
					var b = new Array();
					for ( var n = 0; n < arrarea.length; n++) {
						b[n] = parseFloat(arrarea[n].substring(arrarea[n].indexOf(":") + 1,
								arrarea[n].indexOf("}")));
						yvalorig[n]=arrarea[n].substring(arrarea[n].indexOf(":") + 1,
								arrarea[n].indexOf("}"));
					}
					var maxlen = 0;
					for (i=0; i<yvalorig.length; i++) {
					  if (yvalorig[i].length>maxlen) {
					    maxlen = yvalorig[i].length;
					  }
					}
					if(maxlen>=13)
						{
						var left=130;
						}
					else
						left=90;
					
					var data = [];
					var k = 0;
					for (i = 0; i < earea.length; i++) {
						data.push({
							key : earea[i],
							values : [],
							color : colors[i]
						});
						for (j = 0; j < xvalarea.length; j++) {
							xvalarea[j] = xvalarea[j].replace(/amp;/g, '');
							if(isNaN(b[k]))
								b[k]=0;
							data[i].values.push({
								x : xvalarea[j],
								y : parseFloat(b[k])
							});
							k++;
						}
					}
					if(layoutchoice!=0)
						{
						 if(type == 3)
						   {
						    var yvalarea=new Array();
						    var str=new Array();
						    var xval=new Array();
						    var earea = new Array();
						    var arrarea = new Array();
						    var testarea=new Array();
						    var xvalarea=new Array();
						     if((jsonstring=="NA1")||(jsonstring=="NA2")|| (jsonstring == 'error'))
						     {
						    	 bootbox.alert({ 
					   					title: 'Alert',
					   		    	    size: 'small',
					   		    	    message: "chart can't be found for empty data", 
					   		    	});
						     }
						     else
						      {
						      
						     /*var state = document.getElementById(id).style.display;
						     if (state == 'none') {
						               document.getElementById(id).style.display = 'block';
						           }*/ 
						    	 $('div[id^='+id+']').css('display','block');
						    var c = document.getElementById(id); 
						     while (c.firstChild) { 
						    	 $('div[id^='+id+']').attr("class", "");
								c.removeChild(c.firstChild); }
						     var t=jsonstring.split("],");
						   var  f=t[0].split("~,");
						   for(var i=0;i<f.length;i++)
						   {
						  xvalarea[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));
						   }
						   var k = 0;
						   for(var i=0;i<t.length;i++)
						   {
						    testarea[i]=t[i].substring(t[i].indexOf("{"),t[i].lastIndexOf("}")+1);
						   earea[i] = t[i].substring(0,t[i].indexOf("["));
						   if(earea[i]=="")
						    {
						    earea[i]=yname;
						    }
						   var x = testarea[i].split(",");
						       for(var j=0;j<x.length;j++){
						          arrarea[k++] = x[j];
						    }
						   }
						   var month = "";
						   var day="";
						   var b = new Array();
						   var yvalorig=new Array();
						   for(var n=0; n<arrarea.length;n++){
						    b[n] =parseFloat( arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}")));
						    yvalorig[n]=arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}"));
						   }
						     }
						     var maxlen = 0;
						     for (i=0; i<yvalorig.length; i++) {
						       if (yvalorig[i].length>maxlen) {
						         maxlen = yvalorig[i].length;
						       }
						     }
						     if(maxlen>=13)
						      {
						      var left=130;
						      }
						     else
						      left=90;
						    var data=[];
						    var k=0;
						    for (i = 0; i < earea.length;i++ ) {
						    if(i == 0)
						      {
						       data.push({
						          key: earea[i],
						           "bar":"true",
						            values: []
						          });
						      }
						    else
						     {
						     data.push({
						         key: earea[i],
						           values: [],
						           color : colors[0]
						         });
						     }
				        for (j = 0; j < xvalarea.length;j++) {
				        	xvalarea[j] = xvalarea[j].replace(/amp;/g, '');
						          data[i].values.push({
						            x: xvalarea[j],
						            y:parseFloat(b[k])
						          });
						       k++;
						        }
					      }

						    nv.addGraph(function() {
						     var chart =nv.models.linePlusBarChart();
						     chart.x(function(d,i) { return i; });
						      chart.xAxis.tickFormat(function(d, i){
						           return xvalarea[d]; 
						      })
					       chart.xAxis.axisLabel(xname);
						   chart.showControls(false);//to remove stacked/group functionality
						   chart.y1Axis.tickFormat(d3.format(',f'));
						   chart.y2Axis.tickFormat(d3.format('.2f'));
						   chart.margin({top: 15, right: left, bottom: 50, left:left});
						     if(xvalarea.length>=10)
						     {
							     chart.margin({bottom: 100});
							     chart.xAxis.rotateLabels(-15);
						     }
						     chart.bars.forceY([0]);
						     d3.select('div[id^='+id+']').append("svg").style('height','420px').datum(data).transition()
						       .duration(500).call(chart);
								nv.utils.windowResize(chart.update);
						     return chart;
						    });
				}
				else// type 1 or 2
				 {
					nv.addGraph(function() {
						var chart = nv.models.lineChart();
						chart.x(function(d, i) {
							return i;
						});
						chart.xAxis.tickFormat(function(d, i) {
							return xvalarea[d]; //"Year1 Year2, etc depending on the tick value - 0,1,2,3,4"
						});
						chart.tooltips(true);
						chart.yAxis.tickFormat(d3.format('.2f'));
						 chart.yAxis.axisLabel(yname);
						 chart.margin({top: 20, right: 30, bottom: 50, left:left});
						 chart.xAxis.axisLabel(xname);
						 //chart.showControls(false);//to remove stacked/group functionality
						if(xvalarea.length>10)
							{
							chart.margin({bottom: 100});
							chart.xAxis.rotateLabels(-15);
						}
						d3.select('div[id^='+id+']').append("div").attr('class','table-responsive').append("svg").style({'height' : '420px','width' : '3000px'}).datum(data).transition().duration(500).call(chart);
						//====legend start
							var legend ;//= nv.models.legend1();
							var divlgnd = d3.select('div[id^='+id+']').append('div').attr('class','legenddiv');
							var contnr = divlgnd.attr('id','lgdiv_'+id).append('svg').attr('id','lgsvg_'+id);//.style('width','200px').style('height','440px');
							var vrap = contnr.selectAll('g.nv-wrap.nv-lineChart').data([data]);//.append('g').attr('class','nv-legendWrap').datum(data).call(legend);
							 var gntr = vrap.enter().append('g').attr('class', 'nvd3 nv-wrap nv-lineChart').append('g').attr('id','lgnd_g_'+id);
							 var gg = vrap.select('g');
							 gntr.append('g').attr('class', 'nv-legendWrap');
							 if(layoutchoice==1 ){
								 	legend = nv.models.legend1().layoutChoice(1);
									/*divlgnd.attr('class','legenddiv2');
								 	contnr.attr('class','legendsvg2');*/
								 	legend.width(900);
								 }
							 else if(layoutchoice==2 ){
								 	legend = nv.models.legend1().layoutChoice(2);
									divlgnd.attr('class','legenddiv2');
								 	contnr.attr('class','legendsvg2');
								 	legend.width(500);
								 }
							 else if(layoutchoice==3 ){
								divlgnd.attr('class','legenddiv3');
								contnr.attr('class','legendsvg3');
								legend = nv.models.legend1().width(330);
								legend.layoutChoice(3);
							 }
							 gg.select('.nv-legendWrap')
					            .datum(data)
					            .call(legend);
							 if($('.nv-series').length <= 3) {
									$('#lgsvg_'+id).closest('.legenddiv').css({overflow: 'hidden', height: '20px'});
							 }
							legend.dispatch.on('legendClick', function(d,i) {
						        try{
								d.disabled = !d.disabled;
						        }catch(e){}
		
						        if (!data.filter(function(d) { return !d.disabled }).length) {
						          data.map(function(d) {
						            d.disabled = false;
						            vrap.selectAll('.nvd3 .nv-legend .nv-series').classed('disabled', false);
						            return d;
						          });
						        }
						        d3.select('#lgdiv_'+id+' svg').datum(data).call(legend);
						        d3.select('#'+id+' svg').datum(data).transition().duration(500).call(chart);
						      });
							//$("#"+id).append($("#"+id+" div").get().reverse());
							//d3.select('#lgsvg_'+id).style('height',document.getElementById('lgnd_g_'+id).getBoundingClientRect().height+'px');
						//====legend end
							nv.utils.windowResize(chart.update);
						return chart;
					});
						
				 }
				}/*
				if(layoutchoice==2)
				{
						nv.addGraph(function() {
							var chart = nv.models.lineChart();
							chart.x(function(d, i) {
								return i;
							});
							chart.xAxis.tickFormat(function(d, i) {
								return xvalarea[d]; //"Year1 Year2, etc depending on the tick value - 0,1,2,3,4"
							});
						  chart.yAxis.tickFormat(d3.format(',.2f'));
						 chart.yAxis.axisLabel(yname);
						 chart.margin({top: 15, right: 31, bottom: 50, left:left})
						 chart.xAxis.axisLabel(xname);
							if(xvalarea.length>10)
							{
								chart.margin({bottom: 100});
								chart.xAxis.rotateLabels(-15);
							}
							d3.select('#' + id).append("svg").datum(data).transition().duration(500).call(chart);
							return chart;
						});
						}
				if(layoutchoice==3)
				{
						var width;
						if(userType=='all')
							{
							width=250;
						}
						if(userType=='indi')
							{
							
							width=300;
							}
						nv.addGraph(function() {
							var chart = nv.models.lineChart();
							chart.x(function(d, i) {
								return i;
							});
							chart.xAxis.tickFormat(function(d, i) {
								return xvalarea[d]; //"Year1 Year2, etc depending on the tick value - 0,1,2,3,4"
							});
							 chart.yAxis.tickFormat(d3.format(',.2f'));
							 chart.yAxis.axisLabel(yname);
							 chart.xAxis.axisLabel(xname);
							 chart.margin({top: 15, right: 25, bottom: 50, left:left})
							 if(xvalarea.length>10)
							 {
								 chart.margin({bottom: 100});
								 chart.xAxis.rotateLabels(-15);
							 }
							d3.select('#' + id).append("svg").datum(data).transition().duration(500).call(chart);
							return chart;
						});
						}*/
			}
		}
	}
	}
}
	
	function getyvalstack(k)
	{
	 return parseFloat(b[k]);
	}
	
	function get_random_color() {
	    var letters = '0123456789ABCDEF'.split('');
	    var color = '#';
	    for (var i = 0; i < 6; i++ ) {
	        color += letters[Math.round(Math.random() * 15)];
	    }
	    return color;
	}
	
	function  showFusionArea(divid,xname,yname,layoutchoice,userType)
	{
	//alert("This type of chart is not supported");
		/*var state = document.getElementById(divid).style.display;
		if (state == 'none') {
            document.getElementById(divid).style.display = 'block';
        }*/ 
		$('#'+id).css('display','block');
		widthValue = getWidthValue(layoutchoice,userType);
		var yvalarea=new Array();
		var jsonstring = $('#data_'+id).val();
		var str=new Array();
		var xval=new Array();
		var earea = new Array();
		var arrarea = new Array();
		var testarea=new Array();
		
		var strxml="<graph  xAxisName="+"'"+xname+"'"+"  yAxisName="+"'"+yname+"'"+" yAxisMaxValue='1'>";
		strxml+="<categories>";
		
		var xvalarea=new Array();
		
		var t=jsonstring.split("],");
 		
 		
		 var  f=t[0].split("~,");

		 for(var i=0;i<f.length;i++)
		 {

		xvalarea[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));
		//alert("categories"+xvalarea[i]);
		strxml+="<category name="+"'"+xvalarea[i]+"'"+"/>";
		 }
        strxml+="</categories>";
		 var k = 0;
		 for(var i=0;i<t.length;i++)
		 {

		  testarea[i]=t[i].substring(t[i].indexOf("{"),t[i].lastIndexOf("}")+1);
		 //alert("testing i::::::::"+test[i] );
		 earea[i] = t[i].substring(0,t[i].indexOf("["));
		 //alert(e[i]);

		 //e[i] = test[i];
		 //alert("eeeeeeeeee"+e[i]);
		 var x = testarea[i].split(",");
		 //alert("X LENGTH :"+x.length);
		     for(var j=0;j<x.length;j++){

		        arrarea[k++] = x[j];
		 	}

		 }

		 var month = "";
		 var day="";
		 var b = new Array();
		 for(var n=0; n<arrarea.length;n++){

		   
		 	b[n] =parseFloat( arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}")));
		 	
		 	
		 	
		    
		 }
		 var k=0;
		 
		 for (i = 0; i < earea.length;i++ ) {
			  
                 strxml+="<dataset seriesName="+"'"+earea[i]+"'"+" color="+"'"+get_random_color()+"'"+">";
			    for (j = 0; j < xvalarea.length;j++) {

	                   
				
strxml+="<set value="+"'"+b[k]+"'"+"/>";
k++;
			     
			    }
			    strxml+="</dataset>"; 
				
			  }
		 strxml+="</graph>";
		
		// alert("xml after final"+strxml);
		//var strxmls="<graph ><categories><category name='08/01' /><category name='08/02' /></categories> <dataset seriesname='Product A'><set value='36634' /> <set value='43653' /> <set value='55565' /> </dataset><dataset seriesname='Product B'><set value='12152' /> <set value='15349' /> <set value='16442' /> </dataset></graph>";
		 var strxmls="<graph yAxisMaxValue='1'><categories><category name='08/01' /> <category name='08/02' /> </categories><dataset  seriesname='ProductA'><set value='36634' /> <set value='43653' /> <set value='55565' /> </dataset><dataset  seriesname='Product B' ><set value='12152' /> <set value='15349' /> <set value='16442' /> </dataset> </graph>";
		 if(layoutchoice==1)
		  {
	 
	  var chart= new FusionCharts("plugins/Charts/FCF_MSArea2D.swf", "mychart", widthValue, "350", "0", "0");
	 
		  }
	  
	  if(layoutchoice==2)
	  {
  
	var chart= new FusionCharts("plugins/Charts/FCF_MSArea2D.swf", "mychart",widthValue,"360", "0", "0");

	  }
	  
	  if(layoutchoice==3)
	  {

	var chart= new FusionCharts("plugins/Charts/FCF_MSArea2D.swf", "mychart",widthValue, "360", "0", "0");

	  }
	  chart.setDataXML(strxml);
      chart.setTransparent(true);
      
      chart.render(divid);
		 
	}
	
 	function frameChartarea(id,divid,xname,yname,layoutchoice,chartid,updateType,userType)//,mapdiv)//,bigdiv,bigchart)
	{
		
		
		var currentDiv=$('div[id^='+id+']');
		var jsonstring="" ;
		id=currentDiv.attr('id').split('@')[0];
		var currentlevel=currentDiv.attr('id').split('@')[1];
		
		if(currentlevel!=null && currentlevel!='undefined'){
			
			if($('input[id^='+chartid+']'+'[id*='+currentlevel+']').length>0)
				{
				jsonstring=	$('input[id^='+chartid+']'+'[id*='+currentlevel+']').val();
				}else{
				jsonstring=	$('#data_divid'+chartid).val();
				}
			
		}else{
			jsonstring= $('#data_'+id).val();
		}
		currentDiv.empty();
 		/*$("#"+id).empty();*/
 		if(showHide == "true"){
 			bootbox.alert({ 
					title: 'Alert',
		    	    size: 'small',
		    	    message: "Please Maximize", 
		    	});
		}else{
		var yvalarea=new Array();
		var str=new Array();
		var xval=new Array();
		var earea = new Array();
		var arrarea = new Array();
		var testarea=new Array();
		var xvalarea=new Array();
		/*var jsonstring = $('#data_'+id).val();*/
		var brwsrSupport = getInternetExplorerVersion();

		if (brwsrSupport=='fus') 
		{ 
			widthValue = getWidthValue(layoutchoice,userType);
			if((jsonstring=="NA1")||(jsonstring=="NA2") || (jsonstring == 'error'))
			{
				bootbox.alert({ 
					title: 'Alert',
		    	    size: 'small',
		    	    message: "chart can't be found for empty data", 
		    	});
			}
			else
			{
				if(updateType)
				{
				updateChartType(chartid,3);
				}
				//var state = document.getElementById(divid).style.display;
				var type=jsonstring.charAt(jsonstring.length-1);
				if(type==2)
				{
					showFusionArea(divid,jsonstring,xname,yname,layoutchoice,userType);
				}
				else if(type==1)
				{
					/*if (state == 'none') {
		            document.getElementById(divid).style.display = 'block';
		        } */
					$('div[id^='+id+']').css('display','block');
					var yval=new Array();
					var xval=new Array();
					str = jsonstring.split("~,");
					for (i in str) {
						xval[i] = str[i].substring(str[i].indexOf("{") + 1, str[i].indexOf(":"));
						yval[i] = parseFloat(str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}")));
					}
		 
					var strxml="<graph  xAxisName="+"'"+xname+"'"+"  yAxisName="+"'"+yname+"'"+" yAxisMaxValue='1'>";
					for(var j=0;j<xval.length;j++)
					{
						strxml+="<set name="+"'"+xval[j]+"'"+" "+"value="+"'"+yval[j]+"'"+" "+"color='AFD8F8'/>";
					}
					var xml=strxml+"</graph>";
					if(layoutchoice==1)
					{
						var chart= new FusionCharts("plugins/Charts/FCF_Area2D.swf", "mychart",widthValue, "350", "0", "0");
					}
					if(layoutchoice==2)
					{
						var chart= new FusionCharts("plugins/Charts/FCF_Area2D.swf", "mychart", widthValue,"360", "0", "0");
					}
					if(layoutchoice==3)
					{
						var chart= new FusionCharts("plugins/Charts/FCF_Area2D.swf", "mychart",widthValue, "360", "0", "0");
					}
					var strXML = "<graph caption='Hours worked' showNames='1'><set name='Tom' value='32' color='AFD8F8'/><set name='Mary' value='16' color='F6BD0F'/><set name='Jane' value='42' color='8BBA00'/></graph>";
					chart.setDataXML(xml);
					chart.setTransparent(true);
					chart.render(divid);
				}
			}
		}
		else{
			/*var state = document.getElementById(id).style.display;
	        if (state == 'none') {
	            document.getElementById(id).style.display = 'block';
	        	}*/
			$('div[id^='+id+']').css('display','block');
			if((jsonstring=="NA1")||(jsonstring=="NA2")|| (jsonstring == 'error'))
			{
				bootbox.alert({ 
					title: 'Alert',
		    	    size: 'small',
		    	    message: "chart can't be found for empty data", 
		    	});
			}
			else
			{
				if(updateType)
				{
				updateChartType(chartid,3);
				}
			/*var state = document.getElementById(id).style.display;
			if (state == 'none') {
	            document.getElementById(id).style.display = 'block';
	        }*/ 
				$('div[id^='+id+']').css('display','block');
			var c = $('div[id^='+id+']');
			while (c.firstChild) { 
				$('div[id^='+id+']').attr("class", "");
				c.removeChild(c.firstChild); }
			var t=jsonstring.split("],");
			var  f=t[0].split("~,");
			for(var i=0;i<f.length;i++)
			{
				xvalarea[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));
			}
			var k = 0;
			for(var i=0;i<t.length;i++)
			{
				testarea[i]=t[i].substring(t[i].indexOf("{"),t[i].lastIndexOf("}")+1);
				earea[i] = t[i].substring(0,t[i].indexOf("["));
				if(earea[i]=="")
				{
					earea[i]=yname;
				}
				var x = testarea[i].split(",");
				for(var j=0;j<x.length;j++){
					arrarea[k++] = x[j];
				}
			}
			
			var month = "";
			var day="";
			var b = new Array();
			var yvalorig=new Array();
			for(var n=0; n<arrarea.length;n++){
				b[n] =parseFloat( arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}")));
				yvalorig[n]=arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}"));
			}
			var maxlen = 0;
			for (i=0; i<yvalorig.length; i++) {
				if (yvalorig[i].length>maxlen) {
					maxlen = yvalorig[i].length;
				}
				}
			if(maxlen>=13)
			{
				var left=130;
			}
			else
				left=90;
			var data=[];
			var k=0;
			for (i = 0; i < earea.length;i++ ) {
				data.push({
					key: earea[i],
					values: []
				});
		    for (j = 0; j < xvalarea.length;j++) {
		    	xvalarea[j] = xvalarea[j].replace(/amp;/g, '');
		    	var v = b[k];
				if(isNaN(v)){
					v=0;
				}
				//else
		    	data[i].values.push({
		    		x: xvalarea[j],
		    		y:parseFloat(v) //b[k])
		    	});
		    	k++;
		    	}
			}
			/*if(layoutchoice==1)
			{*/
				nv.addGraph(function() {
					var chart =nv.models.stackedAreaChart();
					chart.x(function(d,i) { return i; });
					chart.xAxis.axisLabel(xname);
					chart.showControls(false);//to remove stacked/group functionality
					chart.xAxis.tickFormat(function(d, i){
						return xvalarea[d]; //"Year1 Year2, etc depending on the tick value - 0,1,2,3,4"
					})
					chart.yAxis.axisLabel(yname);
					chart.yAxis.tickFormat(d3.format('.2f'));
					chart.margin({top: 15, right: 30, bottom: 50, left:90});
					
					d3.select('div[id^='+id+']').append("div").attr('class','table-responsive starea1').append("svg").attr('class','starea1').datum(data).transition().duration(500).call(chart);
					//====legend start
						var legend ;//= nv.models.legend1();
						var divlgnd = d3.select('div[id^='+id+']').append('div').attr('class','legenddiv');
						var contnr = divlgnd.attr('id','lgdiv_'+id).append('svg').attr('id','lgsvg_'+id);//.style('width','200px').style('height','440px');
						var vrap = contnr.selectAll('g.nv-wrap.nv-stackedAreaChart').data([data]);//.append('g').attr('class','nv-legendWrap').datum(data).call(legend);
						 var gntr = vrap.enter().append('g').attr('class', 'nvd3 nv-wrap nv-stackedAreaChart').append('g').attr('id','lgnd_g_'+id);
						 var gg = vrap.select('g');
						 gntr.append('g').attr('class', 'nv-legendWrap');
						 if(layoutchoice==1 ){
							 	legend = nv.models.legend1().layoutChoice(1);
								/*divlgnd.attr('class','legenddiv2');
							 	contnr.attr('class','legendsvg2');*/
							 	legend.width(900);
							 }
						 else if(layoutchoice==2 ){
							 	legend = nv.models.legend1().layoutChoice(2);
								divlgnd.attr('class','legenddiv2');
							 	contnr.attr('class','legendsvg2');
							 	legend.width(500);
							 }
						 else if(layoutchoice==3 ){
							divlgnd.attr('class','legenddiv3');
							contnr.attr('class','legendsvg3');
							legend = nv.models.legend1().width(330);
							legend.layoutChoice(3);
						 }
						 gg.select('.nv-legendWrap')
				            .datum(data)
				            .call(legend);
						 if($('.nv-series').length <= 3) {
								$('#lgsvg_'+id).closest('.legenddiv').css({overflow: 'hidden', height: '20px'});
						 }
						legend.dispatch.on('legendClick', function(d,i) {
					        try{
							d.disabled = !d.disabled;
					        }catch(e){}
	
					        if (!data.filter(function(d) { return !d.disabled }).length) {
					          data.map(function(d) {
					            d.disabled = false;
					            vrap.selectAll('.nvd3 .nv-legend .nv-series').classed('disabled', false);
					            return d;
					          });
					        }
					        d3.select('#lgdiv_'+id+' svg').datum(data).call(legend);
					        d3.select('#'+id+' svg').datum(data).transition().duration(500).call(chart);
					      });
						//$("#"+id).append($("#"+id+" div").get().reverse());
						//d3.select('#lgsvg_'+id).style('height',document.getElementById('lgnd_g_'+id).getBoundingClientRect().height+'px');
					//====legend end
						nv.utils.windowResize(chart.update);
					return chart;
				});
				$('#lgdiv_'+id).resizable();
			/*}
			if(layoutchoice==2)
			{
				nv.addGraph(function() {
					var chart =nv.models.stackedAreaChart();
					chart.x(function(d,i) { return i; });
					chart.margin({top: 15, right: 31, bottom: 50, left:left});
					chart.xAxis.axisLabel(xname);
					chart.xAxis.tickFormat(function(d, i){
							return xvalarea[d]; //"Year1 Year2, etc depending on the tick value - 0,1,2,3,4"
					})
					chart.yAxis.axisLabel(yname);
					chart.yAxis.tickFormat(d3.format(',.1f'));
					if(xvalarea.length>=10)
					{
						chart.margin({bottom: 100});
						chart.xAxis.rotateLabels(-15);
					}
					d3.select('#'+id).append("svg").datum(data).transition()
						.duration(500).call(chart);
					return chart;
			});
			}
			if(layoutchoice==3)
			{
				nv.addGraph(function() {
					var chart =nv.models.stackedAreaChart();
					chart.x(function(d,i) { return i; });
					chart.xAxis.axisLabel(xname);
					chart.xAxis.tickFormat(function(d, i){
					     return xvalarea[d]; //"Year1 Year2, etc depending on the tick value - 0,1,2,3,4"
					})
					chart.yAxis.axisLabel(yname);
					chart.margin({top: 15, right: 25, bottom: 50, left:left});
					chart.yAxis.tickFormat(d3.format(',.1f'));
				if(xvalarea.length>10)
				{
					chart.margin({bottom: 100});
					chart.xAxis.rotateLabels(-15);
				}
				d3.select('#'+id).append("svg").datum(data).transition().duration(500).call(chart);
				return chart;
			});
			}*/
		} 
	}//else
	}
}
	
	function get_random_color() {
	    var letters = '0123456789ABCDEF'.split('');
	    var color = '#';
	    for (var i = 0; i < 6; i++ ) {
	        color += letters[Math.round(Math.random() * 15)];
	      
	    }
	    return color;
	}
	
	
	function frameChartpie(id, divid, layoutchoice, chartid, updateType,userType){//,mapdiv,bigdiv,bigchart) {
		
		
		var currentDiv=$('div[id^='+id+']');
		var jsonstring="" ;
		id=currentDiv.attr('id').split('@')[0];
		var currentlevel=currentDiv.attr('id').split('@')[1];
		
		if(currentlevel!=null && currentlevel!='undefined'){
			
			if($('input[id^='+chartid+']'+'[id*='+currentlevel+']').length>0)
				{
				jsonstring=	$('input[id^='+chartid+']'+'[id*='+currentlevel+']').val();
				}else{
				jsonstring=	$('#data_divid'+chartid).val();
				}
			
		}else{
			jsonstring= $('#data_'+id).val();
		}
		currentDiv.empty();
		if(showHide == "true"){
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "Please Maximize", 
	    	});
		}else{
		var brwsrSupport = getInternetExplorerVersion();
		if (brwsrSupport=='fus') 
		{
			widthValue = getWidthValue(layoutchoice,userType);
		if (jsonstring == "NA1" || jsonstring == "NA2" ||jsonstring == 'error' ) {
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "chart can't be found for empty data", 
	    	});
		}
		else {
			var type = jsonstring.charAt(jsonstring.length - 1);
			if (type == 2) {
				bootbox.alert({ 
					title: 'Alert',
		    	    size: 'small',
		    	    message: "Pie chart is not supported", 
		    	});
			} else if (type == 1) {
				if (updateType) {
					updateChartType(chartid, 2);
				}
				/*var state = document.getElementById(divid).style.display;
				if (state == 'none') {
					document.getElementById(divid).style.display = 'block';
				}*/

				var yval = new Array();
				var xval = new Array();
				str = jsonstring.split("~,");

				for (i in str) {
					xval[i] = str[i].substring(str[i].indexOf("{") + 1, str[i].indexOf(":"));
					yval[i] = parseFloat(str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}")));
				}
				var strxml = "<graph showNames='1' showValues='0' manageLabelOverflow='1' useEllipsesWhenOverflow='1'>";
				for ( var j = 0; j < xval.length; j++) {
					strxml += "<set name=" + "'" + xval[j] + "'" + " "
							+ "value=" + "'" + yval[j] + "'" + " " + "color="
							+ "'" + get_random_color() + "'" + "/>";
				}
				var xml = strxml + "</graph>";

				
				if (layoutchoice == 1) {
					var chart = new FusionCharts(
							"plugins/Charts/FCF_Pie2D.swf", "mychart", widthValue,
							"350", "0", "0");

				}

				if (layoutchoice == 2) {
					var chart = new FusionCharts(
							"plugins/Charts/FCF_Pie2D.swf", "mychart",widthValue,
							"360", "0", "0");
				}

				if (layoutchoice == 3) {

					var chart = new FusionCharts(
							"plugins/Charts/FCF_Pie2D.swf", "mychart",widthValue,
							"360", "0", "0");
				}
				var strXML = "<graph caption='Hours worked' showNames='1'><set name='Tom' value='32' color='AFD8F8'/><set name='Mary' value='16' color='F6BD0F'/><set name='Jane' value='42' color='8BBA00'/></graph>";

				chart.setDataXML(xml);
				chart.setTransparent(true);
				chart.render(divid);
			}
		}

	} else {
		
		/*var state = document.getElementById(id).style.display;
	    if (state == 'none') {
	        document.getElementById(id).style.display = 'block';
	       }*/
		$('div[id^='+id+']').css('display','block');
	    if ((jsonstring == "NA1") || (jsonstring == "NA2") || (jsonstring == 'error')) {
	    	bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "chart can't be found for empty data", 
	    	});
		} else {
			/*var state = document.getElementById(id).style.display;
			if (state == 'none') {
				document.getElementById(id).style.display = 'block';
			}*/
			var testdata1 = [];
			var type = jsonstring.charAt(jsonstring.length - 1);
			if (updateType) {
				if (type == 2 || type == 3)
					updateChartType(chartid, 11);
				else 
					updateChartType(chartid, 2);
			}
			if (type == 2 || type == 3) {
				//alert("Pie chart is not supported");
				/*var c = document.getElementById(id);*/
				var c =$('div[id^='+id+']');
				while (c.firstChild) {
					$('#'+id).attr("class", "");
					c.removeChild(c.firstChild);
				}
				var yval = new Array();
				var xval = new Array();
				var t = [],e=[];
				t = jsonstring.split("],");
				//====================
					for ( var i = 0; i < t.length; i++) {
	
						e[i] = t[i].substring(0, t[i].indexOf("["));
						}
				//====================
				for(var ts=0;ts<t.length; ts++){
				//for(var str in ts){
				var str = t[ts].split("~,");
				for (i in str) {
					xval[i] = str[i].substring(str[i].indexOf("{") + 1, str[i].indexOf(":"));
					yval[i] = parseFloat(str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}")));
				 }
				var result = 0;
				for ( var i = 0; i < yval.length; i++) {
					if(isNaN(yval[i])){
						yval[i]=0;
					}
					result += parseFloat(yval[i]);
				}
				var testdata = [];
				for ( var i = 0; i < xval.length; i++) {
					if(isNaN(yval[i])){
						yval[i]=0;
					}
					try{xval[i] = xval[i].replace(/amp;/g, '');}catch(e){}
					testdata.push({
						key : xval[i],
						  y : yval[i],
						  r : result
					});
				
				}
				testdata1[ts]=[];
				testdata1[ts][0]=testdata;
				testdata1[ts][1]=e[ts];//t[i].substring(0, t[i].indexOf("["));//"";
				//result1[ts] =result;
				}
			}
			else if (type == 1) {
				/*var c = document.getElementById(id);*/
				var c =$('div[id^='+id+']');
				while (c.firstChild) {
					$('div[id^='+id+']').attr("class", "");
					c.removeChild(c.firstChild);
				}
				var yval = new Array();
				var xval = new Array();
				str = jsonstring.split("~,");
				for (i in str) {
					xval[i] = str[i].substring(str[i].indexOf("{") + 1, str[i].indexOf(":"));
					yval[i] = parseFloat(str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}")));
				 }
				var result = 0;
				for ( var i = 0; i < yval.length; i++) {
					if(isNaN(yval[i])){
						yval[i]=0;
					}
					result += parseFloat(yval[i]);
				}
				var testdata = [];
				for ( var i = 0; i < xval.length; i++) {
					xval[i] = xval[i].replace(/amp;/g, '');
					if(isNaN(yval[i])){
						yval[i]=0;
					}
					testdata.push({
						key : xval[i],
						  y : yval[i],
						  r : result
					});
	
				}
				testdata1[0] = [];
				testdata1[0][0]=testdata;
				testdata1[0][1]="";
			}//sample
			var chart = [];
			$('div[id^='+id+']').attr("class", "table-responsive");
				nv.addGraph(function() {
					var data ;//=testdata1[jj][0];
					for(var jj=0;jj<testdata1.length;jj++){
						data = testdata1[jj][0];
						chart[jj] = nv.models.pieChart()
						.chartName(testdata1[jj][1])
					.x(function(d) {
						return d.key;
					}).y(function(d) {
						//alert(d.y/d.r);
						return ((d.y / d.r) * 100);
					}).showLabels(true).values(function(d) {
						return d;
					});
						if(testdata1.length == 1) {
							d3.select('div[id^='+id+']').append("svg").style({'height' : '420px','width' : '100%'}).datum([ data ]).transition().duration(1200)
							.call(chart[jj]);;
						} else {
							d3.select('div[id^='+id+']').append("svg").attr('class','piesvg_2').style('height','420px').datum([ data ]).transition().duration(1200)
							.call(chart[jj]);;
						}
						
						/*if(type==1 ){
							d3.select('#' + id).append("svg").attr('class','piesvg_2').style('height','420px').datum([ data ]).transition().duration(1200)
							.call(chart[jj]);
						}
						else{
							d3.select('#' + id).append("svg").attr('class','piesvg1_2').style('height','420px').datum([ data ]).transition().duration(1200)
							.call(chart[jj]);
						}*/
						 nv.utils.windowResize(chart[jj].update);
					//return chart;
					//====legend start
					/*var legend ;
					//legend = nv.models.legend1();
					var divlgnd = d3.select('#' + id).append('div').attr('class','legenddiv');
					var contnr = divlgnd.attr('id','lgdiv_'+id).append('svg');//.style('width','200px').style('height','440px');
					contnr.attr('id','lgsvg_'+id);
					var vrap = contnr.selectAll('g.nv-wrap.nv-pieChart').data([data]);//.append('g').attr('class','nv-legendWrap').datum(data).call(legend);
					 var gntr = vrap.enter().append('g').attr('class', 'nvd3 nv-wrap nv-pieChart').append('g').attr('id','lgnd_g_'+id);
					 var gg = vrap.select('g');
					 gntr.append('g').attr('class', 'nv-legendWrap');
					 
					 if(layoutchoice==1 ){
						 	legend = nv.models.legend1().layoutChoice(1);divlgnd.style('width','70%');
							//divlgnd.attr('class','legenddiv2');
						 	//contnr.attr('class','legendsvg2');
						 	legend.width(300);
						 }
					 else if(layoutchoice==2 ){
						 	legend = nv.models.legend1().layoutChoice(2);
							divlgnd.attr('class','legenddiv2');divlgnd.style('width','70%');
						 	contnr.attr('class','legendsvg2');
						 	legend.width(300);
						 }
					 else if(layoutchoice==3 ){
						divlgnd.attr('class','legenddiv3');divlgnd.style('width','70%');
						contnr.attr('class','legendsvg3');
						legend = nv.models.legend1().width(300);
						legend.layoutChoice(3);
					 }
					 gg.select('.nv-legendWrap')
			            .datum(data)
			            .call(legend);
					legend.dispatch.on('legendClick', function(d,i) {
				        try{
						d.disabled = !d.disabled;
				        }catch(e){}

				        if (!data.filter(function(d) { return !d.disabled }).length) {
				        	data.map(function(d) {
				            d.disabled = false;
				            vrap.selectAll('.nvd3 .nv-legend .nv-series').classed('disabled', false);
				            return d;
				          });
				        }
				        d3.select('#lgdiv_'+id+' svg').datum(data).call(legend);
				        d3.select('#'+id+' svg').datum(data).transition().duration(500).call(chart);
				      });
					//$("#"+id).append($("#"+id+" div").get().reverse());
					d3.select('#lgsvg_'+id).style('height',document.getElementById('lgnd_g_'+id).getBoundingClientRect().height+'px');*/
				//====legend end
					}
					nv.utils.windowResize(chart.update);
					return chart;
				});
		//unused_dahelper.js
	   }//else if
	 }//else
	}
	}//method
	
	function getWidthValue(layoutchoice,userType)
	{
		var widthValue="300";
		if (userType == 'indi') {
		if (layoutchoice == 1) {
			widthValue = "1100";
		} else if (layoutchoice == 2) {
			widthValue = "450";
		} else if (layoutchoice == 3) {
			widthValue = "350";
		}
	}  else if (userType == 'all') {
		if (layoutchoice == 1) {
			widthValue = "800";
		} else if (layoutchoice == 2) {
			widthValue = "400";
		} else if (layoutchoice ==3) {
			widthValue = "288";
		}
	 }
		return widthValue;
	}
	
	function frameChartsDefaultIE(id,xname,yname,chartid,charttype,layoutchoice,userType,bubblechartStr,timeCol)
	{
		$("#"+id).empty();
		showHide = "false";
		var divid = 'img'+chartid;
		var updateType= false;
		
		if(charttype==1){
			frameChartbar(id,divid,xname,yname,layoutchoice,chartid,updateType,userType);
			}
		else if(charttype==2 || charttype==11)
			{
			frameChartpie(id,divid,layoutchoice,chartid,updateType,userType);
			}
		else if(charttype==3)
		{
			frameChartarea(id,divid,xname,yname,layoutchoice,chartid,updateType,userType);
		}
		else if(charttype==4)
		{
			//frameChartbubble(id,divid,jsonstring,xname,yname,layoutchoice,chartid,updateType);
			frameChartbar(id,divid,xname,yname,layoutchoice,chartid,updateType,userType);
		}
		else if(charttype==5)
		{
			frameCharthorizontalBar(id,divid,xname,yname,layoutchoice,chartid,updateType,userType);
		}
		else if(charttype==6)
		{
			framelineChart(id,divid,xname,yname,layoutchoice,chartid,updateType,userType);
		}
	}
	
	 function frameChartsDefult(id,xname,yname,chartid,charttype,layoutchoice,userType,bubblechartStr,timeCol,bins)//,mapdiv,bigdiv,bigchart)
	{
		 $("#"+id).empty();
		showHide = "false";
		var divid = 'img'+chartid;
		var updateType= false;
		if(charttype==1)
			{
			frameChartbar(id,divid,xname,yname,layoutchoice,chartid,updateType,userType);//,mapdiv);//,bigdiv,bigchart);
			}
		else if(charttype==2 || charttype==11)
			{
			frameChartpie(id,divid,layoutchoice,chartid,updateType,userType);//,mapdiv);//,bigdiv,bigchart);
			}
		else if(charttype==3)
		{
			frameChartarea(id,divid,xname,yname,layoutchoice,chartid,updateType,userType);//,mapdiv);//,bigdiv,bigchart);
		}
		else if(charttype==4)
		{
			frameChartbubble(id,divid,bubblechartStr,xname,yname,layoutchoice,chartid,updateType,userType,timeCol);//,mapdiv);//,bigdiv,bigchart);
		}
		else if(charttype==5)
		{
			frameCharthorizontalBar(id,divid,xname,yname,layoutchoice,chartid,updateType,userType);//,mapdiv);//,bigdiv,bigchart);
		}
		else if(charttype==6)
		{
			framelineChart(id,divid,xname,yname,layoutchoice,chartid,updateType,userType);//,mapdiv);//,bigdiv,bigchart);
		}
		else if(charttype==7)
		{
			frameHistogram(id,xname,yname,layoutchoice,chartid,bins);
		}
		else if(charttype==10)
		{
			frameGauge(id,layoutchoice,chartid);
		}
		else if(charttype==8)
		{
			frameFunnel(id,chartid,layoutchoice);//,"chart_div"+chartid,chartid);
		}
		else if(charttype==9)
		{
			showBulletChart(id,layoutchoice,chartid);
			//frameChartbar(id,divid,xname,yname,layoutchoice,chartid,updateType,userType);//for time being
		}
	}
	
	 function frameChartsDefult2(id,xname,yname,chartid,charttype,layoutchoice,userType,bubblechartStr,timeCol)
		{
		 $("#"+id).empty();
		 	showHide = "false";
			var divid = 'img2';
			var updateType= false;
			if(charttype==1)
				{
				frameChartbar(id,divid,xname,yname,layoutchoice,chartid,updateType,userType);
				}
			else if(charttype==2 || charttype==11)
				{
				frameChartpie(id,divid,layoutchoice,chartid,updateType,userType);
				}
			else if(charttype==3)
			{
				frameChartarea(id,divid,xname,yname,layoutchoice,chartid,updateType,userType);
			}
			else if(charttype==4)
			{
				frameChartbubble(id,divid,bubblechartStr,xname,yname,layoutchoice,chartid,updateType,userType,timeCol);
			}
			else if(charttype==5)
			{
				frameCharthorizontalBar(id,divid,jsonstring,xname,yname,layoutchoice,chartid,updateType,userType);
			}
			else if(charttype==6)
			{
				framelineChart(id,divid,xname,yname,layoutchoice,chartid,updateType,userType);
			}
		}

	 
	 /*actual code to generatedBarGraph*/
	function showbarsimple(id,jsonstring, xName, yName, chartid,layoutchoice,fcst) {
		$('div[id^='+id+']').css('display','block');
		var c = $('div[id^='+id+']');
		while (c.firstChild) {
			$('#'+id).attr("class", "");;
			c.removeChild(c.firstChild);
		}
		var yval = new Array();
		var xval = new Array();
		var str = new Array();
		var keys = new Array();
		var yvalorig=new Array();
		keys[0] = "";
		str = jsonstring.split("~,");
		for (i in str) {
			xval[i] = str[i].substring(str[i].indexOf("{") + 1, str[i].indexOf(":"));
			yvalorig[i]=str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}"));
			yval[i] = parseFloat(str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}")));
		}
		
		var maxlen = 0;
		for (i=0; i<yvalorig.length; i++) {
		  if (yvalorig[i].length>maxlen) {
		    maxlen = yvalorig[i].length;
		  }
		}
		if(maxlen>=13)
			{
			var left=100;//130
			}
		else
			left=90;//90
	
		var barchartdata = [];
		var k = 0;
	
		for ( var i = 0; i < keys.length; i++) {
			barchartdata.push({
				key : keys[i],
				values : []
			});
			for ( var j = 0; j < xval.length; j++) {
				xval[j] = xval[j].replace(/amp;/g, '');
			var v = yval[k];
			if(isNaN(v)){
			}
			else
			barchartdata[i].values.push({
				label : xval[j],
				value : yval[k]
			});
			k++;
		}
		}

		var chart;
		var chartRefresh;
		nv.addGraph(function() {
			chart = nv.models.discreteBarChart().x(function(d) {
				return d.label;
			})
	
			.y(function(d) {
				return d.value;
			}).staggerLabels(false)
			.margin({top: 20, right: 10, bottom: 60, left:90})
			// .staggerLabels(barchartdata[0].values.length > 8)
			.showValues(true)
			.tooltips(true);
			chart.yAxis.axisLabel(yName)
			chart.yAxis.tickFormat(d3.format('d'));
			try{if(fcst != undefined)chart.forcast = fcst;}catch(e){}
			chart.xAxis.axisLabel(xName)
			
			if(xval.length>=10){
				chart.margin({bottom:60});
				chart.showValues(false);
				chart.xAxis.rotateLabels(-20);
				}
			var parent = $('div[id^='+id+']').parent().attr("id");
			//document.getElementById(id).style.width = document.getElementById(id).getBoundingClientRect().width+'px';
			if(!$('div[id^='+id+']').is(':empty')) {
				$("#"+id).empty();
			}
			try{
				//d3.select("#"+parent).style("text-align","center");
				try{
					$("#sr_"+id).remove();
					$("#sr_"+id).remove();
					}catch(e){}
					d3.select('div[id^='+id+']').append('div').attr('id','sr_'+id).style('display','flex');
				//if(id != null && id != 'dispchart') {
/*					d3.select('div[id^='+id+']').append('a').attr('href',"javascript:void(0)").text('Sort').attr("class", "sort").attr('id',"sortid"+id).on("click", sortBars).html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sort&nbsp;');
					d3.select('div[id^='+id+']').append('a').attr('href',"javascript:void(0)").text('Reset').attr('class','reset').attr('id',"resetid"+id).on("click", resetBars);
*/
					d3.select("#sr_"+id).append('a').attr('href',"javascript:void(0)").text('Sort').attr("class", "sort").attr('id',"sortid"+id).on("click", sortBars).html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sort&nbsp;');
					d3.select("#sr_"+id).append('a').attr('href',"javascript:void(0)").text('Reset').attr('class','reset').attr('id',"resetid"+id).on("click", resetBars);
					$('#sr_'+id).append('<br />');
				//}
			}catch(e){}
			var wid = 1;
			if(xval.length>15 && layoutchoice=='1'){
				 
				 if(layoutchoice=='1'){
					 //d3.select("#"+id).attr("class","bigi");
					 d3.select('div[id^='+id+']').attr("class","table-responsive");
					 wid = 40 * xval.length;
						if(xval.length >= 45){// || (xval.length>15 && layoutchoice=='2'))//||(xval.length>8 && layoutchoice == "3"))
							if(fcst == 'homechart'){
								chartRefresh=d3.select('div[id^='+id+']').append('svg').attr('id','svg_'+id).attr("class","bigsvg").style('width','4000px').datum(barchartdata).transition().duration(500).call(chart);
							} else{
								chartRefresh=d3.select('div[id^='+id+']').append('svg').attr('id','svg_'+id).attr("class","bigsvg").style('width','4000px').datum(barchartdata).transition().duration(500).call(chart);
							}
						}else{
							if(fcst == 'homechart'){
								chartRefresh=d3.select('div[id^='+id+']').append('svg').attr('id','svg_'+id).attr("class","bigsvg").style('width','4000px').datum(barchartdata).transition().duration(500).call(chart);
							} else{
								chartRefresh=d3.select('div[id^='+id+']').append('svg').attr('id','svg_'+id).attr("class","bigsvg").style('width','4000px').datum(barchartdata).transition().duration(500).call(chart);
							}
						}
				 }
			}else if(xval.length>10 && (layoutchoice=='2' || layoutchoice==2) ) {
					 
					 wid = 40 * xval.length;
					 d3.select("#"+id).attr("class","table-responsive");
					 //if(xval.length>10){// || (xval.length>15 && layoutchoice=='2'))//||(xval.length>8 && layoutchoice == "3"))
					 chartRefresh=d3.select('div[id^='+id+']').append('svg').attr('id','svg_'+id).attr("class","bigsvg").style('width','2000px').datum(barchartdata).transition().duration(500).call(chart);
						//}else{
						//d3.select("#"+id).append('svg').attr('id','svg_'+id).attr("class","bigsvg").style('width',wid+'px').datum(barchartdata).transition().duration(500).call(chart);
						//}
				 	//}
		   }else if(xval.length>5 && (layoutchoice=='3' || layoutchoice==3) ) {
			   chart.xAxis.rotateLabels(-20);
			   
			   wid = 40 * xval.length;
			   d3.select('div[id^='+id+']').attr("class","table-responsive");
			   if(xval.length>5 && wid > 400){
				   chartRefresh= d3.select('div[id^='+id+']').append('svg').attr('id','svg_'+id).attr("class","bigsvg").style('width','1500px').datum(barchartdata).transition().duration(500).call(chart);
				}else{
					chartRefresh=d3.select('div[id^='+id+']').append('svg').attr('id','svg_'+id).attr("class","bigsvg").style('width','1500px').datum(barchartdata).transition().duration(500).call(chart);
				}
		  }else{
			  	wid = 40 * xval.length;
			  	d3.select("#"+id).attr("class","table-responsive");
			  	chartRefresh=	d3.select('div[id^='+id+']').append('svg').attr('id','svg_'+id).attr("class","bigsvg")
			  	.style('width','1800px').datum(barchartdata).transition().duration(500)
				.call(chart);
			}
			nv.utils.windowResize(chart.update);
			return chart;
		});
		resetBars=function () {
			var idd = this.id.replace('resetid','');
			c =$('div[id^='+id+']');
			//var w = $("#"+id+" > svg").css("width");
			//=============
			var barchartdata = [];
			barchartdata = barchartData(idd);
			var w = $("#"+idd+" > svg").css("width");
			d3.select('#'+idd+' svg').datum(barchartdata).transition().duration(500).call(chart);
		}
		var sortOrder ;
		sortBars = function () {
			var idd = this.id.replace('sortid','');
			/*c = document.getElementById(idd);*/
			c =$('div[id^='+id+']');
			//var w = $("#"+id+" > svg").css("width");
			//=============
				
				var barchartdata = [];
				barchartdata = barchartData(idd);
				
			//=============
		if( !sortOrder || sortOrder == null)
			sortOrder = true;
			else sortOrder = false;
			var historicalBarChart11 ;
			var barchartdata1 = JSON.parse(JSON.stringify(barchartdata));//cloning object
			historicalBarChart11 = barchartdata1[0].values;
		    historicalBarChart11.sort(function(a,b){
	    	if(sortOrder)
	    		return a["value"]-b["value"];//[1];
	    	else
	    		return b.value-a.value;//[1];
		    });
			for(var i=0;i<historicalBarChart11.length;i++){
				barchartdata1[0]["values"][i] = historicalBarChart11[i];
			}
			d3.select('#'+idd+' svg').datum(barchartdata1).transition().duration(500).call(chart);
		}
}
	
 function barchartData(id){
	 var jsonstring = $('#data_'+id).val();
	 var yval = new Array();
		var xval = new Array();
		var str = new Array();
		var keys = new Array();
		var yvalorig=new Array();
		keys[0] = "";
		str = jsonstring.split("~,");
		for (i in str) {
			xval[i] = str[i].substring(str[i].indexOf("{") + 1, str[i].indexOf(":"));
			yvalorig[i]=str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}"));
			yval[i] = parseFloat(str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}")));
		}
		
		/*var maxlen = 0;
		for (i=0; i<yvalorig.length; i++) {
		  if (yvalorig[i].length>maxlen) {
		    maxlen = yvalorig[i].length;
		  }
		}
		if(maxlen>=13)
			{
			var left=100;//130
			}
		else
			left=90;//90
*/	
		var barchartdata = [];
		var k = 0;
		//134 colors
		//var colors = new Array("#7fffd4","#0000ff","#8a2be2","#a52a2a","#deb887","#5f9ea0","#7fff00","#d2691e","#ff7f50","#6495ed","#fff8dc","#dc143c","#00ffff","#00008b","#008b8b","#b8860b","#006400","#a9a9a9","#bdb76b","#8b008b","#556b2f","#ff8c00","#9932cc","#8b0000","#e9967a","#8fbc8f","#483d8b","#2f4f4f","#00ced1","#9400d3","#ff1493","#00bfff",,"#696969","#1e90ff","#b22222","#fffaf0","#228b22","#ff00ff","#dcdcdc","#f8f8ff","#ffd700","#daa520","#008000","#adff2f","#808080","#f0fff0","#ff69b4","#cd5c5c","#4b0082","#fffff0","#f0e68c","#e6e6fa","#fff0f5","#7cfc00","#fffacd","#add8e6","#f080808","#e0ffff","#fafad2","#d3d3d3","#90ee90","#d3d3d3","#ffb6c1","#ffa07a","#20b2aa","#87cefa","#778899","#778899","#b0c4de","#ffffe0","#00ff00","#32cd32","#faf0e6","#ff00ff","#800000","#66cdaa","#0000cd","#ba55d3","#9370db","#3cb371","#7b68ee","#00fa9a","#48d1cc","#c71585","#191970","#f5fffa","#ffe4e1","#ffe4b5","#ffdead","#000080","#fdf5e6","#808000","#6b8e23","#ffa500","#ff4500","#da70d6","#eee8aa","#98fb98","#afeeee","#db7093","#ffefd5","#ffdab9","#cd853f","#ffc0cb","#dda0dd","#b0e0e6","#800080","#ff0000","#bc8f8f","#4169e1","#8b4513","#fa8072","#f4a460","#2e8b57","#fff5ee","#a0522d","#c0c0c0","#87ceeb","#6a5acd","#708090","#fffafa","#00ff7f","#4682b4","#d2b48c","#008080","#d8bfd8","#ff6347","#40e0d0","#ee82ee","#f5deb3","#ffffff","#f5f5f5","#ffff00","#9acd32");
		for ( var i = 0; i < keys.length; i++) {
			barchartdata.push({
				key : keys[i],
				values : []
			});
			for ( var j = 0; j < xval.length; j++) {
				xval[j] = xval[j].replace(/amp;/g, '');
			var v = yval[k];
			if(isNaN(v)){
			}
			else
			barchartdata[i].values.push({
				label : xval[j],
				value : yval[k],
		//	color : colors[j]
			});
			k++;
		}
		}
		return barchartdata;
 }
	
	
	function showbar(divid,jsonstring,xname,yname,layoutchoice,userType)
	{
		var jsonstring = $('#data_'+divid).val();
		//alert(widthValue);
		widthValue = getWidthValue(layoutchoice,userType);
		var yvalarea=new Array();
		
		var str=new Array();
		var xval=new Array();
		var earea = new Array();
		var arrarea = new Array();
		var testarea=new Array();
		
		var strxml="<graph  xAxisName="+"'"+xname+"'"+"  yAxisName="+"'"+yname+"'"+" yAxisMaxValue='1'>";
		
		strxml+="<categories>";
		
		var xvalarea=new Array();
		
		var t=jsonstring.split("],");
 		
 		
		 var  f=t[0].split("~,");

		 for(var i=0;i<f.length;i++)
		 {

		xvalarea[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));
		//alert("categories"+xvalarea[i]);
		strxml+="<category name="+"'"+xvalarea[i]+"'"+"/>";
		 }
        strxml+="</categories>";
		 var k = 0;
		 for(var i=0;i<t.length;i++)
		 {

		  testarea[i]=t[i].substring(t[i].indexOf("{"),t[i].lastIndexOf("}")+1);
		 //alert("testing i::::::::"+test[i] );
		 earea[i] = t[i].substring(0,t[i].indexOf("["));
		 //alert(e[i]);

		 //e[i] = test[i];
		 //alert("eeeeeeeeee"+e[i]);
		 var x = testarea[i].split(",");
		 //alert("X LENGTH :"+x.length);
		     for(var j=0;j<x.length;j++){

		        arrarea[k++] = x[j];
		 	}

		 }

		 var month = "";
		 var day="";
		 var b = new Array();
		 for(var n=0; n<arrarea.length;n++){
		 	b[n] =parseFloat( arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}")));
		 }
		 var k=0;        
		 for (i = 0; i < earea.length;i++ ) {
                 strxml+="<dataset seriesName="+"'"+earea[i]+"'"+" color="+"'"+get_random_color()+"'"+">";
			    for (j = 0; j < xvalarea.length;j++) {
			    	strxml+="<set value="+"'"+b[k]+"'"+"/>";
			    	k++;
			    }
			    strxml+="</dataset>"; 
			  }
		 strxml+="</graph>";
		
		// alert("xml after final"+strxml);
		//var strxmls="<graph ><categories><category name='08/01' /><category name='08/02' /></categories> <dataset seriesname='Product A'><set value='36634' /> <set value='43653' /> <set value='55565' /> </dataset><dataset seriesname='Product B'><set value='12152' /> <set value='15349' /> <set value='16442' /> </dataset></graph>";
		 var strxmls="<graph yAxisMaxValue='1'><categories><category name='08/01' /> <category name='08/02' /> </categories><dataset  seriesname='ProductA'><set value='36634' /> <set value='43653' /> <set value='55565' /> </dataset><dataset  seriesname='Product B' ><set value='12152' /> <set value='15349' /> <set value='16442' /> </dataset> </graph>";
		 if(layoutchoice==1)
		  {
	 
			 var chart= new FusionCharts("plugins/Charts/FCF_MSColumn2D.swf", "mychart", widthValue, "350", "0", "0");
	 
		  }
	  
	  if(layoutchoice==2)
	  {
  
	var chart= new FusionCharts("plugins/Charts/FCF_MSColumn2D.swf", "mychart",widthValue,"360", "0", "0");

	  }
	  
	  if(layoutchoice==3)
	  {

	var chart= new FusionCharts("plugins/Charts/FCF_MSColumn2D.swf", "mychart",widthValue, "360", "0", "0");

	  }
	  chart.setDataXML(strxml);
      chart.setTransparent(true);
      
      chart.render(divid);
		 
	}
		
	function MinimizeChart(id,imgid,sticky)
	{
		var item=document.getElementById(id);
		
	if(item.style.display=="block")
	{
		document.getElementById(imgid).src="images/dashboard/maximize.png"
		document.getElementById(imgid).title="maximize";
		item.style.display="none";
		$('#'+sticky).hide();
		showHide = "true";
	}
	else if(item.style.display=="none")
	{
		document.getElementById(imgid).src="images/dashboard/minimize.png"
			document.getElementById(imgid).title="minimize";
		item.style.display="block";
		
		$('#'+sticky).show();
		showHide = "false";
	}
		 
	}
	function frameChartbar(id,divid,xname,yname,layoutchoice,chartid,updateType,userType)//,mapdiv)//,bigdiv,bigchart)
	{
		var currentDiv=$('div[id^='+id+']');
		var jsonstring="" ;
		id=currentDiv.attr('id').split('@')[0];
		var currentlevel=currentDiv.attr('id').split('@')[1];
		
		if(currentlevel!=null && currentlevel!='undefined'){
			
			if($('input[id^='+chartid+']'+'[id*='+currentlevel+']').length>0)
				{
				jsonstring=	$('input[id^='+chartid+']'+'[id*='+currentlevel+']').val();
				}else{
				jsonstring=	$('#data_divid'+chartid).val();
				}
			
		}else{
			jsonstring= $('#data_'+id).val();
		}
		currentDiv.empty();
		if(showHide == "true"){
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "Please Maximize", 
	    	});
		}else{
		var brwsrSupport = getInternetExplorerVersion();
		/*var jsonstring = $('#data_'+id).val();*/
		if (brwsrSupport=='fus') 
		{
			
			widthValue = getWidthValue(layoutchoice,userType);
			if((jsonstring=="NA1")||(jsonstring=="NA2") ||(jsonstring == 'error'))
			{http://localhost:8080/SutiDAnalytics
				bootbox.alert({ 
					title: 'Alert',
		    	    size: 'small',
		    	    message: "chart can't be found for empty data", 
		    	});
			}
			else
				{
				if(updateType)
				{
				updateChartType(chartid,1);
				}
		var type=jsonstring.charAt(jsonstring.length-1);
		
		 if (type == 2) {
				showbar(divid, jsonstring, xname, yname, layoutchoice, userType);
			} else if (type == 1) {
				

				var yval = new Array();
				var xval = new Array();
				var str = jsonstring.split("~,");
				// alert("before"+str[0]);]
				for (i in str) {
					// alert("string functions");

					str[i] = str[i].substring(str[i].indexOf("{") + 1, str[i]
							.lastIndexOf("}"));
					// alert("string is"+str[i]);

					xval[i] = str[i].split(":")[0];

					yval[i] = parseFloat(str[i].split(":")[1]);

				}

				var strxml = "<graph  xAxisName="+"'"+xname+"'"+"  yAxisName="+"'"+yname+"'"+" yAxisMaxValue='1'>";
				
				for ( var j = 0; j < xval.length; j++) {
					strxml += "<set name=" + "'" + xval[j] + "'" + " "
							+ "value=" + "'" + yval[j] + "'" + " " + "color="
							+ "'" + get_random_color() + "'" + "/>";
				}
				var xml = strxml + "</graph>";
				// var state = document.getElementById(id).style.display;

				if (layoutchoice == 1) {
                 // alert("first layout");
					var chart = new FusionCharts(
							"plugins/Charts/FCF_Column2D.swf", "mychart",
							widthValue, "350", "0", "0");

				}

				if (layoutchoice == 2) {
					// alert("second layout");
					var chart = new FusionCharts(
							"plugins/Charts/FCF_Column2D.swf", "mychart",
							widthValue, "360", "0", "0");

				}

				if (layoutchoice == 3) {

					var chart = new FusionCharts(
							"plugins/Charts/FCF_Column2D.swf", "mychart",
							widthValue, "360", "0", "0");

				}
				chart.setDataXML(xml);
				chart.setTransparent(true);

				chart.render(divid);

			}

		}
	} else {
		$('div[id^='+id+']').css('display','block');
		
		if ((jsonstring == "NA1") || (jsonstring == "NA2") || (jsonstring == 'error')) {
			 bootbox.alert({ 
					title: 'Alert',
		    	    size: 'small',
		    	    message: "chart cannot be found for empty data", 
		    	});
		} else {
			if (updateType) {
				updateChartType(chartid, 1);
			}
			$('div[id^='+id+']').css('display','block');
			var type;
			var fcst;
			if(typeof jsonstring != 'undefined') {				
				type =jsonstring.charAt(jsonstring.length - 1);
				fcst=jsonstring.split("fcst")[1];
			}
			try{
				if(fcst != undefined)
					fcst = fcst.substring(0,fcst.length - 1);
			}catch(e){}
			jsonstring = jsonstring.split("fcst")[0];

			if (type == 1) {
					showbarsimple(id, jsonstring, xname, yname,chartid,layoutchoice,fcst);
				}
			
			else if(type==2||type==3)
			{
				var yval_length=0;
				var c = $('div[id^='+id+']'); 
				 while (c.firstChild) {//$('#'+id).siblings().css("display","none");
					$('#'+id).attr("class", "");
					c.removeChild(c.firstChild);}
				var yval=new Array();
				var xval=new Array();
				var str=new Array();
				var xvals=new Array();
				var e = new Array();
				var arr = new Array();
				var test=new Array();
				var a= new Array();
				var b = new Array();
				var t = "";
				var len=0;
				if(jsonstring.indexOf("],") == '-1')
				{
					t = jsonstring.split("],");	
				}
				else
				{
					t=jsonstring.split("],");
				}
				var f = t[0].split(",");
						for ( var i = 0; i < f.length; i++) {
		
							xvals[i] = f[i].substring(f[i].indexOf("{") + 1, f[i]
									.indexOf(":"));
						}
						var k = 0;
						for ( var i = 0; i < t.length; i++) {
		
							test[i] = t[i].substring(t[i].indexOf("{"), t[i]
									.lastIndexOf("}") + 1);
							e[i] = t[i].substring(0, t[i].indexOf("["));
							var x = test[i].split(",");
							for ( var j = 0; j < x.length; j++) {
								arr[k++] = x[j];
								}
							}
		
						var month = "";
						var day = "";
						for ( var n = 0; n < arr.length; n++) {
							a[n] = arr[n].substring(arr[n].indexOf("{") + 1, arr[n]
									.indexOf(":"));
							b[n] = parseFloat(arr[n].substring(arr[n].indexOf(":") + 1,
									arr[n].indexOf("}")));
		
							month = month + a[n] + " ";
							
							day = day + b[n] + " ";
							}
						var data = [];
						var k = 0;
						for ( var i = 0; i < e.length; i++) {
							data.push({
								key : e[i],
								values : []
							});
							//var notnulls=0;if(!isNaN(b[k]))notnulls++;
							
						for ( var j = 0; j < xvals.length; j++) {
							xvals[j] = xvals[j].replace(/amp;/g, '');
							var v = b[k];
							yval_length = yval_length < v.toString().length ? v.toString().length : yval_length; 
							if(isNaN(v)){//alert("null");
							}else
							data[i].values.push({
								label : xvals[j],
								value : parseFloat(b[k])
							});
							k++;
						}
						len=len+xvals.length;//notnulls
					}		             //if(layoutchoice==1){
						nv.addGraph(function() {
							var chart = nv.models.multiBarChart1().x(function(d) {
								return d.label;
							}).y(function(d) {
								return d.value;
							});
							if(yval_length>10)
								chart.margin({left: 120});
							// .staggerLabels(true)
							// .tooltips(false)
							// .showValues(true);
							chart.yAxis.axisLabel(yname);
							chart.yAxis.tickFormat(d3.format('.2f'));
							chart.xAxis.axisLabel(xname);
							chart.showControls(false);//to remove stacked/group functionality
							if(fcst != undefined) chart.forcast = fcst;
							//document.getElementById(id).style.width = document.getElementById(id).getBoundingClientRect().width+'px';
							$('div[id^='+id+']').empty();
							if(len>15){//&&layoutchoice==1
								var wid = 1;
								chart.xAxis.rotateLabels(-15);
								chart.margin({bottom: 85});
								//d3.select("#"+id).append('div').attr("class","bigi"+layoutchoice);
								d3.select('div[id^='+id+']').append('div').attr("class","table-responsive");
								 if(layoutchoice=='1'){
									 wid = 40 * len;//xvals.length;
										if(len>30){// || (xval.length>15 && layoutchoice=='2'))//||(xval.length>8 && layoutchoice == "3"))
											d3.select("#"+id+' div').append('svg').attr('id','svg_'+id).attr("class","bigsvgtype2").style('width','3000px').datum(data).transition().duration(500).call(chart);
										}else{
											d3.select("#"+id+' div').append('svg').attr('id','svg_'+id).attr("class","bigsvgtype2").style('width','3000px').datum(data).transition().duration(500).call(chart);
										}
								 }else {
									 wid = 40 * len;//xvals.length;
									 //d3.select("#"+id+' div').attr("class","bigi"+layoutchoice);
									 chart.xAxis.rotateLabels(-15);
									 
									 if(len>15){// || (xval.length>15 && layoutchoice=='2'))//||(xval.length>8 && layoutchoice == "3"))
											d3.select("#"+id+' div').append('svg').attr('id','svg_'+id).attr("class","bigsvgtype2").style('width','3000px').datum(data).transition().duration(500).call(chart);
											}else{
											d3.select("#"+id+' div').append('svg').attr('id','svg_'+id).attr("class","bigsvgtype2").style('width','3000px').datum(data).transition().duration(500).call(chart);
											}
								 }
							}else{
								d3.select('#' + id).append('div').attr("class","table-responsive").append('svg').attr('id','svg_'+id).attr("class","simplediv"+layoutchoice).datum(data).transition().duration(500)
								.call(chart);
							}
							//====legend start
								var legend ;
								//legend = nv.models.legend1();
								var divlgnd = d3.select('#' + id).append('div').attr('class','legenddiv');
								var contnr = divlgnd.attr('id','lgdiv_'+id).append('svg');//.style('width','200px').style('height','440px');
								contnr.attr('id','lgsvg_'+id);
								var vrap = contnr.selectAll('g.nv-wrap.nv-multiBarWithLegend').data([data]);//.append('g').attr('class','nv-legendWrap').datum(data).call(legend);
								 var gntr = vrap.enter().append('g').attr('class', 'nvd3 nv-wrap nv-multiBarWithLegend').append('g').attr('id','lgnd_g_'+id);
								 var gg = vrap.select('g');
								 gntr.append('g').attr('class', 'nv-legendWrap');
								 
								 if(layoutchoice==1 ){
									 	legend = nv.models.legend1().layoutChoice(1);
										/*divlgnd.attr('class','legenddiv2');
									 	contnr.attr('class','legendsvg2');*/
									 	legend.width(900);
									 }
								 else if(layoutchoice==2 ){
									 	legend = nv.models.legend1().layoutChoice(2);
										divlgnd.attr('class','legenddiv2');
									 	contnr.attr('class','legendsvg2');
									 	legend.width(500);
									 }
								 else if(layoutchoice==3 ){
									divlgnd.attr('class','legenddiv3');
									contnr.attr('class','legendsvg3');
									legend = nv.models.legend1().width(330);
									legend.layoutChoice(3);
								 }
								 gg.select('.nv-legendWrap')
						            .datum(data)
						            .call(legend);
								 if($('.nv-series').length <= 3) {
										$('#lgsvg_'+id).closest('.legenddiv').css({overflow: 'hidden', height: '20px'});
								 }
								legend.dispatch.on('legendClick', function(d,i) {
							        try{
									d.disabled = !d.disabled;
							        }catch(e){}
	
							        if (!data.filter(function(d) { return !d.disabled }).length) {
							          data.map(function(d) {
							            d.disabled = false;
							            vrap.selectAll('.nvd3 .nv-legend .nv-series').classed('disabled', false);
							            return d;
							          });
							        }
							        d3.select('#lgdiv_'+id+' svg').datum(data).call(legend);
							        d3.select('#'+id+' svg').datum(data).transition().duration(500).call(chart);
							      });
								//$("#"+id).append($("#"+id+" div").get().reverse());
								//d3.select('#lgsvg_'+id).style('height',document.getElementById('lgnd_g_'+id).getBoundingClientRect().height+'px');
							//====legend end
								nv.utils.windowResize(chart.update);
							return chart;
						});
		                	 /*}
		                 else if(layoutchoice==2||layoutchoice==3)
		                	 {
		                	 nv.addGraph(function() {
		     					var chart = nv.models.multiBarChart1().x(function(d) {
		     						return d.label;
		     					}).y(function(d) {
		     						return d.value;
		     					});
		     					// .staggerLabels(true);
		     					// .tooltips(false)
		     					// .showValues(true);
		     					chart.yAxis.axisLabel(yname);
		     					chart.yAxis.tickFormat(d3.format('.2f'));
		     					chart.xAxis.axisLabel(xname);
		     					chart.xAxis.rotateLabels(-15);
		     					chart.margin({bottom: 80});
		
		     					d3.select('#' + id).append("svg").datum(data).transition().duration(500)
		     							.call(chart);
		
		     					return chart;
		     				});
		                	 }*/
				}
			}
	     }
	}
	}
 	
	 function addDashletsToLayout()
		{
			 var dsblayoutId = document.getElementById('dsblayoutId').value;
			 $.post('addDashletsToLayout.action?dsblayoutId='+dsblayoutId, function(result) {
				 document.getElementById('customizeLayoutPopup').innerHTML = result;
					$.blockUI({
						message : $('#customizeLayoutPopup'),
						css : {
							top : ($(window).height() - 900) / 3 + 'px',
							left : ($(window).width() - 400) / 3 + 'px',
							border : 'none',
							width : '0px',
							height : '0px'
						}
					});
				});
		}
	function customizeLayout()
	{
		 var dsblayoutId = document.getElementById('dsblayoutId').value;
		 $.post('showLayoutPopUp.action?dsblayoutId='+dsblayoutId, function(result) {
			 document.getElementById("customizeLayoutPopup1").innerHTML = result;
				$.blockUI({
					message : $('#customizeLayoutPopup1'),
					css : {
						top : ($(window).height() - 900) / 3 + 'px',
						left : ($(window).width() - 400) / 3 + 'px',
						border : 'none',
						width : '0px',
						height : '0px'
					}
				});
			});
	}
	function deleteDashboardLayoutIntegration(){
		var dsblayoutId = document.getElementById('dsblayoutId').value;
		if(dsblayoutId > 0){
			bootbox.confirm({ 
        		title: 'Delete Dashboard',
        	    size: 'small',
        	    message: "Are you sure,You want to delete dashboard?", 
        	    callback: function(r){ 
        	    	if(r){
        	    		document.forms["dsbforms"].action = "deleteLayout.action";
						document.forms["dsbforms"].submit();
                        return true;
                    }
        	    }
        	});
		} else {
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: 'No dashboard layout selected to delete'
	    	});
		}
	}
	function createNewLayout(){
	    document.forms["dsbforms"].action = "appNewDsbLayout.action";
		document.forms["dsbforms"].submit();
    }
	 
	function editChartDetails(chart,test)
	{
		//alert("daHelper.js");
		
		if(test == 'NO'){
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "This chart can't be editable", 
	    	});
		}else{
			var dsblayoutId = document.getElementById('dsblayoutId').value; 
			$("body").mask("");
			$.post('editChartDetails.action?chartId='+chart+'&dsblayoutId='+dsblayoutId, function(result) {
				 //document.getElementById('customizeLayoutPopup').innerHTML = result;
				var div2= $('<div>').addClass('centerpopup').html(result);
				 //dateBlockQuery();
				 jQuery("body").unmask();
				 $.blockUI({
						message : $(div2),
						css : {
							top : ($(window).height() - 900) / 3 + 'px',
							left : ($(window).width() - 400) / 3 + 'px',
							border : 'none',
							width : '0px',
							height : '0px'
						}
					});
				});
			}
		}
	function showTypesOfCharts(id,layoutChoice,xaxisName,yaxisName,
			xmlData,isTimeCol,measureName,bins,chartName,userLayoutId,lastYear,dsblayoutId){
			
			$.post('chartDetailsPopup.action?chartId='+id+'&layoutChoice='+layoutChoice+
					'&xaxisName='+xaxisName+'&yaxisName='+yaxisName+ //'&xmlData='+xmlData+
					'&isTimeCol='+isTimeCol+'&measureName='+measureName+
					'&bins='+bins+'&chartName='+chartName+'&=userLayoutId'+userLayoutId+
					'&lastYear='+lastYear+'&dsblayoutId='+dsblayoutId, function(result) {
				var div2= $('<div>').addClass('centerpopup').html(result);
				 $.blockUI({
						message : $(div2),
						css : {
							top : ($(window).height() - 900) / 3 + 'px',
							left : ($(window).width() - 400) / 3 + 'px',
							border : 'none',
							width : '0px',
							height : '0px'
						}
					});
				});
		}
	function showTypesOfExports(id,xmlData,chartName,xaxisName,yaxisName,chartTypeid){
		
			$.post('exportDetailsPopup.action?chartId='+id+ //'&xmlData='+xmlData+
					'&chartName='+chartName+'&xaxisName='+xaxisName+'&yaxisName='+yaxisName+
					'&chartTypeid='+chartTypeid, function(result) {
				var div2= $('<div>').addClass('centerpopup').html(result);
				 $.blockUI({
						message : $(div2),
						css : {
							top : ($(window).height() - 900) / 3 + 'px',
							left : ($(window).width() - 400) / 3 + 'px',
							border : 'none',
							width : '0px',
							height : '0px'
						}
					});
				});
		}
	
	
	function editChartDetails1(chart,test,clanId)
	{
		var test = $('#data_'+chart).val();
		//alert("*** " + clanId);
		if(test == 'NO'){
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "This chart can't be editable", 
	    	});
		}else{
			jQuery("body").mask("");
			var dsblayoutId = 0;//document.getElementById('dsblayoutId').value; 
			$.post('editChartDetails1.action?chartId='+chart+'&dsblayoutId='+dsblayoutId+'&clanId='+clanId, function(result) {
				 //document.getElementById('DivId_ChartDisplay').innerHTML = result;
				var div2= $('<div>').addClass('centerpopup').html(result);
				jQuery("body").unmask();
				 //dateBlockQuery();
				 $.blockUI({
						message : $(div2),
						css : {
							top : ($(window).height() - 900) / 3 + 'px',
							left : ($(window).width() - 400) / 3 + 'px',
							border : 'none',
							width : '0px',
							height : '0px'
						}
					});
				});
			}
		}
		
	
	function chartForecasting(chart,jsonstring,time)
	{
		if(jsonstring == 'NO'){
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "Forecasting not supported for this chart", 
	    	});
		}else{
			if(time == 'N'){
				bootbox.alert({ 
					title: 'Alert',
		    	    size: 'small',
		    	    message: "Forecasting not supported for this chart", 
		    	});
			}else{
			document.getElementById("forcastId").value = jsonstring;
			var xvalarea=new Array();
			/*var strFlag = false;
			 var t=jsonstring.split("],");
			 var  f=t[0].split("~,");
			 var strFlag=false;
			 for(var i=0;i<f.length;i++)
			 {
				xvalarea[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));
				if(isNaN(xvalarea[i]))
				{
				strFlag = true;
				}
		   }
			
			 if(strFlag)
				 {
				 //alert("X-Axis should be numeric for Forecasting");
				 }
			 else{*/
			var dsblayoutId = document.getElementById('dsblayoutId').value; 
			$("body").mask("");
			$.post('editChartDetails.action?chartId='+chart+'&dsblayoutId='+dsblayoutId+'&acnType=forecasting', function(result) {
				 document.getElementById('customizeLayoutPopup').innerHTML = result;
				 //dateBlockQuery();
				 jQuery("body").unmask();
				 $.blockUI({
						message : $('#customizeLayoutPopup'),
						css : {
							top : ($(window).height() - 900) / 3 + 'px',
							left : ($(window).width() - 400) / 3 + 'px',
							border : 'none',
							width : '0px',
							height : '0px'
						}
					});
				});
			}
		}
		}
	function mailReportDashboard(dsblayoutId)
	{
		$.post('showReportMailDashboard.action?dsblayoutId='+dsblayoutId, function(result) {
			 document.getElementById('customizeLayoutPopup').innerHTML = result;
			 //jQuery("body").unmask();
			 $.blockUI({
					message : $('#customizeLayoutPopup'),
					css : {
						top : ($(window).height() - 900) / 3 + 'px',
						left : ($(window).width() - 400) / 3 + 'px',
						border : 'none',
						width : '0px',
						height : '0px'
					}
				});
			});
	}
	
	function mailReport(chartId,source)
	{
		var currentDiv=$('div[id^=divid'+chartId+']');
		var data="" ;
		id=currentDiv.attr('id').split('@')[0];
		var currentlevel=currentDiv.attr('id').split('@')[1];
		
		if(currentlevel!=null && currentlevel!='undefined'){
			
			if($('input[id^='+chartId+']'+'[id*='+currentlevel+']').length>0)
				{
				data=	$('input[id^='+chartId+']'+'[id*='+currentlevel+']').val();
				xmlData=data;
				}else{
				data=	$('#data_divid'+id).val();
				xmlData=data;
				}
		}else{
			data= $('#data_'+id).val();
			xmlData=data;
		}
		currentDiv.empty();
		/*var data = $('#data_divid'+chartId).val();*/
		var dsblayoutId = document.getElementById('dsblayoutId').value; 
		//jQuery("body").mask("Processing...");
		if(data == 'NA1' || data == 'NA2' || data == 'error'){
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "Empty chart can't be mailed", 
	    	});
		}else if(data == 'NO'){
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "This chart can't be mailed", 
	    	});
		}
		else{
		$.post('showReportMail.action?chartid='+chartId+'&dsblayoutId='+dsblayoutId+'&data='+encodeURIComponent(data)+'&mailSource='+source, function(result) {
			 document.getElementById('customizeLayoutPopup').innerHTML = result;
			 //jQuery("body").unmask();
			 $.blockUI({
					message : $('#customizeLayoutPopup'),
					css : {
						top : ($(window).height() - 900) / 3 + 'px',
						left : ($(window).width() - 400) / 3 + 'px',
						border : 'none',
						width : '0px',
						height : '0px'
					}
				});
			});
		 //  }
		}
	}
	
	function sendDashboardDetails(){
		var param = validateSendMailDashboard();
		if(param){
			$("#sendDashboardMail").submit(function(event) {
				event.preventDefault();
				var $form = $(this);
				var url1 = $form.attr('action');
				var data1 = $("#sendDashboardMail").serialize();
				jQuery("#mailPopupDivId").mask("");
				$.ajax({
			        type: "POST",
			        url : url1, 
			        cache: false,
			        timeout: 60000,
			        processData: false,
			        data: data1,
			        dataType: "text",
			        success: function(result) {
			        	/*document.getElementById('customizeLayoutPopup').innerHTML = result;
			        	$.blockUI({
							message : $('#customizeLayoutPopup'),
							css : {
								top : ($(window).height() - 900) / 3 + 'px',
								left : ($(window).width() - 400) / 3 + 'px',
								border : 'none',
								width : '0px',
								height : '0px'
							}
						});*/
			        	if(result.indexOf('sessionExpired') != -1) {
			        		window.location = "unAuthorizedPage.jsp";
			        	} else {
			        		$.unblockUI({});
			        		var message = result.split('@@')[0];
			        		var type = result.split('@@')[1];
			        		$( "#custerror" ).text('');
			        		if(type == 'GREEN') {
			        			$( "#custerror" ).addClass( "alert alert-success");
			        		} else if(type == 'RED') {
			        			$( "#custerror" ).addClass("alert alert-danger");
			        		} else if(type == 'ORANGE') {
			        			$( "#custerror" ).addClass("alert alert-warning");
			        		}
			        		$('#custerror').append(message);
			        		$( "#custerror" ).css( "display", "block" );
			        	}
			        },
			        error: function (data, status, e)
			        {
			            if(status == "timeout"){
			                //alert("timeout"+e);
			            	jQuery("#mailPopupDivId").unmask();
			            }else{
			               // alert(status);
			            	jQuery("#mailPopupDivId").unmask();
			            }
			        }
				});
			});
		}
	}

	function sendReportDetails() {
	
		var chartid;
		chartid= $('#divid'+document.getElementById('chartidsdadadad').value+' > svg');
		if(!chartid[0])
			chartid= $('#divid'+document.getElementById('chartidsdadadad').value+' > div >svg');
		//chartid = chartid[0].serialize();chartid[0]
		//
		var param = validateSendMailReport();
		if(param){
			$("#sendReportMail").submit(function(event) {
				//var id = "image"+chartid;
				event.preventDefault();
				/*var browser = getInternetExplorerVersion();
				if(browser == 'nvd'){
					//alert("Browser supported");
					var svgString = new XMLSerializer().serializeToString(chartid[0]);
					
					var canvas = document.createElement('canvas');
					canvas.setAttribute('style', 'display: none;');
		
					document.body.appendChild(canvas);
				    if (context)
				    	canvas.getContext = context;
				    canvg(canvas, svgString, {ignoreClear: true});
				    var imageContent = canvas.toDataURL("image/png");
				    document.getElementById('imageContent1').value = unescape(encodeURIComponent(imageContent));
				}else{
					document.getElementById('imageContent1').value = "NO";
					//alert("Browser not supported");
				}*/
				var $form = $(this);
				var url1 = $form.attr('action');
				var data1 = $("#sendReportMail").serialize();
				//var newurl = url1 + "?" + data1 ;
				jQuery("#mailPopupDivId").mask("");
				$.ajax({
			        type: "POST",
			        url : url1, 
			        cache: false,
			        timeout: 60000,
			        processData: false,
			        data: data1,
			        dataType: "text",
			        success: function(result) {
			        	document.getElementById('customizeLayoutPopup').innerHTML = result;
			        	$.blockUI({
							message : $('#customizeLayoutPopup'),
							css : {
								top : ($(window).height() - 900) / 3 + 'px',
								left : ($(window).width() - 400) / 3 + 'px',
								border : 'none',
								width : '0px',
								height : '0px'
							}
						});
			        },
			        error: function (data, status, e)
			        {
			            if(status == "timeout"){
			                //alert("timeout"+e);
			            	jQuery("#mailPopupDivId").unmask();
			            }else{
			               // alert(status);
			            	jQuery("#mailPopupDivId").unmask();
			            }
			        }
				});
			});
		}
	}
	
	function validateMailForm() {
		var param = $("#sendReportMail").validate({
			rules : {
				toAddress: {
	                required: true,
	                email: true
	            },
	            chartNames : {
					required : true
				}
			},
			messages : {
				toAddress : {
					required : "<br>Email is required\n",
						email : "<br>Invalid email address"
				},
				chartNames : {
					required : "<br>Report name is required\n"
				}
			}
		}).form();
		return param;
	}
	function getCharts(chartId,svgid,httpVal)
	{
		var loadingid = svgid+"id";
		//alert("Hello"+loadingid);
		document.getElementById(loadingid).style.display='block';
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			httpVal = new XMLHttpRequest();
		} else {// code for IE6, IE5
			httpVal = new ActiveXObject("Microsoft.XMLHTTP");
		}
		httpVal.onreadystatechange = function() {
			if (httpVal.readyState == 4 && httpVal.status == 200) {
				document.getElementById(divid).innerHTML = httpVal.responseText;
			}
		};

		httpVal.open("GET", "getChartData.action?datasetId="+ chartId, true);
		httpVal.send();
		
		httpVal.onreadystatechange = function() {
			if (httpVal.readyState == 4 && httpVal.status == 200) {
				var chartValue = null;
				chartValue = httpVal.responseText;
				if(chartValue=='SESSION EXPIRED')
				{
				window.location ="SessionExpiredPage.jsp";
				flag = false;
				}
				else if(chartValue=='NA')
					{
					document.getElementById(loadingid).style.display='none';
					
					}
				else
				{
					var xaxisName =" "; //chartValue.split("@@@",0);
					var yaxisName = " ";
					var chartData = chartValue;//.split("@@@",2);
					document.getElementById(loadingid).style.display='none';
					frameChartsHomePage(svgid,chartData,xaxisName,yaxisName);
				}

			}
			httpVal.responseText="";
		};
	}
	
	
	
	function frameChartsHomePage(id,xName,yName){
		$("#"+id).empty();
		/*var state = document.getElementById(id).style.display;
        if (state == 'none') {
            document.getElementById(id).style.display = 'block';
        } */
		$('#'+id).css('display','block');
        var jsonstring = $('#data_'+id).val();
	var type=jsonstring.charAt(jsonstring.length-1);
	if(type==1)
	 {
		var c = document.getElementById(id);
		while (c.firstChild) {
			//$('#'+id).siblings().css("display","none");
			$('#'+id).attr("class", "table-responsive");
			c.removeChild(c.firstChild);
		}
		var yval = new Array();
		var xval = new Array();
		var str = new Array();
		var keys = new Array();
		keys[0] = "";
		str = jsonstring.split(",");
		for (i in str) {

			str[i] = str[i].substring(str[i].indexOf("{") + 1, str[i]
					.lastIndexOf("}"));

			xval[i] = str[i].split(":")[0].trim();
			yval[i] = parseFloat(str[i].split(":")[1]);

		}
		var barchartdata = [];
		var k = 0;
		for ( var i = 0; i < keys.length; i++) {
			barchartdata.push( {
				key : keys[i],
				values : []
			});

			for ( var j = 0; j < xval.length; j++) {
				xval[j] = xval[j].replace(/amp;/g, '');
				barchartdata[i].values.push( {
					label : xval[j],
					value : yval[k]
				});
				k++;
			}
		}
		nv.addGraph(function() {
			var chart = nv.models.discreteBarChart().x(function(d) {
				return d.label;
			})

			.y(function(d) {
				return d.value;
			}).staggerLabels(true)
			.tooltips(false).showValues(false);
			
			
			d3.select('#' + id).append("svg").style('height','440px').datum(barchartdata).transition().duration(500)
					.call(chart);
			nv.utils.windowResize(chart.update);
			return chart;
		});
	}
	if(type==2)
		{
	var yval=new Array();
	var xval=new Array();
	var str=new Array();
	var xvals=new Array();
	var e = new Array();
	var arr = new Array();
	var test=new Array();
	var a= new Array();
	var b = new Array();
	var t = "";
	if(jsonstring.indexOf("],") == '-1')
	{
		t = jsonstring.split("]");	
	}
	else
	{
		t=jsonstring.split("],");
	}
	
	
var  f=t[0].split(",");
for(var i=0;i<f.length;i++)
{

xvals[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));

}

var k = 0;
for(var i=0;i<t.length;i++)
{

test[i]=t[i].substring(t[i].indexOf("{"),t[i].lastIndexOf("}")+1);
e[i] = t[i].substring(0,t[i].indexOf("["));
var x = test[i].split(",");
for(var j=0;j<x.length;j++){

   arr[k++] = x[j];
}

}

var month = "";
var day="";
for(var n=0; n<arr.length;n++){

a[n] = arr[n].substring(arr[n].indexOf("{")+1,arr[n].indexOf(":"));
b[n] =parseFloat( arr[n].substring(arr[n].indexOf(":")+1,arr[n].indexOf("}")));

month =month + a[n] + " ";
day = day + b[n]+ " " ;


}
var data = [];
var k = 0;
for ( var i = 0; i < e.length; i++) {
data.push({
	key : e[i],
	values : []
});

for ( var j = 0; j < xvals.length; j++) {
	xvals[j] = xvals[j].replace(/amp;/g, '');
	data[i].values.push({
		label : xvals[j],
		value : parseFloat(b[k])
	});
	k++;
}			
}

nv.addGraph(function() {
var chart = nv.models.multiBarChart().x(function(d) {
	return d.label;
}).y(function(d) {
	return d.value;
});
chart.yAxis.axisLabel(yName)


chart.xAxis.axisLabel(xName)



d3.select('#' + id).append("svg").datum(data).transition().duration(500).call(chart);

nv.utils.windowResize(chart.update);

return chart;
}); 
}
		}
	
	function frameChartbubble(id,divid,xname,yname,layoutchoice,chartid,updateType,userType,timeCol)//,mapdiv,bigdiv,bigchart)
	{
		var currentDiv=$('div[id^='+id+']');
		var jsonstring="" ;
		id=currentDiv.attr('id').split('@')[0];
		var currentlevel=currentDiv.attr('id').split('@')[1];
		
		if(currentlevel!=null && currentlevel!='undefined'){
			
			if($('input[id^='+chartid+']'+'[id*='+currentlevel+']').length>0)
				{
				jsonstring=	$('input[id^='+chartid+']'+'[id*='+currentlevel+']').val();
				}else{
				jsonstring=	$('#data_divid'+chartid).val();
				}
			
		}else{
			jsonstring= $('#data_'+id).val();
		}
		currentDiv.empty();
		if(showHide == "true"){
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "Please Maximize", 
	    	});
		}else{
		var testxval=new Array();
		/*var jsonstring = $('#data_'+id).val();*/
		var yvalarea=new Array();
		var str=new Array();
		var xval=new Array();
		var earea = new Array();
		var arrarea = new Array();
		var testarea=new Array();
		var xvalarea=new Array();
		
		var brwsrSupport = getInternetExplorerVersion();
		if (brwsrSupport=='fus') 
		{
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "This feature is not supported in Internet Explorer 8.0", 
	    	});
		}
		else
			{
			if((jsonstring=="NA1")||(jsonstring=="NA2") || (jsonstring == 'error'))
			{
				bootbox.alert({ 
					title: 'Alert',
		    	    size: 'small',
		    	    message: "chart can't be found for empty data", 
		    	});
			}
			else
				{
				
				/*var state=document.getElementById("id");
				if(state==null)
					{
					 state='none'
					}*/
				
		
		 var t=jsonstring.split("],");
 		
 		
 var  f=t[0].split("~,");
 var strFlag=false;

 for(var i=0;i<f.length;i++)
 {

xvalarea[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));
if(isNaN(xvalarea[i]))
{
	strFlag = true;
}
 
 }
//alert("flag::::::is"+strFlag);
 var k = 0;
 for(var i=0;i<t.length;i++)
 {

  testarea[i]=t[i].substring(t[i].indexOf("{"),t[i].lastIndexOf("}")+1);
 //alert("testing i::::::::"+test[i] );
 earea[i] = t[i].substring(0,t[i].indexOf("["));
 //alert(e[i]);
 if(earea[i]=="")
 {
 
 earea[i]=yname;
 }

 //e[i] = test[i];
 //alert("eeeeeeeeee"+e[i]);
 var x = testarea[i].split(",");
 //alert("X LENGTH :"+x.length);
     for(var j=0;j<x.length;j++){

        arrarea[k++] = x[j];
 	}

 }

 var month = "";
 var day="";
 var b = new Array();
 var yvalorig=new Array();
 for(var n=0; n<arrarea.length;n++){

   
 	b[n] =parseFloat( arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}")));
 	yvalorig[n]=arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}"));
 }
			}

			var maxlen = 0;
			for (i=0; i<yvalorig.length; i++) {
				//alert("length yval is"+yval.length);
				
			  if (yvalorig[i].length>maxlen) {
				  //alert("test length");
			    maxlen = yvalorig[i].length;
			   
			  }
			  
			}

			if(maxlen>=13)
				{
				var left=160;
				}
			else
				left=100;
		if(layoutchoice==1)
		{
		var width;
		if(userType=='all')
			{
			width=800;
		}
		if(userType=='indi')
			{
			width=1100;
			}
		}
		if(layoutchoice==2)
		{
		 var width;
			if(userType=='all')
				{
				width=370;
			}
			if(userType=='indi')
				{
				width=500;
				}
		}
		 if(layoutchoice==3)
			{
			 var width;
				if(userType=='all')
					{
					width=250;
				}
				if(userType=='indi')
					{
					
					width=300;
					}
			}
		 
		 if(strFlag==true)
			 {
			 if(xval.length>15&&layoutchoice==1)
				{
				 alert(document.getElementById(id).style.display);
				 $('div[id^='+id+']').style.display='none';
				 
				 document.getElementById(bigdiv).style.display='block';
				 document.getElementById(bigchart).style.display='block';
				
				}
			// document.getElementById(id).style.display = 'none';
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "X Axis chart should be Numeric for bubble chart", 
	    	});
			
			 }
		 else if(strFlag==false)
			 {
			 if(updateType)
				{
				updateChartType(chartid,4);
				}
			 //var state = document.getElementById(id).style.display;
			 var c = $('div[id^='+id+']');
			 while (c.firstChild) {
				 $('div[id^='+id+']').attr("class", "table-responsive");
				// $('#'+id).siblings().css("display","none");
					c.removeChild(c.firstChild);
				}
				/*if (state == 'none') {
					document.getElementById(id).style.display = 'block';
				}*/
			 $('div[id^='+id+']').css('display','block');
				
			//alert("helloo It is time column");
			 var xval=new Array();
			 var yval=new Array();
			
			 nv.addGraph(function() {
				  var chart = nv.models.scatterChart()
				                .showDistX(true)
				                .showDistY(true)
				                //.height(500)
				                .useVoronoi(true)
				                .color(d3.scale.category10().range());

                    if(timeCol=="Y")
                    	{
                    	
				  chart.xAxis.tickFormat(d3.format('d'));
				  chart.yAxis.tickFormat(d3.format('d'));
                    	}
                    else if(timeCol=="N")
                    	{
                    	 chart.xAxis.tickFormat(d3.format('.2f'));
       				  chart.yAxis.tickFormat(d3.format('.2f'));
                    	}
				  chart.xAxis.axisLabel(xname);
				  chart.showControls(false);//to remove stacked/group functionality
					chart.yAxis.axisLabel(yname);
					//chart.width(width);
					chart.margin({top: 15, right: 35, bottom: 50, left:left});

				  d3.select('div[id^='+id+']').append("svg")
				      .datum(randomData(jsonstring,yname))
				    .transition().duration(500)
				      .call(chart);

				  nv.utils.windowResize(chart.update);

				  return chart;
				});
			 }
			}
	}
	}
	function  showhorizontalbar(divid,xname,yname,layoutchoice,userType)
	{
	/*var state = document.getElementById(divid).style.display;
	if (state == 'none') {
        document.getElementById(divid).style.display = 'block';
    }*/
		$('#'+id).css('display','block');
	var jsonstring = $('#data_'+divid).val();
	var yvalarea=new Array();
	widthValue = getWidthValue(layoutchoice,userType);
	var str=new Array();
	var xval=new Array();
	var earea = new Array();
	var arrarea = new Array();
	var testarea=new Array();
	
	var strxml="<graph xAxisName="+"'"+xname+"'"+"  yAxisName="+"'"+yname+"'"+" yAxisMaxValue='1'>";
	strxml+="<categories>";
	
	var xvalarea=new Array();
	
	var t=jsonstring.split("],");
		
		
	 var  f=t[0].split("~,");

	 for(var i=0;i<f.length;i++)
	 {

	xvalarea[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));
	//alert("categories"+xvalarea[i]);
	strxml+="<category name="+"'"+xvalarea[i]+"'"+"/>";
	 }
    strxml+="</categories>";
	 var k = 0;
	 for(var i=0;i<t.length;i++)
	 {

	  testarea[i]=t[i].substring(t[i].indexOf("{"),t[i].lastIndexOf("}")+1);
	 //alert("testing i::::::::"+test[i] );
	 earea[i] = t[i].substring(0,t[i].indexOf("["));
	 //alert(e[i]);

	 //e[i] = test[i];
	 //alert("eeeeeeeeee"+e[i]);
	 var x = testarea[i].split(",");
	 //alert("X LENGTH :"+x.length);
	     for(var j=0;j<x.length;j++){

	        arrarea[k++] = x[j];
	 	}

	 }

	 var month = "";
	 var day="";
	 var b = new Array();
	 for(var n=0; n<arrarea.length;n++){

	   
	 	b[n] =parseFloat( arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}")));
	 	
	 	
	 	
	    
	 }
	 var k=0;
	 
	 for (i = 0; i < earea.length;i++ ) {
		  
             strxml+="<dataset seriesName="+"'"+earea[i]+"'"+" color="+"'"+get_random_color()+"'"+">";
		    for (j = 0; j < xvalarea.length;j++) {

                   
			
strxml+="<set value="+"'"+b[k]+"'"+"/>";
k++;
		     
		    }
		    strxml+="</dataset>"; 
			
		  }
	 strxml+="</graph>";
	
	 
	//var strxmls="<graph ><categories><category name='08/01' /><category name='08/02' /></categories> <dataset seriesname='Product A'><set value='36634' /> <set value='43653' /> <set value='55565' /> </dataset><dataset seriesname='Product B'><set value='12152' /> <set value='15349' /> <set value='16442' /> </dataset></graph>";
	 var strxmls="<graph yAxisMaxValue='1'><categories><category name='08/01' /> <category name='08/02' /> </categories><dataset  seriesname='ProductA'><set value='36634' /> <set value='43653' /> <set value='55565' /> </dataset><dataset  seriesname='Product B' ><set value='12152' /> <set value='15349' /> <set value='16442' /> </dataset> </graph>";
	 if(layoutchoice==1)
	  {
 
  var chart= new FusionCharts("plugins/Charts/FCF_MSBar2D.swf", "mychart",widthValue, "350", "0", "0");
 
	  }
  
  if(layoutchoice==2)
  {

var chart= new FusionCharts("plugins/Charts/FCF_MSBar2D.swf", "mychart",widthValue,"360", "0", "0");

  }
  
  if(layoutchoice==3)
  {

var chart= new FusionCharts("plugins/Charts/FCF_MSBar2D.swf", "mychart",widthValue, "360", "0", "0");

  }
  chart.setDataXML(strxml);
  chart.setTransparent(true);
  
  chart.render(divid);
	 
}
	
	
	function frameCharthorizontalBar(id,divid,xname,yname,layoutchoice,chartid,updateType,userType)//,mapdiv)//,bigdiv,bigchart)
	{
		
		
		var currentDiv=$('div[id^='+id+']');
		var jsonstring="" ;
		id=currentDiv.attr('id').split('@')[0];
		var currentlevel=currentDiv.attr('id').split('@')[1];
		
		if(currentlevel!=null && currentlevel!='undefined'){
			
			if($('input[id^='+chartid+']'+'[id*='+currentlevel+']').length>0)
				{
				jsonstring=	$('input[id^='+chartid+']'+'[id*='+currentlevel+']').val();
				}else{
				jsonstring=	$('#data_divid'+chartid).val();
				}
			
		}else{
			jsonstring= $('#data_'+id).val();
		}
		currentDiv.empty();
		if(showHide == "true"){
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "Please Maximize", 
	    	});
		}else{
		var brwsrSupport = getInternetExplorerVersion();
		/*var jsonstring = $('#data_'+id).val();*/
		if (brwsrSupport=='fus') 
		{
			widthValue = getWidthValue(layoutchoice,userType);
			if((jsonstring=="NA1")||(jsonstring=="NA2") || (jsonstring == 'error'))
			{
				bootbox.alert({ 
					title: 'Alert',
		    	    size: 'small',
		    	    message: "chart can't be found for empty data", 
		    	});
			}
			else
				{
				if(updateType)
				{
				updateChartType(chartid,5);
				}
		var type=jsonstring.charAt(jsonstring.length-1);
		//var state = document.getElementById(divid).style.display;
		 if(type==2)
			 {
			 showhorizontalbar(divid,jsonstring,xname,yname,layoutchoice,userType);
			 }
		 else if(type==1)
		 {
			
			 /*if (state == 'none') {
		            document.getElementById(divid).style.display = 'block';
		        } */
		var yval=new Array();
		 var xval=new Array();
		 var str= jsonstring.split("~,");
		// alert("before"+str[0]);
		for(i in str)
    	  {
    	  //alert("string functions");
    	  
    	  str[i]= str[i].substring(str[i].indexOf("{")+1,str[i].lastIndexOf("}"));
    	  //alert("string is"+str[i]);
    	
    	  xval[i]=str[i].split(":")[0];
    	 
    	 yval[i]=parseFloat(str[i].split(":")[1]);
    	
    	
    	  }
	
		 
		var strxml="<graph xAxisName="+"'"+xname+"'"+"  yAxisName="+"'"+yname+"'"+" yAxisMaxValue='1'>";
		for(var j=0;j<xval.length;j++)
			{
			strxml+="<set name="+"'"+xval[j]+"'"+" "+"value="+"'"+yval[j]+"'"+" "+"color="+"'"+get_random_color()+"'"+"/>";
			}
		var xml=strxml+"</graph>";
		 //var state = document.getElementById(id).style.display;

		 if(layoutchoice==1)
		  {
	 
	  var chart= new FusionCharts("plugins/Charts/FCF_Bar2D.swf", "mychart", widthValue, "350", "0", "0");
	 
		  }
	  
	  if(layoutchoice==2)
	  {
 //  alert("second layout");
	var chart= new FusionCharts("plugins/Charts/FCF_Bar2D.swf", "mychart", widthValue,"360", "0", "0");

	  }
	  
	  if(layoutchoice==3)
	  {

	var chart= new FusionCharts("plugins/Charts/FCF_Bar2D.swf", "mychart",widthValue, "360", "0", "0");

	  }
		      chart.setDataXML(xml);
		      chart.setTransparent(true);
		      
		      chart.render(divid);
		}
				}
		}
		else
		{
			/*var state = document.getElementById(id).style.display;
			if (state == 'none') {
				document.getElementById(id).style.display = 'block';
			}*/
			$('div[id^='+id+']').css('display','block');
			if((jsonstring=="NA1")||(jsonstring=="NA2") || (jsonstring == "error"))
			{
				bootbox.alert({ 
					title: 'Alert',
		    	    size: 'small',
		    	    message: "chart can't be found for empty data", 
		    	});
			}
			else
			{
				if((jsonstring=="NA1")||(jsonstring=="NA2"))
				{
					bootbox.alert({ 
						title: 'Alert',
			    	    size: 'small',
			    	    message: "chart can't be found for empty data", 
			    	});
				}
/*			var state = $('div[id^='+id+']').style.display;*/
			var type=jsonstring.charAt(jsonstring.length-1);
			if(updateType)
			{
				updateChartType(chartid,5);
			}
			var c = $('div[id^='+id+']'); 
			while (c.firstChild) { //$('#'+id).siblings().css("display","none");
				$('div[id^='+id+']').attr("class", "");
				c.removeChild(c.firstChild);}
			var yval=new Array();
			var xval=new Array();
			var str=new Array();
			var xvals=new Array();
			var e = new Array();
			var arr = new Array();
			var test=new Array();
			var a= new Array();
			var b = new Array();
			var t = "";
			if(jsonstring.indexOf("],") == '-1')
			{
				t = jsonstring.split("],");	
			}
			else
			{
				t=jsonstring.split("],");
			}
			var  f=t[0].split(",");
			for(var i=0;i<f.length;i++)
			{
				xvals[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));
			}
			var k = 0;
			for(var i=0;i<t.length;i++)
			{
				test[i]=t[i].substring(t[i].indexOf("{"),t[i].lastIndexOf("}")+1);
				e[i] = t[i].substring(0,t[i].indexOf("["));
				if(e[i]=="")
				{
					e[i]=yname;
				}
				var x = test[i].split(",");
				for(var j=0;j<x.length;j++){
					arr[k++] = x[j];
				}
			}
			var left;
			var month = "";
			var day="";
			for(var n=0; n<arr.length;n++){
				a[n] = arr[n].substring(arr[n].indexOf("{")+1,arr[n].indexOf(":"));
				b[n] =parseFloat( arr[n].substring(arr[n].indexOf(":")+1,arr[n].indexOf("}")));
				
				month =month + a[n] + " ";
				day = day + b[n]+ " " ;
			}
			var maxlen = 0;
			for (i=0; i<xvals.length; i++) {
				if (xvals[i].length>maxlen) {
					maxlen = xvals[i].length;
				}	
			}
			var data = [];
			var k = 0;
			for ( var i = 0; i < e.length; i++) {
				data.push({
					key : e[i],
					values : []
				});	
				for ( var j = 0; j < xvals.length; j++) {
					xvals[j] = xvals[j].replace(/amp;/g, '');
					data[i].values.push({
						label : xvals[j],
						value : parseFloat(isNaN(b[k])?0:b[k])
					});
					k++;
				}			
			}
			if(type==1)
			{
				nv.addGraph(function() {
					var chart = nv.models.multiBarHorizontalChart().x(function(d) {
						return d.label;
					}).y(function(d) {
						return d.value;
					})
					.margin({top: 30, right: 30, bottom: 50, left:80})
					.showValues(true)
					.tooltips(true)
					.showControls(false)
					chart.yAxis.axisLabel(yname);
					chart.xAxis.axisLabel(xname);
					chart.showControls(false);//to remove stacked/group functionality
					d3.select('div[id^='+id+']').append("svg").style({'width' : '3000px','height' : '700px'}).datum(data).transition().duration(500).call(chart);
					nv.utils.windowResize(chart.update);
					return chart;
				}); 
			}
			else if(type==2||type==3)
			{
				/*if(layoutchoice==1)
				{*/
					nv.addGraph(function() {
						var chart = nv.models.multiBarHorizontalChart().x(function(d) {
							return d.label;
						}).y(function(d) {
							return d.value;
						})
						.margin({top: 30, right: 30, bottom: 50, left:100})
						//.width(parseInt($('#'+id).css('width'))-200)
						.showValues(false)
						.tooltips(true)
						.showControls(false);
						chart.yAxis.axisLabel(yname);
						chart.xAxis.axisLabel(xname);
						chart.showControls(false);//to remove stacked/group functionality
						d3.select('div[id^='+id+']').append("div").attr('class','table-responsive').append("svg").attr('class','multiBarHorizontal1').datum(data).transition().duration(500).call(chart);
						//====legend start
							var legend ;//= nv.models.legend1();
							var divlgnd = d3.select('div[id^='+id+']').append('div').attr('class','legenddiv')
							var contnr = divlgnd.attr('id','lgdiv_'+id).append('svg').attr('id','lgsvg_'+id); //.style('width','200px').style('height','440px');
							var vrap = contnr.selectAll('g.nv-wrap.nv-multiBarHorizontalChart').data([data]);//.append('g').attr('class','nv-legendWrap').datum(data).call(legend);
							 var gntr = vrap.enter().append('g').attr('class', 'nvd3 nv-wrap nv-multiBarHorizontalChart').append('g').attr('id','lgnd_g_'+id);
							 var gg = vrap.select('g');
							 gntr.append('g').attr('class', 'nv-legendWrap');
							 if(layoutchoice==1 ){
								 	legend = nv.models.legend1().layoutChoice(1);
									/*divlgnd.attr('class','legenddiv2');
								 	contnr.attr('class','legendsvg2');*/
								 	legend.width(900);
								 }
							 else if(layoutchoice==2 ){
								 	legend = nv.models.legend1().layoutChoice(2);
									divlgnd.attr('class','legenddiv2');
								 	contnr.attr('class','legendsvg2');
								 	legend.width(500);
								 }
							 else if(layoutchoice==3 ){
								divlgnd.attr('class','legenddiv3');
								contnr.attr('class','legendsvg3');
								legend = nv.models.legend1().width(330);
								legend.layoutChoice(3);
							 }
							 gg.select('.nv-legendWrap')
					            .datum(data)
					            .call(legend);
							 if($('.nv-series').length <= 3) {
									$('#lgsvg_'+id).closest('.legenddiv').css({overflow: 'hidden', height: '20px'});
							 }
							legend.dispatch.on('legendClick', function(d,i) {
						        try{
								d.disabled = !d.disabled;
						        }catch(e){}
		
						        if (!data.filter(function(d) { return !d.disabled }).length) {
						          data.map(function(d) {
						            d.disabled = false;
						            vrap.selectAll('.nvd3 .nv-legend .nv-series').classed('disabled', false);
						            return d;
						          });
						        }
						        d3.select('#lgdiv_'+id+' svg').datum(data).call(legend);
						        d3.select('#'+id+' svg').datum(data).transition().duration(500).call(chart);
						      });
							//$("#"+id).append($("#"+id+" div").get().reverse());
							//d3.select('#lgsvg_'+id).style('height',document.getElementById('lgnd_g_'+id).getBoundingClientRect().height+'px');
					//====legend end
							nv.utils.windowResize(chart.update);
						return chart;
					}); 
				//}
				/*if(layoutchoice==2)
				{
					nv.addGraph(function() {
						var chart = nv.models.multiBarHorizontalChart().x(function(d) {
							return d.label;
						}).y(function(d) {
							return d.value;
						})
						.margin({top: 30, right: 31, bottom: 50, left:140})
						.showValues(false)
						.tooltips(true)
						.showControls(false);
						chart.yAxis.axisLabel(xname);
						chart.xAxis.axisLabel(yname);
						d3.select('#' + id).append("svg").datum(data).transition().duration(500).call(chart);
						return chart;
					}); 
				}
				if(layoutchoice==3)
				{
					nv.addGraph(function() {
						var chart = nv.models.multiBarHorizontalChart().x(function(d) {
							return d.label;
						}).y(function(d) {
							return d.value;
						})
						.margin({top: 30, right: 25, bottom: 50, left:140})
						.showValues(false)
						.tooltips(true)
						.showControls(false);
						chart.yAxis.axisLabel(yname);
						chart.xAxis.axisLabel(xname);
						d3.select('#' + id).append("svg").datum(data).transition().duration(500).call(chart);
						return chart;
					}); 
				}*/
			}
			}
		}
	}
	}
		
	
	
	function frameChartline(id,divid,xname,yname,layoutchoice)
	{
		$("#"+id).empty();
 		//alert("in area chart");
		//alert("ID:::"+id);
		//alert("jsonstring"+jsonstring);
 		
		var jsonstring = $('#data_'+id).val();
        
		var yvalarea=new Array();
	
		var str=new Array();
		var xval=new Array();
		var earea = new Array();
		var arrarea = new Array();
		var testarea=new Array();
		
		
		
		var xvalarea=new Array();
		if ((browserName=="Netscape" && browserVer<=3) || (browserName=="Microsoft Internet Explorer" && browserVer<9)) 
		{
			//var state = document.getElementById(divid).style.display;
		var type=jsonstring.charAt(jsonstring.length-1);
		 if(type==2)
			 {
			 showFusionArea(divid,jsonstring);
			 }
		 else if(type==1)
		 {
			 /*if (state == 'none') {
		            document.getElementById(divid).style.display = 'block';
		        } */
			 $('#'+id).css('display','block');
		var yval=new Array();
		 var xval=new Array();
		 var str= jsonstring.split(",");
		// alert("before"+str[0]);
		for(i in str)
    	  {
    	  //alert("string functions");
    	  
    	  str[i]= str[i].substring(str[i].indexOf("{")+1,str[i].lastIndexOf("}"));
    	  //alert("string is"+str[i]);
    	
    	  xval[i]=str[i].split(":")[0];
    	 
    	 yval[i]=parseFloat(str[i].split(":")[1]);
    	
    	
    	  }
		 
		var strxml="<graph xAxisName="+"'"+xname+"'"+"  yAxisName="+"'"+yname+"'"+" yAxisMaxValue='1'>";
		for(var j=0;j<xval.length;j++)
			{
			strxml+="<set name="+"'"+xval[j]+"'"+" "+"value="+"'"+yval[j]+"'"+" "+"color='AFD8F8'/>";
			}
		var xml=strxml+"</graph>";
		 //var state = document.getElementById(id).style.display;
			

		 if(layoutchoice==1)
		  {
	 
	  var chart= new FusionCharts("plugins/Charts/FCF_Area2D.swf", "mychart", "800", "350", "0", "0");
	 
		  }
	  
	  if(layoutchoice==2)
	  {
  //  alert("second layout");
	var chart= new FusionCharts("plugins/Charts/FCF_Area2D.swf", "mychart", "450","360", "0", "0");

	  }
	  
	  if(layoutchoice==3)
	  {

	var chart= new FusionCharts("plugins/Charts/FCF_Area2D.swf", "mychart", "200", "150", "0", "0");

	  }
	  
		
			 
		
		      //document.getElementById(id).style.display = 'block';
		      var strXML = "<graph caption='Hours worked' showNames='1'><set name='Tom' value='32' color='AFD8F8'/><set name='Mary' value='16' color='F6BD0F'/><set name='Jane' value='42' color='8BBA00'/></graph>";
		    //  var chart= new FusionCharts("plugins/Charts/FCF_Area2D.swf", "chart1Id", "850", "350", "0", "0");
		      chart.setDataXML(xml);
		      chart.setTransparent(true);
		      
		      chart.render(divid);
		     
		     
		 
		
		}
		
		 
		
		 
		     
		 
		}
		else
			
			{
			/*var state = document.getElementById(id).style.display;
			if (state == 'none') {
	            document.getElementById(id).style.display = 'block';
	        }*/
			$('#'+id).css('display','block');
		var c = document.getElementById(id); 
		 while (c.firstChild) { //$('#'+id).siblings().css("display","none");
			$('#'+id).attr("class", "table-responsive");
			c.removeChild(c.firstChild); }
		 var t=jsonstring.split("],");
 		
 		
 var  f=t[0].split(",");

 for(var i=0;i<f.length;i++)
 {

xvalarea[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));
 
 }

 var k = 0;
 for(var i=0;i<t.length;i++)
 {

  testarea[i]=t[i].substring(t[i].indexOf("{"),t[i].lastIndexOf("}")+1);
 //alert("testing i::::::::"+test[i] );
 earea[i] = t[i].substring(0,t[i].indexOf("["));
 //alert(e[i]);

 //e[i] = test[i];
 //alert("eeeeeeeeee"+e[i]);
 var x = testarea[i].split(",");
 //alert("X LENGTH :"+x.length);
     for(var j=0;j<x.length;j++){

        arrarea[k++] = x[j];
 	}

 }

 var month = "";
 var day="";
 var b = new Array();
 for(var n=0; n<arrarea.length;n++){

   
 	b[n] =parseFloat( arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}")));
 	
 	
 	
    
 }
			}
		var data=[];
		var k=0;
		//alert(values[i])
		//alert("keys"+keys[i]+"         "+"values"+values[i]);
		for (i = 0; i < earea.length;i++ ) {
		    data.push({
		      key: earea[i],
		      values: []
		    });

		    for (j = 0; j < xvalarea.length;j++) {

		    	xvalarea[j] = xvalarea[j].replace(/amp;/g, '');
			

		      data[i].values.push({
		        x: xvalarea[j],
				y:parseFloat(b[k])
		     // , size: Math.random()
		//shape: shapes[j % 6]
		      });
			  k++;
		    }
			
		  }
		nv.addGraph(function() {
			var chart =nv.models.lineChart()();

			chart.x(function(d,i) { return i; });
			chart.xAxis.axisLabel(xname);
			chart.showControls(false);//to remove stacked/group functionality
				chart.xAxis.tickFormat(function(d, i){
				     return xvalarea[d]; //"Year1 Year2, etc depending on the tick value - 0,1,2,3,4"
				})
				chart.yAxis.axisLabel(yname);
			chart.yAxis.tickFormat(d3.format(',.1f'));
			d3.select('#'+id).append("svg").datum(data).transition()
					.duration(500).call(chart);
			//TODO: Figure out a good way to do this automatically
			nv.utils.windowResize(chart.update);
			//nv.utils.windowResize(function() { d3.select('#chart1 svg').call(chart) });
			
			return chart;
		});
		
		
	} 
	
	function frametablePopup(id,divid,xname,yname,layoutchoice,title)
	{
		//alert("toitle is:::::::::::::::"+title);
		/*var jsonstring = $('#data_'+id).val();*/
		var currentDiv=$('div[id^='+id+']');
		var jsonstring="" ;
		id=currentDiv.attr('id').split('@')[0];
		var currentlevel=currentDiv.attr('id').split('@')[1];
		var chartid= divid.split('img')[1];
		if(currentlevel!=null && currentlevel!='undefined'){
			
			if($('input[id^='+chartid+']'+'[id*='+currentlevel+']').length>0)
				{
				jsonstring=	$('input[id^='+chartid+']'+'[id*='+currentlevel+']').val();
				}else{
				jsonstring=	$('#data_divid'+chartid).val();
				}
			
		}else{
			jsonstring= $('#data_'+id).val();
		}
		currentDiv.empty();
 		/*$("#"+id).empty();*/

		if(jsonstring == 'NO'){
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "Can't view tabular data for this chart", 
	    	});
		}else if(jsonstring == 'NA1' || jsonstring == 'NA2' || jsonstring == 'error'){
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "Tabular data not available for empty chart", 
	    	});
		}else{
			document.getElementById("tablejsondataid").value= jsonstring;
			$.post('frameTable.action?xname='+encodeURIComponent(xname)+'&yname='+encodeURIComponent(yname)+'&title='+encodeURIComponent(title), function(result) {
				document.getElementById('popupDivId4').innerHTML = result;
				 //jQuery("body").unmask();
				 $.blockUI({
						message : $('#popupDivId4'),
						css : {
							top : ($(window).height() - 900) / 3 + 'px',
							left : ($(window).width() - 400) / 3 + 'px',
							border : 'none',
							width : '0px',
							height : '0px'
						}
					});
			});
		}
	}
	
	function frametablecriteria(jsonstring,tableid,xname,yname)
	{
		
		
		var yvalarea=new Array();
		
		var str=new Array();
		var xval=new Array();
		var earea = new Array();
		var arrarea = new Array();
		var testarea=new Array();
		var xvalarea=new Array();
		
		
		
		 var t=jsonstring.split("],");
		var  f=t[0].split("~,");

		 for(var i=0;i<f.length;i++)
		 {

		xvalarea[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));
		 
		 }

		 var k = 0;
		 for(var i=0;i<t.length;i++)
		 {

		  testarea[i]=t[i].substring(t[i].indexOf("{"),t[i].lastIndexOf("}")+1);
		 //alert("testing i::::::::"+test[i] );
		 earea[i] = t[i].substring(0,t[i].indexOf("["));
		 //alert(e[i]);

		 //e[i] = test[i];
		 //alert("eeeeeeeeee"+e[i]);
		 var x = testarea[i].split(",");
		 //alert("X LENGTH :"+x.length);
		     for(var j=0;j<x.length;j++){

		        arrarea[k++] = x[j];
		 	}

		 }
		 var month = "";
		 var day="";
		 var b = new Array();
		 for(var n=0; n<arrarea.length;n++){
		 	b[n] =parseFloat( arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}"))).toFixed(2);
		 }
		// alert("keys length is"+xvalarea.length+"one elem"+xvalarea[0]);
		 var root=document.getElementById(tableid);
			var tab=document.createElement('table');
			tab.className="gridtable";
			var tbo=document.createElement('tbody');
			var row1, cell,cell2,row2,cell4,cell3;
			//===========================
			xndyrow=document.createElement('tr');

			xndyth=document.createElement('th');xndyth.colSpan=xvalarea.length;xndyth.style.padding='0px';//xndyth.style.borderWidth='0px';xndyth.style.borderBottomWidth='1px';
			xndyth.appendChild(document.createTextNode(xname));
			xndyrow.appendChild(xndyth);
			
			row1=document.createElement('tr');
			cell=document.createElement('th');cell.rowSpan=2;cell.style.padding='0px';
			cell.appendChild(document.createTextNode('Criteria'));
			
			xndyrow.appendChild(cell);xndyrow.appendChild(xndyth);
			tbo.appendChild(xndyrow);
			//==========================
			for(var j=0;j<xvalarea.length;j++){
				
				cell=document.createElement('th');
				cell.appendChild(document.createTextNode(xvalarea[j]));
				
				row1.appendChild(cell);
				tbo.appendChild(row1);
			}
			var temp=0;
			for(var k=0;k<testarea.length;k++)
			{
				//alert("elem length"+xvalarea.length+"valuea are"+b.length);
				row2=document.createElement('tr');
				cell3=document.createElement('td');
				cell3.appendChild(document.createTextNode(earea[k]));
				row2.appendChild(cell3);
				for(i=0;i<xvalarea.length;i++)
					{
					cell2=document.createElement('td');
					cell2.appendChild(document.createTextNode(b[temp]));
					row2.appendChild(cell2);
					tbo.appendChild(row2);
					
					temp++;
					
					
					}
				
				
			
				//cell=document.createElement('td');
				//cell.appendChild(document.createTextNode("criteria"));
			}
			
			
			
			
			
			tab.appendChild(tbo);
			root.appendChild(tab);
			
		
	}
	
	function updateChartType(chartid,chartType,minmax)
	{
		//alert("chart id"+chartid+"   "+chartType);
		//alert("InsideUpdateChartType"+"     "+chartid+"         "+chartType);
		var expType = "imgexp"+chartid;
		//alert("expType :"+expType);
		
		try{document.getElementById(expType).value=chartType;}
		catch(e){}
		if(minmax != undefined)
		$.post("updateChartType.action?chartId="+chartid+"&chartType="+chartType+"&minmax="+minmax,function(result){
			if(result.indexOf('updated')!=-1)
				$('#minmax_divid'+chartid).val(minmax);
			//else alert('Chart Updating failed');
		});
		else
			$.post("updateChartType.action?chartId="+chartid+"&chartType="+chartType,function(result){});
		
	

	}
	
	
	function dataCompIE(id,xaxis,yaxis,widthValue)
	{
		
		var jsonstring = $('#data_'+id).val();
			
		
				if((jsonstring=="NA1")||(jsonstring=="NA2"))
				{
					bootbox.alert({ 
						title: 'Alert',
			    	    size: 'small',
			    	    message: "chart can't be found for empty data", 
			    	});
				}
				else
					
					/*var state = document.getElementById(id).style.display;
			        if (state == 'none') {
			            document.getElementById(id).style.display = 'block';
			        }*/
					$('#'+id).css('display','block');
			var type=jsonstring.charAt(jsonstring.length-1);
			//alert("type is:::::"+type);
			 if(type==2)
			 {
				 	(id,jsonstring,'','',1,widthValue);
			 }
			 else if(type==1)
			 {
				
			var yval=new Array();
			 var xval=new Array();
			 var str= jsonstring.split(",");
			// alert("before"+str[0]);]
			for(i in str)
	    	  {
	    	  //alert("string functions");
	    	  
	    	  str[i]= str[i].substring(str[i].indexOf("{")+1,str[i].lastIndexOf("}"));
	    	  //alert("string is"+str[i]);
	    	
	    	  xval[i]=str[i].split(":")[0];
	    	 
	    	 yval[i]=parseFloat(str[i].split(":")[1]);
	    	
	    	
	    	  }
			 
			var strxml="<graph xAxisName="+"'"+xname+"'"+"  yAxisName="+"'"+yname+"'"+" yAxisMaxValue='1'>";
			for(var j=0;j<xval.length;j++)
				{
				strxml+="<set name="+"'"+xval[j]+"'"+" "+"value="+"'"+yval[j]+"'"+" "+"color='AFD8F8'/>";
				}
			var xml=strxml+"</graph>";
			 //var state = document.getElementById(id).style.display;

		
		  var chart= new FusionCharts("plugins/Charts/FCF_Column2D.swf", "mychart", widthValue, "350", "0", "0");
				 
			      chart.setDataXML(xml);
			      chart.setTransparent(true);
			      
			      chart.render(divid);
			     
			}
			
			 
			}
	
	
	function getInternetExplorerVersion()
	{
	   var brwsr = "fus";
	   var rv = -1; // Return value assumes failure.
	   if (navigator.appName == 'Microsoft Internet Explorer')
	   {
	      var ua = navigator.userAgent;
	      var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
	      if (re.exec(ua) != null)
	         rv = parseFloat( RegExp.$1 );
	   }
	   else
		   {
		   rv = -1;
		   }
	   
	   if ( rv> -1 )
	   {
		   if(rv>=9)
			   brwsr = "nvd";
	   else if ( rv== 8.0 )
	    	  brwsr = "fus";
	      else if ( rv == 7.0 )
	    	  brwsr = "fus";
	      else if ( rv == 6.0 )
	    	  brwsr = "fus";
	      else
	    	  brwsr = "fus";
	    }
	   else 
		   {
		   brwsr = "nvd";
		   }
	   
	   return brwsr;
	}
	   
	function resetMyAccFrm()
	{
		document.myaccntfrm.submit();
	}
	function frametable(tableid,xname,yname) {
		
		var jsonstring = document.getElementById("tablejsondataid").value;
		var type=jsonstring.charAt(jsonstring.length-1);
		//alert("type is::::::::"+type);
		 if(type==2||type==3){
			 frametablecriteria(jsonstring,tableid,xname,yname);
		 }else if(type==1){
			 var yval=new Array();
			 var xval=new Array();
			 var str= jsonstring.split("~,");
				for(i in str){
		    	  str[i]= str[i].substring(str[i].indexOf("{")+1,str[i].lastIndexOf("}"));
		    	  xval[i]=str[i].split(":")[0];
		    	 yval[i]=parseFloat(str[i].split(":")[1]).toFixed(2);
		 }
		var root=document.getElementById(tableid);
		var tab=document.createElement('table');
		tab.className="gridtable";
		var tbo=document.createElement('tbody');
		var row, cell,cell2,row1,cell4,cell3;
		row1=document.createElement('tr');
		for(var j=0;j<2;j++){
			cell3=document.createElement('th');
			cell3.appendChild(document.createTextNode(xname));
			
			cell4=document.createElement('th');
			cell4.appendChild(document.createTextNode(yname));
		}
		
		row1.appendChild(cell3);
		row1.appendChild(cell4);
		tbo.appendChild(row1);
		
		for(var i=0;i<xval.length;i++){
			if(!isNaN(yval[i])){
			row=document.createElement('tr');
			for(var j=0;j<2;j++){
				cell=document.createElement('td');
				cell.appendChild(document.createTextNode(xval[i]));
				
				cell2=document.createElement('td');
				cell2.appendChild(document.createTextNode(yval[i]));
			}
			row.appendChild(cell);
			row.appendChild(cell2);
			
			tbo.appendChild(row);
		}}
		
		tab.appendChild(tbo);
		root.appendChild(tab);
		}
	}
	function randomData(jsonstring,yname) {
		//alert("in number checking");
		//Laptop[{20:50.0}~,{25:26.0}], Mobile[{20:67.0}~,{21:32.0}~,{22:93.0}~,{23:87.0}], PC[{21:12.0}~,{23:41.0},{23:43.0}~,{25:14.0},{25:21.0}]]2


		//alert("jsonstring"+jsonstring);//# groups,# points per group
		var yvalarea=new Array();
		var str=new Array();
		var xval=new Array();
		var earea = new Array();
		var arrarea = new Array();
		var testarea=new Array();
		var xvalarea=new Array();
		 var t=jsonstring.split("],");
		 
		 //Laptop[{20:50.0}~,{25:26.0}], Mobile[{20:67.0}~,{21:32.0}~,{22:93.0}~,{23:87.0}], PC[{21:12.0}~,{23:41.0},{23:43.0}~,{25:14.0},{25:21.0}]]2
          var data=[];

		 for(var temp=0;temp<t.length;temp++)
		 {
		 earea[temp] = t[temp].substring(0,t[temp].indexOf("["));
		 if(earea[temp]=="")
			 {
			 earea[temp]=yname;
			 }
		 
		 data.push({
		      key: earea[temp],
		      values: []
		    });
		 var totalParam=t[temp].substring(t[temp].indexOf("{"),t[temp].lastIndexOf("}")+1);
		 var innerparam =totalParam.split("~,");
		 
		 
		 for(var count=0;count<innerparam.length;count++ )
			 {
			 
			 var xval = innerparam[count].substring(innerparam[count].indexOf("{")+1,innerparam[count].indexOf(":"));
			 var yval = innerparam[count].substring(innerparam[count].indexOf(":")+1,innerparam[count].indexOf("}"));
			 
			 data[temp].values.push({
		    	 
				 	x: parseFloat(xval),
					y:parseFloat(yval),
					 size: Math.random()
			 });
			 }
		 
		 }
		 
		/* var  f=t[0].split("~,");
		
		 
	 			
		 for(var i=0;i<f.length;i++)
		 {

		xvalarea[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));
		
		 
		 }
		
		 var k = 0;
		 for(var i=0;i<t.length;i++)
		 {

		  testarea[i]=t[i].substring(t[i].indexOf("{"),t[i].lastIndexOf("}")+1);
		 //alert("testing i::::::::"+test[i] );
		 earea[i] = t[i].substring(0,t[i].indexOf("["));
		 //alert(e[i]);

		 //e[i] = test[i];
		 //alert("eeeeeeeeee"+e[i]);
		 var x = testarea[i].split(",");
		 //alert("X LENGTH :"+x.length);
		     for(var j=0;j<x.length;j++){

		        arrarea[k++] = x[j];
		 	}

		 }

		 var month = "";
		 var day="";
		 var b = new Array();
		 var yvalorig=new Array();
		 for(var n=0; n<arrarea.length;n++){
			
            
		 	b[n] =parseFloat( arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}")));
		 	yvalorig[n]=arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}"));
				 	 alert("arrarea is"+arrarea[n]);
				 	xvalarea[n]=arrarea[n].substring(arrarea[n].indexOf("{")+1,arrarea[n].indexOf(":"));
				 	
		    
		 }
			
		// alert(xvalarea.length);
		 //alert(b);
		 var data=[];
					
		 var k=0;
			//alert(values[i])
			//alert("keys"+keys[i]+"         "+"values"+values[i]);
			for (i = 0; i < earea.length;i++ ) {
				alert(earea[i]);
			    data.push({
			      key: earea[i],
			      values: []
			    });

			    for (j = 0; j < xvalarea.length;j++) {
	               
	                 
				

			      data[i].values.push({
			    	 
			        x: parseFloat(xvalarea[k]),
			        
					y:parseFloat(b[k]),
					 size: Math.random()
					
			     // , size: Math.random()
			//shape: shapes[j % 6]
			      });
				  k++;
			    }
				
			  }*/
				
		  return data;
		
				}
	

	function hideMenu() {
	var divVal = document.getElementById("menuImg").value;
	var charName = divVal.charAt(0);
	if (charName == '~') {
		var chartIdVal = divVal.replace("~", "");
		document.getElementById("menuImg").value = chartIdVal;
	} else {
		if (divVal.length > 0) {
			var id1 = 'image1' + divVal;
			var id2 = 'image2' + divVal;
			var id3 = 'divDropDown1' + divVal;
			document.getElementById(id2).style.display = 'NONE';
			document.getElementById(id1).style.display = 'BLOCK';
			document.getElementById(id3).style.display = 'NONE';
		}
	}
}
	
	function validateSendMailReport()
	{
		var param = $("#sendReportMail").validate({
			rules : {
				toAddress: {
	                required: true,
	                multiemails: true
	            },
	            chartNames : {
					required : true
				},
				reportBody:{
					required : true
				},
				headerName :{
					required : true
				},
				footerName :{
					required : true
				},
				numOfRecords :{
					min : 0,
					max : 100
				}
			},
			messages : {
				toAddress : {
					required : "Email is required",
					multiemails : "You must enter a valid email, or semicolon separate multiple"
				},
				chartNames : {
					required : "Report name is required"
				},
				reportBody:{
					required : "Description is required"
				},
				headerName :{
					required : "Header is required"
				},
				footerName :{
					required : "Footer is required"
				},
				numOfRecords :{
					min : "Minimum 0 #Records in the chart are allowed",
					max : "Maximum 100 #Records in the chart are allowed"
				}
			},
			errorElement : 'div',
			errorLabelContainer: '#error'

		}).form();
		return param;
	}
	function validateSendMailDashboard()
	{
		var param = $("#sendDashboardMail").validate({
			rules : {
				toAddress: {
	                required: true,
	                multiemails: true
	            },
	            dashboardName : {
					required : true
				},
				reportBody:{
					required : true
				},
				headerName :{
					required : true
				},
				footerName :{
					required : true
				}
			},
			messages : {
				toAddress : {
					required : "Email is  required",
					multiemails : "You must enter a valid email, or semicolon separate multiple"
				},
				dashboardName : {
					required : "Dashboard name is required"
				},
				reportBody:{
					required : "Description is required"
				},
				headerName :{
					required : "Header is required"
				},
				footerName :{
					required : "Footer is required"
				}
			},
			errorElement : 'div',
			errorLabelContainer: '#error'
		}).form();
		return param;
	}

	 String.prototype.count = function(s1) {
		 return (this.length - this.replace(new RegExp(s1, "g"), '').length)/ s1.length;
	 }

	 function checkTimeValue(data){
		var cnt = data.count('-');
		var year = data.substr(data.lastIndexOf('-')+1).trim();
		if(cnt > 2){
			return false;
		}else if(isNaN(year)){
			var year2 = data.substr(data.length-4);
				if(isNaN(year2)){
					return false;
				}else{
					return true;
				}
			}else{
				return true;
			}
	}
	 
	 function refreshMe(timeoutPeriod) {
		 //alert("timeoutPeriod : "+timeoutPeriod);
		 if(timeoutPeriod > 0)
			{ setInterval(autoRefresh,timeoutPeriod);
			 dsblayoutId4 = $('#dsblayoutId4').val();
			 showUserDashboardLayout(dsblayoutId4);
			}
		 else{
			// alert("No time specifiled");
		 }
	}
	
	 function showbarChart( jsonstring, xName, yName,bigDiv,bigChart) {
			
			
			var id = 'dispchart';
			var type = jsonstring.charAt(jsonstring.length - 1);
			if(type==1)
				{
			/*var c = document.getElementById(id);
			while (c.firstChild) {
				c.removeChild(c.firstChild);
			}*/
			var yval = new Array();
			var xval = new Array();
			var str = new Array();
			var keys = new Array();
			var yvalorig=new Array();
			keys[0] = "";
			str = jsonstring.split("~,");

			for (i in str) {
				xval[i] = str[i].substring(str[i].indexOf("{") + 1, str[i].indexOf(":"));
				yvalorig[i]=str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}"));
				//yval[i] = parseFloat(str[i].split(":")[1]);
				yval[i] = parseFloat(str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}")));
			}
			
			var maxlen = 0;
			for (var i=0; i<yvalorig.length; i++) {
			  if (yvalorig[i].length>maxlen) 
			    maxlen = yvalorig[i].length;
			}
			if(maxlen>=13)
				var left=80;
			else
				left=80;
			var barchartdata = [];
			var k = 0;
			for ( var i = 0; i < keys.length; i++) {
				barchartdata.push({
					key : keys[i],
					values : []
				});
				for ( var j = 0; j < xval.length; j++) {
					xval[j] = xval[j].replace(/amp;/g, '');
					barchartdata[i].values.push({
						label : xval[j],
						value : yval[k]
					});
					k++;
				}
			}
			var chart;
			nv.addGraph(function() {
				chart= nv.models.discreteBarChart().x(function(d) {
					return d.label;
				})
				.y(function(d) {
					return d.value;
				}).staggerLabels(false)
				 .margin({top: 30, right: 40, bottom: 60, left:left})
				//.staggerLabels(barchartdata[0].values.length > 8)
				 //.showValues(true)
				.tooltips(true);
				chart.yAxis.axisLabel(yName)
				chart.yAxis.tickFormat(d3.format('d'));
				chart.xAxis.axisLabel(xName)
				
				if(xval.length>=10)
				{
					chart.margin({bottom:90});
					chart.xAxis.rotateLabels(-25);
				}
				//d3.select('#' + id).append('svg').style('height','720px').datum(barchartdata).transition().duration(500)
				d3.select('#' + id).append('svg').style({'width' : '2000px','height' : '720px'}).datum(barchartdata).transition().duration(500)
						.call(chart);
				nv.utils.windowResize(chart.update);
				return chart;
			});
		//}else
		}
		if(type==2 || type==3)
			{
			/*var c = document.getElementById(id); 
			 while (c.firstChild) { c.removeChild(c.firstChild); }*/
			var yval=new Array();
			var xval=new Array();
			var str=new Array();
			var xvals=new Array();
			var e = new Array();
			var arr = new Array();
			var test=new Array();
			var a= new Array();
			var b = new Array();
			var t = "";
			if(jsonstring.indexOf("],") == '-1')
				t = jsonstring.split("],");	
			else
				t=jsonstring.split("],");
							
				var f = t[0].split(",");
				for ( var i = 0; i < f.length; i++) {
					xvals[i] = f[i].substring(f[i].indexOf("{") + 1, f[i].indexOf(":"));
				}

				var k = 0;
				for ( var i = 0; i < t.length; i++) {
					test[i] = t[i].substring(t[i].indexOf("{"), t[i]
							.lastIndexOf("}") + 1);
					e[i] = t[i].substring(0, t[i].indexOf("["));
					var x = test[i].split(",");
					for ( var j = 0; j < x.length; j++) {
						arr[k++] = x[j];
					}
				}

				var month = "";
				var day = "";
				for ( var n = 0; n < arr.length; n++) {
					a[n] = arr[n].substring(arr[n].indexOf("{") + 1, arr[n]
							.indexOf(":"));
					b[n] = parseFloat(arr[n].substring(arr[n].indexOf(":") + 1,
							arr[n].indexOf("}")));

					month = month + a[n] + " ";
					day = day + b[n] + " ";
				}
				var data = [];
				var k = 0;
				for ( var i = 0; i < e.length; i++) {
					data.push({
						key : e[i],
						values : []
					});
					for ( var j = 0; j < xvals.length; j++) {
						xvals[j] = xvals[j].replace(/amp;/g, '');
						data[i].values.push({
							label : xvals[j],
							value : parseFloat(b[k])
						});
						k++;
					}
				}
				nv.addGraph(function() {
					var chart = nv.models.multiBarChart1().x(function(d) {
						return d.label;
					}).y(function(d) {
						return d.value;
					});
					// .staggerLabels(true);
					// .tooltips(false)
					// .showValues(true);
					chart.yAxis.axisLabel(yName);
					chart.yAxis.tickFormat(d3.format('.2f'));
					chart.xAxis.axisLabel(xName);
					chart.showControls(false);//to remove stacked/group functionality
					if(xvals.length>10)
						{
						chart.margin({bottom: 100});

						chart.xAxis.rotateLabels(-15);
							}
					//d3.select("#"+id).append('div').style('height','720px');
					d3.select("#"+id).append('div').style('height','420px');
						d3.select('#' + id+' div').append('svg').style('height','380px').datum(data).transition().duration(500)
								.call(chart);

						nv.utils.windowResize(chart.update);

						//====legend start
						var legend ;
						var divlgnd = d3.select('#' + id).append('div').attr('class','legenddiv');
						var contnr = divlgnd.attr('id','lgdiv_'+id).append('svg')
						;//.style('width','200px').style('height','440px');
						contnr.attr('id','lgsvg_'+id);
						var vrap = contnr.selectAll('g.nv-wrap.nv-multiBarWithLegend').data([data]);//.append('g').attr('class','nv-legendWrap').datum(data).call(legend);
						 var gntr = vrap.enter().append('g').attr('class', 'nvd3 nv-wrap nv-multiBarWithLegend').append('g').attr('id','lgnd_g_'+id).attr('transform', function(d,i) {
			                  return 'translate(0,-' + 2 + ')';
			                });
						 var gg = vrap.select('g');
						 gntr.append('g').attr('class', 'nv-legendWrap');
						 
							 	legend = nv.models.legend1().layoutChoice(1);
							 	legend.width(900);
						 gg.select('.nv-legendWrap')
				            .datum(data)
				            .call(legend);
						 if($('.nv-series').length <= 3) {
								$('#lgsvg_'+id).closest('.legenddiv').css({overflow: 'hidden', height: '20px'});
						 }
						legend.dispatch.on('legendClick', function(d,i) {
					        try{
							d.disabled = !d.disabled;
					        }catch(e){}

					        if (!data.filter(function(d) { return !d.disabled }).length) {
					          data.map(function(d) {
					            d.disabled = false;
					            vrap.selectAll('.nvd3 .nv-legend .nv-series').classed('disabled', false);
					            return d;
					          });
					        }
					        d3.select('#lgdiv_'+id+' svg').datum(data).call(legend);
					        d3.select('#'+id+' svg').datum(data).transition().duration(500).call(chart);
					      });
						d3.select('#lgsvg_'+id).style('height',document.getElementById('lgnd_g_'+id).getBoundingClientRect().height+'px');
					//====legend end
						$('.nv-controlsWrap').attr('transform','translate(240,-30)')
					nv.utils.windowResize(chart.update);
						return chart;
					});
	               	 
					//}else 
				}
			}
	 
	function showbarChartIE(jsonstring,xname,xname){
	
		document.getElementById("dispchart").style.display='block';
		/*var jsonstring="<s:property value='chartStr'/>";
		var xname="<s:property value='xname'/>";
		var yname="<s:property value='yname'/>";*/
		
		 var type = jsonstring.charAt(jsonstring.length - 1);
			
			 if (type == 1) {
				

				var yval = new Array();
				var xval = new Array();
				var str = jsonstring.split("~,");
				// alert("before"+str[0]);]
				for (i in str) {
					// alert("string functions");

					str[i] = str[i].substring(str[i].indexOf("{") + 1, str[i]
							.lastIndexOf("}"));
					// alert("string is"+str[i]);

					xval[i] = str[i].split(":")[0];

					yval[i] = parseFloat(str[i].split(":")[1]);

				}

				var strxml = "<graph  xAxisName="+"'"+xname+"'"+"  yAxisName="+"'"+yname+"'"+" yAxisMaxValue='1'>";
				
				
				for ( var j = 0; j < xval.length; j++) {
					strxml += "<set name=" + "'" + xval[j] + "'" + " "
							+ "value=" + "'" + yval[j] + "'" + " " + "color="
							+ "'" + get_random_color() + "'" + "/>";
				}
				var strxml="<graph xAxisName="+"'"+xname+"'"+"  yAxisName="+"'"+yname+"'"+" yAxisMaxValue='1'>";
				for(var j=0;j<xval.length;j++)
					{
					strxml+="<set name="+"'"+xval[j]+"'"+" "+"value="+"'"+yval[j]+"'"+" "+"color="+"'"+get_random_color()+"'"+"/>";
					
					}
				var xml=strxml+"</graph>";
				
			
			  var chart= new FusionCharts("plugins/Charts/FCF_Column2D.swf", "mychart", "600", "300", "0", "0");
					 
				      chart.setDataXML(xml);
				      chart.setTransparent(true);
				      
				      chart.render("dispchart");

	}
			 else
				 {
				 var yvalarea=new Array();
					
					var str=new Array();
					var xval=new Array();
					var earea = new Array();
					var arrarea = new Array();
					var testarea=new Array();
					
					var strxml="<graph  xAxisName="+"'"+xname+"'"+"  yAxisName="+"'"+yname+"'"+" yAxisMaxValue='1'>";
					
					strxml+="<categories>";
					
					var xvalarea=new Array();
					
					var t=jsonstring.split("],");
			 		
			 		
					 var  f=t[0].split("~,");

					 for(var i=0;i<f.length;i++)
					 {

					xvalarea[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));
					//alert("categories"+xvalarea[i]);
					strxml+="<category name="+"'"+xvalarea[i]+"'"+"/>";
					 }
			        strxml+="</categories>";
					 var k = 0;
					 for(var i=0;i<t.length;i++)
					 {

					  testarea[i]=t[i].substring(t[i].indexOf("{"),t[i].lastIndexOf("}")+1);
					 //alert("testing i::::::::"+test[i] );
					 earea[i] = t[i].substring(0,t[i].indexOf("["));
					 //alert(e[i]);

					 //e[i] = test[i];
					 //alert("eeeeeeeeee"+e[i]);
					 var x = testarea[i].split(",");
					 //alert("X LENGTH :"+x.length);
					     for(var j=0;j<x.length;j++){

					        arrarea[k++] = x[j];
					 	}

					 }

					 var month = "";
					 var day="";
					 var b = new Array();
					 for(var n=0; n<arrarea.length;n++){

					   
					 	b[n] =parseFloat( arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}")));
					 	
					 	
					 	
					    
					 }
					 var k=0;        
					 
					 for (var i = 0; i < earea.length;i++ ) {
						  
			                 strxml+="<dataset seriesName="+"'"+earea[i]+"'"+" color="+"'"+get_random_color()+"'"+">";
						    for (var j = 0; j < xvalarea.length;j++) {

				                   
							
			strxml+="<set value="+"'"+b[k]+"'"+"/>";
			k++;
						     
						    }
						    strxml+="</dataset>"; 
							
						  }
					 strxml+="</graph>";
					 var chart= new FusionCharts("plugins/Charts/FCF_MSColumn2D.swf", "mychart", "620", "300", "0", "0");
					 
					 chart.setDataXML(strxml);
				      chart.setTransparent(true);
				      
				      chart.render("dispchart");
				 }
	}
	
//chartHelper.js
	function frameHistogram(id,xname,yname,layoutchoice,chartid,bins){
		var currentDiv=$('div[id^='+id+']');
		var jsonstring="" ;
		id=currentDiv.attr('id').split('@')[0];
		var currentlevel=currentDiv.attr('id').split('@')[1];
		
		if(currentlevel!=null && currentlevel!='undefined'){
			
			if($('input[id^='+chartid+']'+'[id*='+currentlevel+']').length>0)
				{
				jsonstring=	$('input[id^='+chartid+']'+'[id*='+currentlevel+']').val();
				}else{
				jsonstring=	$('#data_divid'+chartid).val();
				}
			
		}else{
			jsonstring= $('#data_'+id).val();
		}
		currentDiv.empty();
		
		if(showHide == "true"){bootbox.alert({ 
			title: 'Alert',
    	    size: 'small',
    	    message: "Please Maximize", 
    	});}else{
		var brwsrSupport = getInternetExplorerVersion();
/*		var jsonstring = $('#data_'+id).val();*/
		if (brwsrSupport == 'fus') {
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "This feature is not supported in internet explorer 8.0", 
	    	});
			
		} else {

			if ((jsonstring == "NA1") || (jsonstring == "NA2")) {
				bootbox.alert({ 
					title: 'Alert',
		    	    size: 'small',
		    	    message: "chart cannot be found for empty data", 
		    	});
			} else {
				/*var state = document.getElementById(id).style.display;
				if (state == 'none') {
					document.getElementById(id).style.display = 'block';
				}*/
				$('div[id^='+id+']').css('display','block');
				var type = jsonstring.charAt(jsonstring.length - 1);
				// alert("type is"+type);
				if (type == 1) {
					// alert("json"+jsonstring);
					showHistogram(id, jsonstring, xname, yname,bins);
					updateChartType(chartid, 7);
				}
				
				if(type==2||type==3)
					{
					bootbox.alert({ 
						title: 'Aler t',
			    	    size: 'small',
			    	    message: "Histogram not supported for this chart", 
			    	});
					}
			}
	}
	}
	}
	
	
	function showHistogram(id, jsonstring, xName, yName,bins){

	var c = $('div[id^='+id+']');
	while (c.firstChild) {
		//$('#'+id).siblings().css("display","none");
		c.removeChild(c.firstChild);
	}
	var yval = new Array();
	var xval = new Array();
	var str = new Array();
	var keys = new Array();
	var yvalorig=new Array();
	keys[0] = "";
	str = jsonstring.split("~,");
	
	var histogramData = "";
	for (i in str) {
		xval[i] = str[i].substring(str[i].indexOf("{") + 1, str[i].indexOf(":"));
		yvalorig[i]=str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}"));
		yval[i] = parseFloat(str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}")));
		histogramData = histogramData+yval[i]+",";
	}

	var maxlen = 0;
	for (i=0; i<yvalorig.length; i++) {
		
	  if (yvalorig[i].length>maxlen) {
	    maxlen = yvalorig[i].length;
	  }
	}
	if(maxlen>=13)
		{
		var left=130;
		}
	else
		left=90;

	var barchartdata = [];
	var k = 0;
	//alert("Histogram ...");
	//alert("x-axis Values :"+ xval);
	//alert("yval :"+ yval);
	
	histogramData = histogramData.substr(0,histogramData.length-1);
	 var buckets = 8;
	if(bins > 0){
		buckets = bins;
	}
	  var data = histogramData.replace(/\s+/g, '').split(",");
	  data.sort(function(a,b){return a-b});
	
	  //what if they are decimals?
	  //var last = data.slice(-1)[0];
	  var last = data.slice(-1)[0];
	  var next = new Number(data[0]);
	  //toFixed() returns a string so using MAth function
	  //var step = parseFloat(last/buckets);
	  var rstep = Math.round(((last-next)/buckets)*100)/100;
	  var ranges = [];
	  var values = [];
	  //var next = new Number(data[0]);

	  for (i=0; i<buckets; i++){ 
	    if(i<buckets-1){
	      var first = next;
	      var next = Math.round((next + rstep)*100)/100;
	      var bin = first+"-"+next;
	    } else {
	      var mylast = Math.round(last*100)/100;
	      var bin = next+"-"+mylast;
	      var next = mylast;
	    }

	    var count = 0;

	    while (data[0]<=next) {
	      count++;
	      data.shift();
	    };    

	    values.push(count);
		ranges.push(bin);
		}
	 	  
	for ( var i = 0; i < keys.length; i++) {
		barchartdata.push({
			key : keys[i],
			values : []
		});
		xval = ranges;
		yval = values;
		for ( var j = 0; j < xval.length; j++) {
	
			barchartdata[i].values.push({
				label : xval[j],
				value : parseInt(yval[k])
			});
			k++;
		}
	}


	nv.addGraph(function() {
		var chart = nv.models.discreteBarChart().x(function(d) {
			return d.label;
		})

		.y(function(d) {
			return d.value;
		}).staggerLabels(true)
		 .margin({top: 25, right: 10, bottom: 70, left:left})
		 
		// .staggerLabels(barchartdata[0].values.length > 8)
		 .showValues(true)
		.tooltips(true);
		chart.yAxis.axisLabel("Frequency")
		chart.yAxis.tickFormat(d3.format('d'));

		chart.xAxis.axisLabel(yName)
		
		d3.select('div[id^='+id+']').attr("class","table-responsive");
		d3.select('div[id^='+id+']').append("svg").style("height","420px").datum(barchartdata).transition().duration(500)
				.call(chart);
		nv.utils.windowResize(chart.update);

		return chart;
	});
	}
	
	//Gauge chart script 
	var gauges = [];
	var kk=0;
	function createGauge(name, label, min, max,ele)
	{
		var config = 
		{
			size: 220,
			label: name,
			min: undefined != min ? min : 0,
			max: undefined != max ? max : 500,
			minorTicks: 20
		}
		
		var range = config.max - config.min;
		// we can enhace gauge by giving user a chance to have his own green, yellow, red bands in gauges
		config.greenZones = [{ from: config.min + range*0.5, to: config.min + range*0.75 }];
		config.yellowZones = [{ from: config.min + range*0.75, to: config.min + range*0.9 }];
		config.redZones = [{ from: config.min + range*0.9, to: config.max }];
		d3.select("#g"+ele).append("div").attr("id","gaugedivbox"+kk).attr("class","gaugedivbox");
		gauges[name] = new Gauge("gaugedivbox"+kk , config);
		kk++;
		//document.getElementById(name).innerHTML = "";
		//document.getElementById(name).style = "text-align:center;";
		gauges[name].render();
	}
	
	/*function createGauges(divid,chartName)
	{
		//createGauge("memory", "Memory");	//createGauge("test", "Test", -50, 50 );
		//createGauge(divid,chartName);
		
		createGauge(divid,chartName);
	}*/
	/*function updateGauges_asli()
	{for (var key in gauges)
		{var value = getRandomValue(gauges[key])
		gauges[key].redraw(value);
	}}*/
	var nAgt = navigator.userAgent;
	var values = [];
	var createGauges = function createGauges(ele)
	{
		var slct;
	//ele=this.id;
	try{
		if(ele == undefined || (this.id != undefined && (this.id).contains('s_'))){// if 'this' is window
			ele = this.id;
			slct = document.getElementById(''+ele);
			var opt = d3.select(slct).selectAll('option');
			opt.attr('selected','selected');
		}
		if((nAgt.indexOf("Chrome"))!=-1 || (nAgt.indexOf("Safari"))!=-1){
			//alert("chrome");
			if(ele.indexOf("btn_") != -1){// from select <a>
				ele = ele.replace('btn','s');
				slct = document.getElementById(''+ele);
			}
			else if(ele.indexOf('FT_')!=-1){//on gauge chart selection
				ele = ele.replace('FT','s');
				slct = document.getElementById(''+ele);
				var opt = d3.select(slct).selectAll('option');
				opt.attr('selected','selected');
			}
		}//Chrome
		else{
		if(ele.contains('FT_')){//on gauge chart selection
			ele = ele.replace('FT','s');
			slct = document.getElementById(''+ele);
			var opt = d3.select(slct).selectAll('option');
			opt.attr('selected','selected');
		}
		else if(ele.contains('btn_')){// from select <a>
			ele = ele.replace('btn','s');
			slct = document.getElementById(''+ele);
		}//ele.indexOf("btn_") != -1//chrome
	}//firefox
		//else if()
		//slct = document.getElementById(''+ele);
	}catch(e){}
	if(ele != undefined){
	values=[];gauges = [];
	try{
	$("#g"+ele).html("");//="";
	}catch(e){
		alert(e);
	}
	var svgid = ele.split('_')[1];
	var min = parseFloat($("#min_"+svgid).val());
	var max = parseFloat($("#max_"+svgid).val());
	min=isNaN(min)?0:min;
	max=isNaN(max)?0:max;
	
	if(min>max || (min==0 && max==0) || min==max){
		if(min>max){ bootbox.alert({ 
			title: 'Alert',
    	    size: 'small',
    	    message: "Make sure Min Value less than max Value", 
    	});}
		if(min==0 && max==0) {
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "Make sure Min, Max Values both not zero", 
	    	});
		}
		if(min==max){
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "Make sure Min, Max Values both not equal", 
	    	});
		} 
	}
	else{
		//var num=0;
		var chartid = svgid.replace('divid','');
		updateChartType(chartid,10,min+'@minmax@'+max);
		if(typeof slct == "undefined" ){
			ele = ele.replace('FT','s');
			slct = document.getElementById(''+ele);
			var opt = d3.select(slct).selectAll('option');
			opt.attr('selected','selected');
		}
		for(var i=0,num=0;i<this.length;i++){
			if(slct[i].selected){num=num+1;}
			//6-220, 2-420, 
		}
		for(var i=0,j=0;i<slct.length;i++){
			if(j<allowed){
			if(slct[i].selected){
			createGauge(slct[i].text, slct[i].text, min, max,ele,num);
			values[j]=parseFloat(isNaN(slct[i].value)?0:slct[i].value);j++;
			}
			}
			else {
				var did = ele.split('_')[1];
				x=$("#"+did).position();
				bootbox.alert({ 
					title: 'Alert',
		    	    size: 'small',
		    	    message: "Only first "+allowed+" Gauges are visible", 
		    	});
				break;
			}
		}
		ele=[];
	}}
	};
	function updateGauges(){
		var i=0;
		for (var key in gauges)
		{
		var value =values[i];//ele.options[ele.selectedIndex].value;
		gauges[key].redraw(value);
		i++;
		}
	} 
	
	//End
	var allowed;
	function frameGauge(divid,layoutchoice,chartid){//divid,chartName,layoutchoice,id){
		if(showHide == "true"){bootbox.alert({ 
			title: 'Alert',
    	    size: 'small',
    	    message: "Please Maximize", 
    	});}else{
		/*var jjdata = $('#data_'+divid).val();*/
    		var currentDiv=$('div[id^='+divid+']');
    		var jjdata ="" ;
    		id=currentDiv.attr('id').split('@')[0];
    		var currentlevel=currentDiv.attr('id').split('@')[1];
    		if(currentlevel!=null && currentlevel!='undefined'){
    			if($('input[id^='+chartid+']'+'[id*='+currentlevel+']').length>0)
    				{
    				jjdata =	$('input[id^='+chartid+']'+'[id*='+currentlevel+']').val();
    				}else{
    				jjdata =	$('#data_divid'+chartid).val();
    				}
    		}else{
    			jjdata = $('#data_'+id).val();
    		}
    		currentDiv.empty();
     		/*$("#"+id).empty();*/

		var minmax = $('#minmax_'+divid).val();//'0@minmax@30';//
		$('div[id^='+divid+']').empty();
		$('div[id^='+divid+']').attr("align", "right");
		var brwsrSupport = getInternetExplorerVersion();
		if(layoutchoice == '1')
			allowed = 6;
		else
			allowed = 2;
		if (brwsrSupport=='fus') 
		{
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "This feature is not supported in Internet Explorer 8.0", 
	    	});
		}
		else{
	        ///////////////////////////////////////////
			//updateChartType(chartid,10);
	        var yval = new Array();
	    	var xval = new Array();var xvals = new Array();
	    	var str = new Array();
	    	var keys = new Array();
	    	var yvalorig=new Array();
	    	keys[0] = "";
	    	var jdata = [];
	    	var arr = new Array();
			var test=new Array();
			var a= new Array();
			var b = new Array();
			var min,max;
	    	var type = jjdata.charAt(jjdata.length - 1);
	    	var t=[];
	    	t = jjdata.split("],");	
			if(type == 2 ||type == 3){
				var e = new Array();
				var f = t[0].split(",");
				for ( var i = 0; i < f.length; i++) {
					
					xvals[i] = f[i].substring(f[i].indexOf("{") + 1, f[i]
							.indexOf(":"));
				}
				var k = 0;
				for ( var i = 0; i < t.length; i++) {
					
					test[i] = t[i].substring(t[i].indexOf("{"), t[i]
							.lastIndexOf("}") + 1);
					e[i] = t[i].substring(0, t[i].indexOf("["));
					var x = test[i].split(",");
					for ( var j = 0; j < x.length; j++) {
						arr[k++] = x[j];
					}
				}
				for ( var n = 0; n < arr.length; n++) {
					a[n] = arr[n].substring(arr[n].indexOf("{") + 1, arr[n]
							.indexOf(":"));
					b[n] = parseFloat(arr[n].substring(arr[n].indexOf(":") + 1,
							arr[n].indexOf("}")));
				}
				for ( var i = 0,k=0; i < e.length; i++) {
					for ( var j = 0; j < xvals.length; j++) {
						var v = b[k];
						if(isNaN(v)){
							b[k]=0;
						}
						jdata.push({
							key : xvals[j]+ (e[i]!=null?' : '+e[i]:''),
							value : parseFloat(b[k])
						});
						k++;
					}
			}
				if(minmax==undefined || minmax==''){
					min = Math.min.apply(null, b),
					max = Math.max.apply(null, b);
	    		}
			}else{
		    	str = jjdata.split("~,");
		    	for (i in str) {
		    		xval[i] = str[i].substring(str[i].indexOf("{") + 1, str[i].indexOf(":"));
		    		yvalorig[i]=str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}"));
		    		yval[i] = parseFloat(str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}")));
		    	}
		    	var maxlen = 0;
		    	for (var i=0; i<yvalorig.length; i++) {
		    	  if (yvalorig[i].length>maxlen) {
		    	    maxlen = yvalorig[i].length;
		    	  }
		    	}
		    	var k = 0;
	    		for ( var j = 0; j < xval.length; j++) {
	    			xval[j] = xval[j].replace(/amp;/g, '');
	    			jdata.push({//[i].values.push({
	    				key : xval[j],
	    				value : isNaN(yval[k])?0:yval[k]
	    			});
	    			k++;
	    		}
	    		if(minmax==undefined || minmax==''){
	    			min = Math.min.apply(null, yval),
	    	    	max = Math.max.apply(null, yval);
	    		}
			}
    		
    		
    		min=isNaN(min)?0:min;
    		max=isNaN(max)?0:max;
    		if(minmax!=undefined && minmax!=''){
    			min = minmax.split('@minmax@')[0];max = minmax.split('@minmax@')[1];
    		}
    		min = parseFloat(min).toFixed(2);
    		max = parseFloat(max).toFixed(2);
    		updateChartType(chartid,10);
    		try{
   			document.getElementById(divid).innerHTML="";
		      d3.select("#gs_"+divid).remove();
		      d3.select("#ds_"+divid).remove();
		      }catch(e){}
		      d3.select('div[id^='+divid+']').append("div").attr("id","gs_"+divid).attr("class","gaugediv");
		      var selectDIV =d3.select('div[id^='+divid+']').append('div').attr('class',"selectdiv").attr('id','ds_'+divid);
		      var selectUI = selectDIV.append("select").attr("size",20).attr("id","s_"+divid).attr("multiple","multiple")
		      				//.on('change',createGauges)
		      				;
		      d3.select("#s_"+divid).attr("class","selectbox");
		      selectUI.selectAll("option").data(d3.values(jdata)).enter().append("option").text(function(d) {
		          return d.key;
		      });
		      selectUI.selectAll("option").data(d3.values(jdata)).attr("value", function(d) {
		    	  //alert(d.key+" "+d.value);
		          return parseFloat(d.value);
		      });   
		      var minmaxText = "<table class=minMaxTable>"+
		      					"<tr><td><lable>Minimum Value</lable></td></tr>"+
		      					"<tr><td><input id=min_"+divid+" type='text' value="+min+" class=gaugeminmax></td></tr>"+
		      					"<tr><td><lable>Maximum Value</lable></td></tr>"+
		      					"<tr><td><input id=max_"+divid+" type='text' value="+max+" class=gaugeminmax></td></tr>"+
		      					"<tr><td align=center><a href=javascript:void(0) id=btn_"+divid+" value="+max+" class=btn onclick=createGauges('FT_"+divid+"')>Update</a></td></tr>"
		      					;
		      $('#ds_'+divid).append(minmaxText);
		//createGauges_first(divid,chartName);
		setInterval(updateGauges, 1000);
		createGauges('FT_'+divid);//First Time
		//$("#"+"s_"+divid+" option:first-child").attr("selected", "selected").change(createGauges);//.trigger('change');
		}
	}}
	
	function showBulletChart(divid,lc,chartid)
	{
		
		
		var currentDiv=$('div[id^='+divid+']');
		var jjdata="" ;
		id=currentDiv.attr('id').split('@')[0];
		var currentlevel=currentDiv.attr('id').split('@')[1];
		
		if(currentlevel!=null && currentlevel!='undefined'){
			
			if($('input[id^='+chartid+']'+'[id*='+currentlevel+']').length>0)
				{
				jjdata=	$('input[id^='+chartid+']'+'[id*='+currentlevel+']').val();
				}else{
				jjdata=	$('#data_divid'+chartid).val();
				}
			
		}else{
			jjdata= $('#data_'+divid).val();
		}
		currentDiv.empty();

		
		if(showHide == "true"){bootbox.alert({ 
			title: 'Alert',
    	    size: 'small',
    	    message: "Please Maximize", 
    	});}else{
		var brwsrSupport = getInternetExplorerVersion();
		if (brwsrSupport=='fus') 
		{
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "This feature is not supported in Internet Explorer 8.0", 
	    	});
		}
		else{
			updateChartType(chartid,9);
			//alert("Not yet implemented properly, this is all static data");
			var margin = {top: 21, right: 10, bottom: 10, left: 120};
	        var w = d3.select('div[id^='+divid+']').style('width');
			w = parseInt(w);
	      var width = w ,//- margin.left - margin.right,
	        height = 58 ;//- margin.top - margin.bottom;   
		
	      $('div[id^='+divid+']').innerHTML="";
		var chart = d3.bullet()
	    .width(width - margin.left - margin.right-15)//width/2 + width/3)
		var json = [
			      	  {"title":"Revenue","ranges":[270],"measures":[250,270],"markers":[220]},
			      	  {"title":"Profit","ranges":[33],"measures":[26,33],"markers":[21]},
			      	  {"title":"Order Size","ranges":[620],"measures":[550],"markers":[100]},
			      	  {"title":"New Customers","ranges":[2150],"measures":[2100],"markers":[1000]},
			      	  {"title":"Satisfaction","ranges":[4.7],"measures":[4.4],"markers":[3.2]}
			      	];
		//=========================================
		/*var jjdata = $('#data_'+divid).val();*/
		var minmax = $('#minmax_'+divid).val();
		//$('#'+divid).attr("align", "right").addClass('bulletDiv');
		var brwsrSupport = getInternetExplorerVersion();
		if (brwsrSupport=='fus') 
		{
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "This feature is not supported in Internet Explorer 8.0", 
	    	});
		}
		else{
	        ///////////////////////////////////////////
	        var yval = new Array();
	    	var xval = new Array();var xvals = new Array();
	    	var str = new Array();
	    	var keys = new Array();
	    	var yvalorig=new Array();
	    	keys[0] = "";
	    	var jdata = [];
	    	var arr = new Array();
			var test=new Array();
			var a= new Array();
			var b = new Array();
			var min,max;
	    	var type = jjdata.charAt(jjdata.length - 1);
	    	var t=[];
	    	var y = [];
	    	t = jjdata.split("],");
	    	if(type == 3){
	    		var e = new Array();
				var f = t[0].split(",");
				for ( var i = 0; i < f.length; i++) {
					xvals[i] = f[i].substring(f[i].indexOf("{") + 1, f[i].indexOf(":"));
				}
				var k = 0;
				for ( var i = 0; i < t.length; i++) {
					test[i] = t[i].substring(t[i].indexOf("{"), t[i].lastIndexOf("}") + 1);
					e[i] = t[i].substring(0, t[i].indexOf("["));
					var x = test[i].split(",");
					y[i] = [];
					for ( var j = 0; j < x.length; j++) {
						arr[k++] = x[j];
						y[i][j] = parseFloat(x[j].substring(x[j].indexOf(":") + 1,x[j].indexOf("}")));
					}
				}
				if(minmax==''){
					min = Math.min.apply(null, b),
					max = Math.max.apply(null, b);
					min=isNaN(min)?0:min;
	        		max=isNaN(max)?0:max;
	    		}else{
	        		min = minmax.split('@minmax@')[0];max = minmax.split('@minmax@')[1];
	        		min = parseInt(min);max = parseInt(max);
	    		}
				for ( var i = 0,k=0; i < e.length; i++) {
					for ( var j = 0; j < xvals.length; j++) {
						xvals[j] = xvals[j].replace(/amp;/g, '');
						var v = b[k];
						if(isNaN(v)){
							b[k]=0;
						}
						jdata.push({
							title : xvals[j]+ (e[i]!=null?' : '+e[i]:''),
							markers : [parseFloat(y[1][j])],
							ranges:[y[1][j]],
							measures:[y[0][j],y[1][j]],
							labels:[e[0],e[1]]
						});
						k++;
					}
				}
	    	}
	    	else if(type == 2 ){//||type == 3){
				var e = new Array();
				var f = t[0].split(",");
				for ( var i = 0; i < f.length; i++) {
					xvals[i] = f[i].substring(f[i].indexOf("{") + 1, f[i].indexOf(":"));
				}
				var k = 0;
				for ( var i = 0; i < t.length; i++) {
					test[i] = t[i].substring(t[i].indexOf("{"), t[i].lastIndexOf("}") + 1);
					e[i] = t[i].substring(0, t[i].indexOf("["));
					var x = test[i].split(",");
					for ( var j = 0; j < x.length; j++) {
						arr[k++] = x[j];
					}
				}
				for ( var n = 0; n < arr.length; n++) {
					a[n] = arr[n].substring(arr[n].indexOf("{") + 1, arr[n].indexOf(":"));
					b[n] = parseFloat(arr[n].substring(arr[n].indexOf(":") + 1,arr[n].indexOf("}")));
				}
				if(minmax==''){
					min = Math.min.apply(null, b),
					max = Math.max.apply(null, b);
					min=isNaN(min)?0:min;
	        		max=isNaN(max)?0:max;
	    		}else{
	        		min = minmax.split('@minmax@')[0];max = minmax.split('@minmax@')[1];
	        		min = parseInt(min);max = parseInt(max);
	    		}
				for ( var i = 0,k=0; i < e.length; i++) {
					for ( var j = 0; j < xvals.length; j++) {
						xvals[j] = xvals[j].replace(/amp;/g, '');
						var v = b[k];
						if(isNaN(v)){
							b[k]=0;
						}
						jdata.push({
							title : xvals[j]+ (e[i]!=null?' : '+e[i]:''),
							markers : [parseFloat(b[k])],
							ranges:[max],
							measures:[min,max]
						});
						k++;
					}
				}
			}else{
		    	str = jjdata.split("~,");
		    	for (i in str) {
		    		xval[i] = str[i].substring(str[i].indexOf("{") + 1, str[i].indexOf(":"));
		    		yvalorig[i]=str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}"));
		    		yval[i] = parseFloat(str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}")));
		    	}
		    	var maxlen = 0;
		    	for (var i=0; i<yvalorig.length; i++) {
		    	  if (yvalorig[i].length>maxlen) {
		    	    maxlen = yvalorig[i].length;
		    	  }
		    	}
		    	if(minmax==''){
	    			/*min = Math.min.apply(null, yval),
	    	    	max = Math.max.apply(null, yval);*/
		    		min=0;max=0;
		    		for(var i=0;i<yval.length;i++){
		    			if(!isNaN(yval[i])){
		    				min = yval[i] < min ? yval[i] : min;
		    				max = yval[i] > max ? yval[i] : max;
		    			}
		    		}
	    			min=isNaN(min)?0:min;
	        		max=isNaN(max)?0:max;
	    		}else{
	        		min = minmax.split('@minmax@')[0];max = minmax.split('@minmax@')[1];
	        		min = parseInt(min);max = parseInt(max);
	    		}
		    	var k = 0;
	    		for ( var j = 0; j < xval.length; j++) {
	    			xval[j] = xval[j].replace(/amp;/g, '');
	    			jdata.push({//[i].values.push({
	    				title : xval[j],
	    				markers : [isNaN(yval[k])?0:yval[k]],
	    				ranges:[max],
	    				measures:[min,max]
	    			});
	    			k++;
	    		}
			}
			
		//=========================================
		
	d3.select('div[id^='+divid+']').append('div').attr('id','bullet1Div_'+divid).attr("style","width:auto !important;");
	$('#bullet1Div_'+divid).attr("align", "right").addClass('bullet1Div');
	  var svg = d3.select('div[id^='+divid+']').selectAll("svg")
	      .data(jdata)//data)
	    .enter().append("svg")
	      .attr("class", "bullet")
	      .attr("style","height:71px;")
	      //.attr("width", width - margin.left - margin.right-10)
	      //.attr("height", height)// + margin.top + margin.bottom)
	    .append("g")
	      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
	      .call(chart);

	  var title = svg.append("g")
	      .style("text-anchor", "end")
	      .attr("transform", "translate(-6," + height / 2 + ")");

	  title.append("text")
	      .attr("class", "title")
	      .attr('title',function(d) { return d.title; })
	      .text(function(d) { return d.title.length>14 ? d.title.substring(0,12)+'..':d.title; })
	      .append('title')
	      .text(function(d) { return d.title; })
	      ;

	  /*title.append("text")
	      .attr("class", "subtitle")
	      .attr("dy", "1em")
	      .attr('title',function(d) { return d.subtitle; })
	      .text(function(d) { return d.subtitle; });*/
	  var selectDIV =d3.select("#"+divid).append('div').attr('class',"bullet2Div row").attr('id','bullet2Div_'+divid);
	  	var minLableDiv =selectDIV.append('div').attr('class',"col-md-2").attr('id','minLableDiv_'+divid);
	  	minLableDiv.append("lable").text("Minimum Value").attr("class","minmax");
	  	var minValDiv =selectDIV.append('div').attr('class',"col-md-2").attr('id','minValueDiv_'+divid);
	  	minValDiv.append("input").attr('type', 'text').attr("id","min_"+divid).attr("value",min).attr("class","bulletminmax").style("width","auto");
	  	var maxLableDiv =selectDIV.append('div').attr('class',"col-md-2").attr('id','maxLableDiv'+divid);
	  	maxLableDiv.append("lable").text("Maximum Value").attr("class","minmax");
	  	var maxValDiv =selectDIV.append('div').attr('class',"col-md-2").attr('id','maxValDiv'+divid);
	  	maxValDiv.append("input").attr('type', 'text').attr("id","max_"+divid).attr("value",max).attr("class","bulletminmax").style("width","auto");
	  	var updateBtnDiv =selectDIV.append('div').attr('class',"col-md-2").attr('id','updateBtnDiv'+divid);
	  	updateBtnDiv.append('a').attr('href',"javascript:void(0)").attr("class","btn").attr("id","btn_"+divid).text("Update").on('click',createGauges);//.attr("class","gaugeminmax");
	  d3.select("#btn_"+divid).on("click", function() {
		  var min=0,max=0;
		  min = $('#min_'+divid).val();
		  max = $('#max_'+divid).val();
		  d3.select("#minmax_"+divid).attr('value',min+'@minmax@'+max);
		  updateChartType(chartid,9,min+'@minmax@'+max);
		  showBulletChart(divid,lc,chartid);
	  });
	}
	}
	}
	}
	
function frameFunnel(divid,chartid,lc){//divid,chartName){
	var currentDiv=$('div[id^='+divid+']');
	var jjdata="" ;
	divid=currentDiv.attr('id').split('@')[0];
	var currentlevel=currentDiv.attr('id').split('@')[1];
	
	if(currentlevel!=null && currentlevel!='undefined'){
		
		if($('input[id^='+chartid+']'+'[id*='+currentlevel+']').length>0)
			{
			jjdata=	$('input[id^='+chartid+']'+'[id*='+currentlevel+']').val();
			}else{
			jjdata=	$('#data_divid'+chartid).val();
			}
	}else{
		jjdata= $('#data_'+divid).val();
	}
	currentDiv.empty();
	
	if(showHide == "true"){
		bootbox.alert({ 
			title: 'Alert',
    	    size: 'small',
    	    message: "Please Maximize", 
    	});
	}else{
		//alert("funnel here");
	/*var jjdata=$('#data_'+divid).val();*/
	updateChartType(chartid,8);//8);
	var c = $('div[id^='+divid+']'); 
	 while (c.firstChild) { //$('#'+divid).siblings().css("display","none");
		 $('div[id^='+divid+']').attr("class", "");
		c.removeChild(c.firstChild); }
		 var yval = new Array();
	    	var xval = new Array();
	    	var str = new Array();
	    	var keys = new Array();
	    	var yvalorig=new Array();
	    	keys[0] = "";var dataa =[];
	    	//jjdata="{Jan-2008:10}~,{Feb-2008:9}~,{Mar-2008:1110}~,{Apr-2008:1300.1605}";
	    	//jjdata="{Jan-2011:7257.51}~,{Feb-2011:49403.72}1";
	    	var type = jjdata.charAt(jjdata.length - 1);
	    	if(type==2 || type==3){
	    		var crits=[];
	    		crits=jjdata.split('],');
	    		
	    		for(var c=0;crits.length>c;c++){
	    			keys[c]=crits[c].split('[')[0];
	    			if(crits[c].indexOf('[') != -1) {
	    			  str = crits[c].split('[')[1].split('~,');
	    			} else {
	    				str = crits[c].split('~,');
	    			}
				
			    	for (i in str) {
			    		var v = dataa.length;
			    		dataa[v]=[];
			    		xval[i] = str[i].substring(str[i].indexOf("{") + 1, str[i].indexOf(":"))+(keys[c]==undefined?"":" - "+keys[c]);
			    		xval[i] = xval[i].replace(/amp;/g, '');
			    		yvalorig[i]=str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}"));
			    		yval[i] = parseFloat(str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}")));
									dataa[v][0]=xval[i];dataa[v][1]=isNaN(yval[i])?0:yval[i];
			    	}
			    }
	    	}
	    	else{
		    	str = jjdata.split("~,");
				
		    	for (i in str) {
		    		dataa[i]=[];
		    		xval[i] = str[i].substring(str[i].indexOf("{") + 1, str[i].indexOf(":"));//+(keys[0]==undefined?"":" - "+keys[0]);
		    		xval[i] = xval[i].replace(/amp;/g, '');
		    		yvalorig[i]=str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}"));
		    		yval[i] = parseFloat(str[i].substring(str[i].indexOf(":") + 1, str[i].indexOf("}")));
								dataa[i][0]=xval[i];dataa[i][1]=isNaN(yval[i])?0:yval[i];
		    	}
	    	}
			//===============
			var sortOrder = true; 
			if( !sortOrder || sortOrder == null)
					sortOrder = true;
					else sortOrder = false;
			var historicalBarChart11 ;
					var barchartdata1 = JSON.parse(JSON.stringify(dataa));//cloning object
					historicalBarChart11 = barchartdata1;
				    historicalBarChart11.sort(function(a,b){
				    	if(sortOrder)
				    		return a[1]-b[1];
				    	else
				    		return b[1]-a[1];
				    });
				    
				   d3.select('div[id^='+divid+']').attr('class','table-responsive').style("display",'block');
				  /* if(lc==1)$('#'+divid).parent().css("width","1165px");
				   else if(lc==2)$('#'+divid).parent().css("width","570px");
				   else if(lc==3)$('#'+divid).parent().css("width","370px");*/
				   var w = $('div[id^='+divid+']').parent().css("width");
				   var h = d3.select('div[id^='+divid+']').style("height");
				   w =Math.round(parseFloat(w));
				   h =Math.round(parseFloat(530));
				   
			//===============
			 var chart = new FunnelChart({
			 				data: barchartdata1,
			 				id: divid,
			 				width: w-(100/lc), 
			 				height: h, 
			 				bottomPct: 1/2,
			 				layout:lc
			 			});
			 chart.draw('div[id^='+divid+']', 2);
	}
}
function frameChartLinearRegression(id,divid,jsonstring,xname,yname,layoutchoice,chartid,updateType,userType,timeCol)//,mapdiv,bigdiv,bigchart)
{
	$("#"+id).empty();
	

	var testxval=new Array();
	
	var yvalarea=new Array();
	var str=new Array();
	var xval=new Array();
	var earea = new Array();
	var arrarea = new Array();
	
	
	
	var testarea=new Array();
	var xvalarea=new Array();
	
	var brwsrSupport = getInternetExplorerVersion();
	if (brwsrSupport=='fus') 
	{
		bootbox.alert({ 
			title: 'Alert',
    	    size: 'small',
    	    message: "This feature is not supported in Internet Explorer 8.0", 
    	});
	}
	
	else
		
		{
		
		/*var state3 = document.getElementById(bigdiv).style.display;
		var state4 = document.getElementById(bigchart).style.display;
		if(state3=='block')
			{
			
			document.getElementById(bigdiv).style.display='none';
			}
		if(state4=='block')
			{
			
			document.getElementById(bigchart).style.display='none';
			}*/
		
		/*var state = document.getElementById(id).style.display;
		 
	        if (state == 'none') {
	            document.getElementById(id).style.display = 'block';
	           
	            
	        } 
	      */
		
		if((jsonstring=="NA1")||(jsonstring=="NA2"))
		{
			bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "chart can't be found for empty data", 
	    	});
		}
		else
			{
			
			/*var state=document.getElementById("id");
			if(state==null)
				{
				 state='none'
				}*/
			
	
	 var t=jsonstring.split("],");
		
		
var  f=t[0].split("~,");
var strFlag=false;

for(var i=0;i<f.length;i++)
{

xvalarea[i]=f[i].substring(f[i].indexOf("{")+1,f[i].indexOf(":"));
if(isNaN(xvalarea[i]))
{
strFlag = true;
}

}
//alert("flag::::::is"+strFlag);
var k = 0;
for(var i=0;i<t.length;i++)
{

testarea[i]=t[i].substring(t[i].indexOf("{"),t[i].lastIndexOf("}")+1);
//alert("testing i::::::::"+test[i] );
earea[i] = t[i].substring(0,t[i].indexOf("["));
//alert(e[i]);
if(earea[i]=="")
{

earea[i]=yname;
}

//e[i] = test[i];
//alert("eeeeeeeeee"+e[i]);
var x = testarea[i].split(",");
//alert("X LENGTH :"+x.length);
 for(var j=0;j<x.length;j++){

    arrarea[k++] = x[j];
	}

}

var month = "";
var day="";
var b = new Array();
var yvalorig=new Array();
for(var n=0; n<arrarea.length;n++){


	b[n] =parseFloat( arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}")));
	yvalorig[n]=arrarea[n].substring(arrarea[n].indexOf(":")+1,arrarea[n].indexOf("}"));
	
	
	

}
		}

		var maxlen = 0;
		for (i=0; i<yvalorig.length; i++) {
			//alert("length yval is"+yval.length);
			
		  if (yvalorig[i].length>maxlen) {
			  //alert("test length");
		    maxlen = yvalorig[i].length;
		   
		  }
		  
		}

		if(maxlen>=13)
			{
			var left=160;
			}
		else
			left=100;

	

	
	if(layoutchoice==1)
	{
	var width;
	if(userType=='all')
		{
		width=800;
	}
	if(userType=='indi')
		{
		width=1100;
		}
	}
	if(layoutchoice==2)
	{
	 var width;
		if(userType=='all')
			{
			width=370;
		}
		if(userType=='indi')
			{
			width=500;
			}
	}
	 if(layoutchoice==3)
		{
		 var width;
			if(userType=='all')
				{
				width=250;
			}
			if(userType=='indi')
				{
				
				width=300;
				}
		}
	 
	 if(strFlag==true)
		 {
		 
		 if(xval.length>15&&layoutchoice==1)
			{
			 alert(document.getElementById(id).style.display);
			 document.getElementById(id).style.display='none';
			 
			 document.getElementById(bigdiv).style.display='block';
			 document.getElementById(bigchart).style.display='block';
			
			}
		// document.getElementById(id).style.display = 'none';
		 bootbox.alert({ 
				title: 'Alert',
	    	    size: 'small',
	    	    message: "X-Axis should be numeric for Linear Scatter Plot", 
	    	});
		
		 }
	 else if(strFlag==false)
		 {
		 if(updateType)
			{
			updateChartType(chartid,4);
			}
		 //var state = document.getElementById(id).style.display;
		 var c = document.getElementById(id);
		 while (c.firstChild) {
			 //$('#'+id).siblings().css("display","none");
				c.removeChild(c.firstChild);
			}
			/*if (state == 'none') {
				document.getElementById(id).style.display = 'block';
			}*/
		 $('#'+id).css('display','block');
			
		//alert("helloo It is time column");
		 var xval=new Array();
		 var yval=new Array();
		
		 nv.addGraph(function() {
			  var chart = nv.models.scatterPlusLineChart()
			                .showDistX(true)
			                .showDistY(true)
			                 .useVoronoi(true)
			               // .height(500)
			                .color(d3.scale.category10().range());

			   if(timeCol=="Y")
           	{
		  chart.xAxis.tickFormat(d3.format('d'));
		  chart.yAxis.tickFormat(d3.format('d'));
           	}
           else if(timeCol=="N")
           	{
           	 chart.xAxis.tickFormat(d3.format('.2f'));
				  chart.yAxis.tickFormat(d3.format('.2f'));
           	}
		  chart.xAxis.axisLabel(xname);
		  chart.showControls(false);//to remove stacked/group functionality
			chart.yAxis.axisLabel(yname);
			//chart.width(width);
			chart.margin({top: 65, right: 35, bottom: 80, left:left});

		  d3.select('#'+id).append("svg")
		    .datum(randomDataLinearRegression(jsonstring,yname))
		    .transition().duration(500)
		    .call(chart);

		  nv.utils.windowResize(chart.update);

		  return chart;
		});
		 }
		}
}

function bAlert(message, alertSize){
	//alertSize can be small, medium
	if(alertSize == null || alertSize === undefined || alertSize == ""){
		alertSize="small";
	}
	bootbox.alert({ 
		  size: alertSize,
		  title: "Alert",
		  message: message, 
		  callback: function(){ /* your callback code */ }
		});
}

var currentCountNew=0;
function loadPreviousGraph(value)
{
	var svgIdClicked=value.id;
	var backValues=$('input[id^='+svgIdClicked+']');
	if(backValues.length>1)
		{
		var array=$(backValues).map(function() { 
			return $(this).attr('id').split('_')[1];
        }).get();
		var level=Math.max(...array);
		/*$('#1796@0@level_2').remove();*/
		$('input[id^='+svgIdClicked+']'+'[id*=level_'+level+']').remove();
		}else{
			backValues.remove();
			}
	var heirarchy= $('input[id^='+svgIdClicked+']').map(function() {
		return $(this).attr('id').split('_')[1];
	}).get();
	
	if($('input[id^='+svgIdClicked+']').length > 0)
		{
		previousGraphJsonData=$('input[id^='+svgIdClicked+']').val();
		}else{
			previousGraphJsonData=$('#data_divid'+svgIdClicked).val();
		}
	
		var yval = new Array();
		var xval = new Array();
		var str = new Array();
		var keys = new Array();
		keys[0] = "";
		str = previousGraphJsonData.split("~,");
		for (i in str) {
			str[i] = str[i].substring(str[i].indexOf("{") + 1, str[i]
					.lastIndexOf("}"));
			xval[i] = str[i].split(":")[0].trim();
			yval[i] = parseFloat(str[i].split(":")[1]);
		}
		var barchartdata = [];
		var k = 0;
		for ( var i = 0; i < keys.length; i++) {
			barchartdata.push({
				key : keys[i],
				values : []
			});
			for ( var j = 0; j < xval.length; j++) {
				barchartdata[i].values.push({
					label : xval[j],
					value : yval[k]
				});
				k++;
			}
		}
	
	var chart = nv.models.discreteBarChart().x(function(d) {
			return d.label;
		})
		.y(function(d) {
			return d.value;
		}).staggerLabels(false)
		/*.margin({top: 20, right: 10, bottom: 60, left:90})*/
		// .staggerLabels(barchartdata[0].values.length > 8)
		.showValues(true)
		.tooltips(true);
	
	if(xval.length>=10){
		chart.margin({bottom:60});
		chart.showValues(false);
		chart.xAxis.rotateLabels(-20);
		}
	
	var chartData;
	chartData=d3.select("#svg_divid"+svgIdClicked)
	       	.attr("class","bigsvg")
	       	.style('width','4000px')
	       	.datum(barchartdata);
	chartData
	       	.transition()
	       	.duration(500)
	       	.call(chart);
 	    nv.utils.windowResize(chart.update);
	
}