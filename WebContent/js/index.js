var jsonData = [
  {
    id  : 1,
    text : "Folder 1",
    state : {
      selected  : false
    },
    children    : [
      {
        id  : 2,
        text : "Sub Folder 1",
        state : {
          selected  : false
        },  
      },
      {
        id  : 3,
        text : "Sub Folder 2",
        state : {
          selected  : false
        },  
      }
    ]
  }, 
];

$('#jstree-tree')
  .on('changed.jstree', function (e, data) {
    var objNode = data.instance.get_node(data.selected);
    $('#jstree-result').html('Selected: <br/><strong>' + objNode.id+'-'+objNode.text+'</strong>');
  })
  .jstree({
  core: {
    data: jsonData
  }
});