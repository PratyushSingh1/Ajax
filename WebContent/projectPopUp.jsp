<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <%@ taglib uri="/struts-tags" prefix="s"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">
	function test()
	{
		/* console.log("hello"); */
		$.ajax({
			type:"POST",
			url:"test_getDepartmentsList.action",
			dataType:"json",
			success:function(data)
			{
				console.log(data);
				$.each(data,function(i,obj){
					/* console.log(obj); */
				$("#deptList").append('<option value="'+i+'">'+obj+'</option>')
				});
			}
		});
	};
	function getEmployeesForThisDept()
	{
		var selectedDept=$('#deptList').val();
		$.getJSON("test_getListOfEmployees.action?deptName="+selectedDept+"",function(data){
			console.log(data);
			$.each(data,function(i,obj){
				console.log(obj);
			$("#listOfEmployees").append('<option>'+obj+'</option>')
			});
		});
	};
	</script>
</head>
<body>
  <a onclick="test();" href="#"class="modal-toggle" data-toggle="modal" data-target="#myModal">Link Button</a>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-      labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="exampleModalLabel">Share Report To Employee</h4>
  </div>
  <div class="modal-body" id="modal-body">
  <label for="deptList">Select list (select one):</label>
      <select class="form-control" id="deptList" onchange="getEmployeesForThisDept();">
      </select>
      <label for="sel2">Mutiple select list (hold shift to select more than one):</label>
      <select multiple class="form-control" id="listOfEmployees">
      </select>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary">Add to cart</button>
  </div>
</div>

</body>
</html>