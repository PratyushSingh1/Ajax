<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>JstreeExample</title>
		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jstree/3.3.5/themes/default/style.min.css" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.5/jstree.min.js"></script>
		<script>
		$(function()   {
			var jsondata = [{"parent":"#","id":"1","text":"Time Management","type": "root"},{"parent":"#","id":"2","text":"Personal","type": "root"},
				{"parent":"#","id":"9","text":"testing","type": "root"},{"parent":"#","id":"10","text":"test demo","type": "root"},
				{"parent":"#","id":"11","text":"testing","type": "root"},
			
				{"parent":"1","id":"124_reports","text":"Department Wise Employee details","type": "child"},
				{"parent":"2","id":"125_reports","text":"Employee Annual Salary Details","type": "child"},
				{"parent":"2","id":"12","text":"xxxx","type": "root"}
				];
			/* first get data in json format then convert it to tree format*/
			createJsTree(jsondata);
			/*code written to get static data as js tree*/
		  $('#container').jstree();
		  $('#container').on("select_node.jstree", function (e, data) { alert("node_id: " + data.node.id); });
		});
		function createJsTree(jsondata)
		{
			$('#simpleJsTreeUsingJson').jstree({
				'core':{
					'data':jsondata,
					"check_callback" :function(operation, node, node_parent, node_position) {
	                 
						true;
						/*   if(node.type=='child')
	                    	{
	                    return true;  
	                    	}else
	                    return false; */
	                }  
				},//end of Core
				plugins: ["search", "themes", "types","dnd","contextmenu"],
				 types: {
					    "root": {
					      "icon" : " glyphicon glyphicon-folder-open text-warning"
					    },
					    "child": {
					      "icon" : "glyphicon glyphicon-file text-warning",
					      "valid_children" : []
					    },
					    "default" : {
					    }
					  },
					  "contextmenu": {
		                    "items": function ($node) {
		                        var tree = $("#simpleJsTreeUsingJson").jstree(true);
		                        return {
		                            "Create": {
		                                "separator_before": false,
		                                "separator_after": true,
		                                "label": "Create",
		                                "action": false,
		                                "submenu": {
		                                    "File": {
		                                        "seperator_before": false,
		                                        "seperator_after": false,
		                                        "label": "File",
		                                        action: function (obj) {
		                                            $node = tree.create_node($node, { text: 'New File', type: 'child', icon: 'glyphicon glyphicon-file' });
		                                            tree.deselect_all();
		                                            tree.select_node($node);
		                                        }
		                                    },
		                                    "Folder": {
		                                        "seperator_before": false,
		                                        "seperator_after": false,
		                                        "label": "Folder",
		                                        action: function (obj) {
		                                            $node = tree.create_node($node, { text: 'New Folder', type: 'root' });
		                                            tree.deselect_all();
		                                            tree.select_node($node);
		                                        }
		                                    }
		                                }
		                            },
		                            "Rename": {
		                                "separator_before": false,
		                                "separator_after": false,
		                                "label": "Rename",
		                                "action": function (obj) {
		                                    tree.edit($node);                                    
		                                }
		                            },
		                            "Remove": {
		                                "separator_before": false,
		                                "separator_after": false,
		                                "label": "Remove",
		                                "action": function (obj) {
		                                    tree.delete_node($node);
		                                }
		                            }
		                        };
		                    }
		                }
			});
			$('#simpleJsTreeUsingJson').on("move_node.jstree", function (e, data) { 
				var node=data.node;
				var  nodeparent= data.node_parent;
				console.log(node);
				console.log(nodeparent);
				console.log("Drop node " + data.node.id + " to " + data.parent); 
				$.ajax({
					url:"js_updateTree.action?childId="+data.node.id+"&parentId="+data.parent,
					success:function(data){
				
					}});//ajax Function call on dnd of a node
				});
			
			$('#simpleJsTreeUsingJson').on("create_node.jstree", function (e, data) { 
				var node=data.node;
				var  nodeparent= data.node_parent;
			/* 	node.id = "101"; */
				console.log(node);
				console.log(nodeparent);
				});		
			
			  $('#simpleJsTreeUsingJson').on('show_contextmenu.jstree', function(e, reference, element) {

				  
				  var reportIdClicked=reference.node.id;
				  
				  if(reportIdClicked.indexOf("_reports")>=0) {
				        $('.vakata-context').detach();
				    }

				});   
			/* $('#simpleJsTreeUsingJson').off("contextmenu.jstree", "$11_anchor"); */ 
		}
		</script>
</head>
<body>
<div id="container">
  <ul>
    <li>Root node
      <ul>
        <li>Child node 1</li>
        <li>Child node 2</li>
      </ul>
    </li>
  </ul>
</div>
<div id="simpleJsTreeUsingJson">

</div>
</body>
</html>