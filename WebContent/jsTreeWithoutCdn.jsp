<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>JstreeExample</title>
		  <link rel="stylesheet" href="jsTree/style/bootstrap.min.css">
  		<script src="jsTree/script/jquery.min.js"></script>
  		<script src="jsTree/script/bootstrap.min.js"></script>
		<script src="jsTree/script/jquery3.1.1.min.js"></script>
		<link rel="stylesheet" href="jsTree/style/jsTreeStyleWithImages/style.min.css" />
		<script src="jsTree/script/jstree.min.js"></script>
		<link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
		<script src="jsTree/script/jquery-resizable.js"></script>
		<link rel="stylesheet" href="jsTree/style/jsTreeStyleWithImages/style.min.css" />
		
		    <style>
        html, body {
            height: 100%;
            font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
            padding:0;
            margin: 0;
            overflow: auto;                           
        }
        .page-container { margin: 20px; }

        /* horizontal panel*/
        .panel-container {
            display: flex;
            flex-direction: row;   
            border: 1px solid silver;         
            overflow: hidden;                   
        }
        .panel-left,.panel2-left {
            flex: 0 0 auto;  /* only manually resize */
            padding: 10px;
            width: 300px;
            min-height: 200px;
            min-width: 150px;
            white-space: nowrap;
            
            background: #838383;
            color: white;
        }
        .splitter, .splitter2,.splitter3 {
            flex: 0 0 auto;
            width: 18px;
            background: url(../assets/vsizegrip.png) center center no-repeat #535353;
            min-height: 200px;
            cursor: col-resize;
        }
        .panel-right, .panel2-right {
            flex: 1 1 auto; /* resizable */
            padding: 10px;
            width: 100%;
            min-height: 200px;
            min-width: 200px !important;   /* NOTE: This won't be respected! Explicit size of left forces the size to be fixed */
            background: #eee;
        }

        /* vertical panel */
       .panel-container-vertical {
            display: flex;
            flex-direction: column;   
            height: 500px;
            border: 1px solid silver;         
            overflow: hidden;
        }
        .panel-top {
            flex: 0 0 auto;  /* only manually resize */
            padding: 10px;
            height: 150px;
            width: 100%;                        
            white-space: nowrap;
            
            background: #838383;
            color: white;
        }
        .splitter-horizontal {
            flex: 0 0 auto;
            height: 18px;
            background: url(../assets/hsizegrip.png) center center no-repeat #535353;            
            cursor: row-resize;
        }
        .panel-bottom {
            flex: 1 1 auto; /* resizable */
            padding: 10px;
            min-height: 200px !important; /* NOTE: This won't be respected! Explicit resize forces size of this panel */
            background: #eee;
        }
        label {
            font-size: 1.2em;
            display: block;
            font-weight: bold;
            margin: 30px 0 10px;
        }
        pre {
            margin: 20px;
            padding: 10px;
            background: #eee;
            border: 1px solid silver;
            border-radius: 4px;
            overflow: auto;
        }
    </style>
		<script>
		$(function()   {
			var jsondata = [{"parent":"#","id":"1","text":"Time Management","type": "root"},{"parent":"#","id":"2","text":"Personal","type": "root"},
				{"parent":"#","id":"9","text":"testing","type": "root"},{"parent":"#","id":"10","text":"test demo","type": "root"},
				{"parent":"#","id":"11","text":"testing","type": "root"},
			
				{"parent":"1","id":"124_reports","text":"Department Wise Employee details","type": "child"},
				{"parent":"2","id":"125_reports","text":"Employee Annual Salary Details","type": "child"},
				{"parent":"2","id":"12","text":"xxxx","type": "root"}
				];
			/* first get data in json format then convert it to tree format*/
			createJsTree(jsondata);
			/*code written to get static data as js tree*/
		  $('#container').jstree();
		  $('#container').on("select_node.jstree", function (e, data) { alert("node_id: " + data.node.id); });
          
		  $(".search-input").keyup(function () {
              var searchString = $(this).val();
              $('#simpleJsTreeUsingJson').jstree('search', searchString);
          });
		    $(".panel-left").resizable({
		        handleSelector: ".splitter",
		        resizeHeight: false
		    });

		    $(".panel2-left").resizable({
		        handleSelector: ".splitter2",
		        resizeHeight: false
		    });

		    $(".panel-top").resizable({
		        handleSelector: ".splitter-horizontal",
		        resizeWidth: false
		    });

		});
		function createJsTree(jsondata)
		{
			$('#simpleJsTreeUsingJson').jstree({
				'core':{
					'data':jsondata,
					"check_callback" :function(operation, node, node_parent, node_position) {
	                 
						true;
						/*   if(node.type=='child')
	                    	{
	                    return true;  
	                    	}else
	                    return false; */
	                }  
				},//end of Core
				plugins: ["search", "themes", "types","dnd","contextmenu"],
				"search":{
					"case_sensitive":true,
					"show_only_matches":true,
					"show_only_matches_children":false,
					"search_leaves_only":false
				},
				 types: {
					    "root": {
					      "icon" : " glyphicon glyphicon-folder-open custom" 
					    },
					    "child": {
					      "icon" : "glyphicon glyphicon-file custom", 
					      "valid_children" : []
					    },
					    "default" : {
					    }
					  },
					  "contextmenu": {
		                    "items": function ($node) {
		                        var tree = $("#simpleJsTreeUsingJson").jstree(true);
		                        return {
		                            "Create": {
		                                "separator_before": false,
		                                "separator_after": true,
		                                "label": "RunReportInNewTab",
		                                "action": false
		                            }
		                        };
		                    }
		                }
			});
			$('#simpleJsTreeUsingJson').on("move_node.jstree", function (e, data) { 
				var node=data.node;
				var  nodeparent= data.node_parent;
				var oldParent=data.old_parent;
				var newParent=data.parent;
				data.parent==oldParent;
				});
			
			$('#simpleJsTreeUsingJson').on("create_node.jstree", function (e, data) { 
				var node=data.node;
				var  nodeparent= data.node_parent;
			/* 	node.id = "101"; */
				console.log(node);
				console.log(nodeparent);
				});		
			
			  $('#simpleJsTreeUsingJson').on('show_contextmenu.jstree', function(e, reference, element) {

				  
				  var reportIdClicked=reference.node.id.length;
				  
				  if(reportIdClicked<4) {
				        $('.vakata-context').detach();
				    }

				});   
			/* $('#simpleJsTreeUsingJson').off("contextmenu.jstree", "$11_anchor"); */ 
		}
		</script>
		<style type="text/css">
		.custom{
		color: #ffff00;
		}
		.jstree-default a.jstree-search { color:#000000; }
		</style>
</head>
<body>
<div class="page-container">
        <div class="panel-container">

            <div class="panel-left">
                left panel
            </div>

            <div class="splitter">
            </div>
         

            <div class="panel-right">
                right panelidisdvishbvibvishbvsihvblshvbsidhvbisdhvbsdhvbdsvhbihsbishbvishbvishdbvsihdvbsdhbvsidhvbsidhvbsdihvbsdhbdsvhbsdvihdbsvihdsbvishdbvishdvbidshvhbsidhvbDSIVHBSDIVHBSDIVHBSDIVHBSDIVHBSIDVHBSDIHVBSDIHVBSIVHBSDIVHBSDVIBSDVIHSBDVIHSDBV
            </div>
        </div>
     </div>

<div id="container">
  <ul>
    <li>Root node
      <ul>
        <li>Child node 1</li>
        <li>Child node 2</li>
      </ul>
    </li>
  </ul>
</div>
 <input id="search-input" class="search-input" />
<div id="simpleJsTreeUsingJson">

</div>
</body>
</html>