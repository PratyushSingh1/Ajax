<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="javascript/chosen.jquery.js"></script>
<script type="text/javascript" src="javascript_bootstrap/alertify.js"></script>
<script type="text/javascript">
function unshareReportToEmployee(reportId)
{
	var dept2 = $('#empListValue').val();
		var deleteReportEmp = $('#deleteEmpList').val();
		var empSelectedList = $('#empSelectedList').val();
	  	if(empSelectedList == '' || empSelectedList == null) {
			jAlert('Please select at least one employee to UnShare report.');
		}else
			{
			/* $('#addReportEmpForm').attr('action', 'qb_unshareReportToEmployee.action?reportId='+reportId+'&dept2='+dept2+'&selectedEmp='+selectedEmp+'&deleteReportEmp='+deleteReportEmp).submit(); */
			$('#addReportEmpForm').attr('action', 'qb_unshareReportToEmployee.action?reportId='+reportId+'&dept2='+dept2+'&selectedEmp='+empSelectedList).submit();
			}
}
	function showEmployeeList(reportId) {
		<!-- $('#PleaseWaitDiv').show(); -->
		var deptId = $('#empListValue').val();
		if(deptId != '') {			
			$.ajax({
				url : 'qb_getListOfEmployees.action?deptId='+deptId+'&reportId='+<%=request.getAttribute("reportId")%>,
				async :false,
				success : function(data) {
			    	$("#empList").empty();
			    	$("#empSelectedList").empty();
			    	var options = '';
	                $.map(data.empJson, function (val, ind) {
	                	if(typeof val.empId != 'undefined') {                		
	                		options += '<option value="'+ val.empId +'">'+ val.empName +'</option>';
	                	}
	                });
	                $('#empList').append(options);
	                var options2 = '';
	                $.map(data.selectedEmp, function (val, ind) {
	                	if(typeof val.empId != 'undefined') {                		
	                		options2 += '<option value="'+ val.empId +'">'+ val.empName +'</option>';
	                	}
	                });
	                $('#empSelectedList').append(options2);
				}
			});
		} else {
			jAlert('Please select department');
			$("#empList").empty();
	    	$("#empSelectedList").empty();
		}
	}
	
	$(document).ready(function(){
		if($.browser.mozilla){
			$('#leftRightBtn_Img').css({'margin-top':'122px'});
			$('#moveRightBtn_Img').css({'margin-top':'-278px'});
		}else{
			$('#leftRightBtn_Img').css({'margin-bottom':'190px'});
			$('#moveRightBtn_Img').css({'margin-top':'-42px'});
		}
		var deleteEmp = [];
		$('.chzn-select-deselect').chosen();
		$('#moveRightBtn').click(function() { 
			var options = $('#empList option:selected').sort().clone();
	    	$('#empSelectedList').append(options);
	    	$("#empSelectedList option").each(function(){
	    		if(!$(this).attr('disabled')) {	    			
		    		$(this).attr("selected","selected");	    		    
	    		}
	    	});
	    	$('#empList option:selected').remove();
	    });
		$('#leftRightBtn').click(function() { 
			var options = $('#empSelectedList option:selected').sort().clone();
	    	$('#empList').append(options);
	    	$("#empSelectedList option:selected").each(function(){
	    		deleteEmp.push($(this).val());
	    	});
	    	$('#deleteEmpList').val(deleteEmp);
	    	$('#empSelectedList option:selected').remove();
	    });
	}); 
	
	function addReportToEmployee(reportId){
		var dept2 = $('#empListValue').val();
		var deleteReportEmp = $('#deleteEmpList').val();
		var selectedEmp = $('#empSelectedList').val();
		if(dept2 == '') {
			jAlert('Please select department.');
		} else if(selectedEmp == '' || selectedEmp == null) {
			jAlert('Please select at least one employee to share report.');
		}else{
			$('#addReportEmpForm').attr('action', 'qb_addReportToEmpAction.action?reportId='+reportId+'&dept2='+dept2+'&selectedEmp='+selectedEmp+'&deleteReportEmp='+deleteReportEmp).submit();
		}
	}
	
</script>
</head>
<body>
	<s:form action="#" id="addReportEmpForm" theme="simple">
		<s:hidden name="deleteEmpList" id="deleteEmpList"></s:hidden>
		<div class="list-details-div w100" style="width:700px!important;height:420px!important">
			<div class="form-table w100">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><label class="notRequired"> <s:text name="personel.employee'sDetailsReport.department"/></label> </td>
						<td class="textfld">
							<s:select name="dept2" id="empListValue"   list="#attr['deptList']" class="chzn-select-deselect chzn-done"
                     					headerKey="" headerValue="Select" accesskey="--Select--" onchange="showEmployeeList()">
                    		</s:select>
						</td>
					</tr>
				</table>
				 <div style="height:10px"></div>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td></td>
				<td style="text-align: left;">Employees</td>
				<td></td>
				<td style="text-align: left;">Selected Employee(s)</td>
				</tr>
					<tr>
						<td></td>	
						<td style="text-align: left;width:300px">
							<select name="empList" id="empList" style="height: 260px !important;" size="10" multiple="multiple"></select>
						</td>
						<td class="selectarrows">
						      <a href='#' id="moveRightBtn"><img id="moveRightBtn_Img" src="images_hvm/selectone.png" /></a>
							  <a href='#' id="leftRightBtn"><img id="leftRightBtn_Img" src="images_hvm/desectone.png" /></a>
						</td>
						<td style="text-align: left;width:300px">
							<select name="empSelectedList" id="empSelectedList" style="height: 260px !important;" size="10" multiple="multiple"></select>
						</td>
					</tr>
				</table>
				<br clear="all">
				<br clear="all">
			</div>
		</div>
		<div class="buttons" style="margin-top: -47px !important">
			<input type="button" value="Share Report" class="update_btn" id="shareReportBtn" onclick="addReportToEmployee('<s:property value="#request.reportId"/>',this.id);" />
			<input type="button" value="Unshare Report" class="update_btn" id="unShareReportBtn" onclick="unshareReportToEmployee('<s:property value="#request.reportId"/>',this.id);" />
			<input type="reset" value="Close" class="reset_btn" onclick="closepopup();" />
		</div>
	</s:form>
</body>
</html>