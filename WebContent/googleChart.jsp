<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Google Chart</title>
<script src = "https://www.gstatic.com/charts/loader.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>
 <script type = "text/javascript">
         google.charts.load('current', {packages: ['corechart']});     
      </script>
</head>
 <body>
      <div id = "container" style = "width: 550px; height: 400px; margin: 0 auto">
      </div>
      <script language = "JavaScript">
         function drawChart() {
            // Define the chart to be drawn.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Browser');
            data.addColumn('number', 'Percentage');
            data.addRows([
               ['Firefox', 45.0],
               ['IE', 26.8],
               ['Chrome', 12.8],
               ['Safari', 8.5],
               ['Opera', 6.2],
               ['Others', 0.7]
            ]);
               
            // Set chart options
            var options = {'title':'Browser market shares at a specific website, 2014', 'width':800, 'height':800};

            // Instantiate and draw the chart.
            var columnChart = new google.visualization.ColumnChart(document.getElementById ('container'));
            columnChart .draw(data, options);
  
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>
   </body></html>