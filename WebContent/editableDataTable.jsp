<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DataTable</title>
<link href="media/dataTables/demo_page.css" rel="stylesheet" type="text/css" />
        <link href="media/dataTables/demo_table.css" rel="stylesheet" type="text/css" />
        <link href="media/dataTables/demo_table_jui.css" rel="stylesheet" type="text/css" />
        <link href="media/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
        <link href="media/themes/smoothness/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" media="all" />
        <script src="scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="scripts/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="scripts/jquery.dataTables.editable.js" type="text/javascript"></script>
        <script src="scripts/jquery.jeditable.js" type="text/javascript"></script>
        <script src="scripts/jquery.validate.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui.js" type="text/javascript"></script><script type="text/javascript">
$(function(){
	$('#companies').dataTable({
		"sAjaxSource":'dataEditable_getEmployeeDataEditable.action',
		"bProcessing":true,
		"bServerSide": true,
		"sPaginationType": "full_numbers",
		"bJQueryUI":true,
		"aoColumns": [
            {  "sName": "Id",
               "bSearchable": false,
               "bSortable": false,
               "bVisible": false
                   },
    { "sName": "name" },
    { "sName": "country" }
   ]
	}).makeEditable({
	    "aoColumns": [
            {
                cssclass: "required"
            },
            {
          	  cssclass: "required"
            },//null for read-only columns
            {
          	    cssclass: "required"
            }
        ]
        });
});
</script>
</head>
<body>
    <!--  -->
    <button id="btnAddNewRow" value="Ok">Add new company...</button> 
    		<button id="btnDeleteRow" value="cancel">Delete selected company</button>
		        <table id="companies" class="display">
		            <thead>
		                <tr>
		                	<th>ID</th>
		                    <th>Company name</th>
		                    <th>Address</th>
		                </tr>
		            </thead>
		            <tbody>
		          
		            </tbody>
		        </table>
    <!--  -->
</body>
</html>