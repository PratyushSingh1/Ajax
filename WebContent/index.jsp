<%@ taglib prefix = "s" uri = "/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
   <head>
      <title>Employee Form</title>
      <script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
   </head>
    <script type="text/javascript">
    
    class Person{
    	
    	constructor(name,age,location) {
    	    this.name = name;
    	    this.age = age;
    	    this.location=location;
    	  }
    	
    	toJSON() {
    	    return {
    	    	name: this.name,
    	    	age:  this.age,
    	    	location:   this.location
    	    };
    	  }
    }
    
      function sendDataInJsonFormat(){
    	  var name=$("#name").val();
    	  var age=$("#age").val();
    	  var location=$("#location").val();
    	  var personArray=[];
    	  var person=new Person(name,age,location);
    	  var person1=new Person("Ramesh",23,"Bangalore");
    	  var person2=new Person("Suresh",26,"Chennai");
    	  personArray.push(person);
    	  personArray.push(person1);
    	  personArray.push(person2);
    	  var jsonPerson=JSON.stringify(personArray);
      }
      </script>
   <body>
         Name:<input type="text" class="testClass" name = "name" id = "name" size = "20" /><br/>
         <p></p>
         Age:<input type="text" name = "age" id = "age" size = "20" /><br/>
         <p></p>
         Location:<input type="text" name = "location" id = "location" size = "20" />
         <p></p>
         <button type="button" id="submitButton" onclick="sendDataInJsonFormat();">
         Click Here
         </button>
   </body>
</html>