<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DataTable</title>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="js/dataTable.js"></script>
<link type="text/css" href="css/dataTable.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script> 
<script type="text/javascript">
$(document).ready(function(){
    $('#example').DataTable({
    "sAjaxSource":'data_getEmployeeData.action',
    "sAjaxDataProp" : '',
    "aoColumns": [
        { "mData": "employeeId" },
        { "mData": "firstName" },
        { "mData": "lastName" },
        { "mData": "workEmailId" },
        { "mData": "date" },
        { "mData": "actions" }
    ]
    });
});

function getDataTable(empId){
	
	console.log(empId);
	$("#fullDataOfEmployee").DataTable({
		
		"sAjaxSource": 'data_getFullEmployeeData.action?empId='+empId,
		"sAjaxDataProp" : '',
		"aoColumns": [
            { "mData": "gender" },
            { "mData": "dateOfBirth" },
            { "mData": "employeeMentType" }
          ],
	});
}
</script>
</head>
<body>
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>EmpId</th>
                <th>FirstName</th>
                <th>LastName</th>
                <th>WorkEmail</th>
            	<th>DateOfHire</th>
            	<th>Actions</th>
                </tr>
        </thead>
    </table>
    <table id="fullDataOfEmployee" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Gender</th>
                <th>DateOfBirth</th>
                <th>EmployementType</th>
        </thead>
    </table>
</body>
</html>