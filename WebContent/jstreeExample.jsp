<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>JS Tree Example JSON Data</title>
<link rel='stylesheet prefetch'
	href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css'>
<link rel='stylesheet prefetch'
	href='https://static.jstree.com/3.0.9/assets/dist/themes/default/style.min.css'>
<link rel="stylesheet" href="css/style.css">
	<script
		src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script
		src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.min.js'></script>
<script type="text/javascript">
	var demo;
	function show() {
		$.ajax({
			type : "POST",
			url : "qb_getDataInTreeFormat.action",
			dataType : "json",
			success : function(Data) {
				demo = Data;
			}
		});
	}
	$('#jstree-tree').on(
			'changed.jstree',
			function(e, data) {
				alert(demo);
				var objNode = data.instance.get_node(data.selected);
				$('#jstree-result').html(
						'Selected: <br/><strong>' + objNode.id + '-'
								+ objNode.text + '</strong>');
			}).jstree({
		core : {
			data : demo
		}
	});
</script>
</head>
<body>
	<p>this sample JS tree use JSON data, read the docs <a href="#"onclick="show();">here </a></p>
	<div id="jstree-tree" class="well col-sm-3"></div>
	<div id="jstree-result" class="col-sm-6">this is result</div>
	<script
		src='https://cdnjs.cloudflare.com/ajax/libs/jstree/3.0.9/jstree.min.js'></script>
	 <script src="js/index.js"></script> 
</body>
</html>
