<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Insert title here</title>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jstree/3.3.5/themes/default/style.min.css" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.5/jstree.min.js"></script>
</head>
		<script>
		$(function() {
		$.ajax({
				type:"POST",
				url:"qb_newJsTree",
				dataType:"json",
				success:function(data)
				{
						console.log(data);			
						$('#simpleJsTreeUsingJson').jstree({
							'core':{
								'data':data
							}
						});
				}
		});
		});
		function createJsTree(jsondata)
		{
			$('#simpleJsTreeUsingJson').jstree({
				'core':{
					'data':jsondata
				}
			});
		}
		</script>
<body>
<div id="simpleJsTreeUsingJson">

</div>
</body>
</html>