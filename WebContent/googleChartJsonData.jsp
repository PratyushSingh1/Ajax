<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Google Chart</title>
<script src = "https://www.gstatic.com/charts/loader.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script type = "text/javascript">
         google.charts.load('current', {packages: ['corechart','table']});   
      </script>
</head>
 <body>
 <div class="col-sm-12">
      <div id = "container">
      </div>
  </div>
  <div class="col-sm-12">
      <div id="table_div"></div>
  </div> 
      <script language = "JavaScript">
         function drawChartAndDataTable() {
        	 
        	 
        	var barChartData="{Engineering:8}~,{Engineering - Design:25}~,{Engineering - Manufacturing:23}~,{Engineering - QA:21}~,{Engineering - R & D:1}~,{Engineering - Testing:22}~,{Finance:4}~,{Human Resources:18}~,{Management:1}~,{Marketing:1}1";
        	 
        	 var lineChartData="Female[{Engineering:3}~,{Engineering - Design:11}~,{Engineering - Manufacturing:8}~,{Engineering - QA:8}~,{Engineering - R & D:1}~,{Engineering - Testing:8}~,{Finance:null}~,{Human Resources:8}~,{Management:1}~,{Operations:3}~,{Sales:2}],Male[{Engineering:2}~,{Engineering - Design:13}~,{Engineering - Manufacturing:13}~,{Engineering - QA:11}~,{Engineering - R & D:null}~,{Engineering - Testing:10}~,{Finance:2}~,{Human Resources:4}~,{Management:null}~,{Operations:1}~,{Sales:1}],Unspecified[{Engineering:2}~,{Engineering - Design:null}~,{Engineering - Manufacturing:null}~,{Engineering - QA:null}~,{Engineering - R & D:null}~,{Engineering - Testing:null}~,{Finance:null}~,{Human Resources:null}~,{Management:null}~,{Operations:null}~,{Sales:null}]2";
        	 
        	 var barChartData="{Anotations:4}~,{ATS Training:1}~,{Benefits Module Training:2}~,{Benefits Training:14}~,{Business Etiquettes:2}~,{Communication Skills:2}~,{Communication Skills-Part 2:3}~,{Coolections Framework:2}~,{Corporate Etiquettes:2}~,{Customer Service Training:7}~,{Diversity/Sensitivity Training:1}~,{Employee Safety Training:1}~,{Finance Training Program:1}~,{General Orientation Course:5}~,{Handling customer complaints:1}~,{IBM Certification Training:6}~,{Induction for all Employees:10}~,{Interpersonal Skill training:2}~,{ISO Training:1}~,{java training:1}~,{Javascript Training class:null}~,{Leadership Training:3}~,{Leadership Training and Management:5}1";
             
        	 var areaChartData="{Anotations:4}~,{ATS Training:1}~,{Benefits Module Training:2}~,{Benefits Training:14}~,{Business Etiquettes:2}~,{Communication Skills:2}~,{Communication Skills-Part 2:3}~,{Coolections Framework:2}~,{Corporate Etiquettes:2}~,{Customer Service Training:7}~,{Diversity/Sensitivity Training:1}~,{Employee Safety Training:1}~,{Finance Training Program:1}~,{General Orientation Course:5}~,{Handling customer complaints:1}~,{IBM Certification Training:6}~,{Induction for all Employees:10}~,{Interpersonal Skill training:2}~,{ISO Training:1}~,{java training:1}~,{Javascript Training class:null}~,{Leadership Training:3}~,{Leadership Training and Management:5}1";
        	 
        	 var horizontalBarChart="{Anotations:4}~,{ATS Training:1}~,{Benefits Module Training:2}~,{Benefits Training:14}~,{Business Etiquettes:2}~,{Communication Skills:2}~,{Communication Skills-Part 2:3}~,{Coolections Framework:2}~,{Corporate Etiquettes:2}~,{Customer Service Training:7}~,{Diversity/Sensitivity Training:1}~,{Employee Safety Training:1}~,{Finance Training Program:1}~,{General Orientation Course:5}~,{Handling customer complaints:1}~,{IBM Certification Training:6}~,{Induction for all Employees:10}~,{Interpersonal Skill training:2}~,{ISO Training:1}~,{java training:1}~,{Javascript Training class:null}~,{Leadership Training:3}~,{Leadership Training and Management:5}1";
        	 
        	 
        	 
        	 var jsonData = {
               	  "cols": [
               	        {"id":"","label":"Topping","pattern":"","type":"string"},
               	        {"id":"","label":"Slices","pattern":"","type":"number"}
               	      ],
               	  "rows": [
               	        {"c":[{"v":"Mushrooms","f":null},{"v":3,"f":null}]},
               	        {"c":[{"v":"Onions","f":null},{"v":1,"f":null}]},
               	        {"c":[{"v":"Olives","f":null},{"v":1,"f":null}]},
               	        {"c":[{"v":"Zucchini","f":null},{"v":1,"f":null}]},
               	        {"c":[{"v":"Pepperoni","f":null},{"v":2,"f":null}]}
               	      ]
               	} ;           
            var data = new google.visualization.DataTable(jsonData);
            var options = {'title':'Browser market shares at a specific website, 2014', 'width':400, 'height':400};
            var columnChart = new google.visualization.ColumnChart(document.getElementById ('container'));
            var table = new google.visualization.Table(document.getElementById('table_div'));
            columnChart .draw(data, options);
            table.draw(data,options );
         }
         google.charts.setOnLoadCallback(drawChartAndDataTable);
      </script>
   </body></html>