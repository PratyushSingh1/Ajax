<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DataTable</title>
<link href="media/dataTables/demo_page.css" rel="stylesheet" type="text/css" />
        <link href="media/dataTables/demo_table.css" rel="stylesheet" type="text/css" />
        <link href="media/dataTables/demo_table_jui.css" rel="stylesheet" type="text/css" />
        <link href="media/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
        <link href="media/themes/smoothness/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" media="all" />
        <script src="scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
        <script src="scripts/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="scripts/jquery.dataTables.editable.js" type="text/javascript"></script>
        <script src="scripts/jquery.jeditable.js" type="text/javascript"></script>
        <script src="scripts/jquery.validate.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui.js" type="text/javascript"></script><script type="text/javascript">
$(function(){
	var object=$('#employee').dataTable({
		"bProcessing":true,
		"bServerSide": true,
		"sPaginationType": "full_numbers",
		"sAjaxSource":'dataEditable_getEmployeeDynamicData.action',
		"bJQueryUI":true,
		"aoColumns": [
            {  "sName": "emp_id"},<!-- the name here are passed as column name while updating the record-->
    		{ "sName": "first_name" },
    		{ "sName": "last_name" },
    		{ "sName": "work_email_id" },
    		{ "sName": "date" }
   ]
	});
	
	
	object.makeEditable({
		sUpdateURL: 'UpdateData.action',
		sAddHttpMethod: "POST",
	    "aoColumns": [//null for read-only columns
            {
            	indicator: 'Saving',
             	 submit:'Save changes'
            },
            {
            	indicator: 'Saving',
             	 submit:'Save changes'
            },
            {
            	indicator: 'Saving',
             	 submit:'Save changes'
            },
            {
            	indicator: 'Saving',
             	 submit:'Save changes'
            }
        ]
        });
});
</script>
</head>
<body>
    <!--  -->
    <button id="btnAddNewRow" value="Ok">Add new company...</button> 
    		<button id="btnDeleteRow" value="cancel">Delete selected company</button>
		        <table id="employee" class="display dataTable">
		            <thead>
		                <tr>
		                	   <th>EMPLOYEEID</th>
		                       <th>FIRSTNAME</th>
		                       <th>LASTNAME</th>
		                       <th>WORKEMAILID</th>
		                       <th>DATE</th>
		                </tr>
		            </thead>
		            <tbody>
		          
		            </tbody>
		        </table>
    <!--  -->
</body>
</html>