<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>JstreeExample</title>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.5/themes/default/style.min.css" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.5/jstree.min.js"></script>
		<script>
		$(function()   {
		$('#3').click(function(){alert(this.id)});		
			$.getJSON(
					"js_getJsTreeData.action",
					function(data){
						createJsTree(data);
					});/*end of getJSON Method*/
		});
		/*here the jsTree Is Created based on dynamic data*/
		function createJsTree(data)
		{
			$('#simpleJsTreeUsingJson').jstree({
				'core':{
					'data':data
				}
			});
			$('#simpleJsTreeUsingJson').on("select_node.jstree", function (e,data) { 
				$("#bodyDivToAppend").html(" ");
				if(data.node.id=="1")
					{
				$("#bodyDivToAppend").html("CLICKED 1st NODE")
					}
				});/*end of  onClickFunction of jsTree*/
		}
		</script>
</head>
<body>
<div id="simpleJsTreeUsingJson">

</div>
<div id="bodyDivToAppend">

</div>
</body>
</html>