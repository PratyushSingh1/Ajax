<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<meta charset="utf-8">
    <link href="css/nv.d3.css" rel="stylesheet" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.min.js" charset="utf-8"></script>
    <script src="js/nv.d3.js"></script>

    <style>
        text {
            font: 12px sans-serif;
        }
        svg {
            display: block;
        }
        html, body, #chart1, svg {
            margin: 0px;
            padding: 0px;
            height:100%;
            width: 100%;
        }
    </style>
</head>
<body>

<div id="chart1">
    <svg></svg>
</div>


<script>
var chart ;
var chartData;
    historicalBarChart = [
        {
            key: "Cumulative Return",
            values: [
                {
                    "label" : "A" ,
                    "value" : 29.765957771107
                } ,
                {
                    "label" : "B" ,
                    "value" : 0
                } ,
                {
                    "label" : "C" ,
                    "value" : 32.807804682612
                } ,
                {
                    "label" : "D" ,
                    "value" : 196.45946739256
                } ,
                {
                    "label" : "E" ,
                    "value" : 0.19434030906893
                } ,
                {
                    "label" : "F" ,
                    "value" : 98.079782601442
                } ,
                {
                    "label" : "G" ,
                    "value" : 13.925743130903
                } ,
                {
                    "label" : "H" ,
                    "value" : 5.1387322875705
                }
            ]
        }
    ];

    nv.addGraph(function() {
        chart= nv.models.discreteBarChart()
            .x(function(d) { return d.label })
            .y(function(d) { return d.value })
            .staggerLabels(false)
            .showValues(false)
            .duration(1000)
            ;
        d3.select('#chart1 svg')
            .datum(historicalBarChart)
            .call(chart);

       /*  nv.utils.windowResize(chart.update); */
        return chart;
    },function(){
        d3.selectAll(".nv-bar").on('click',
                function(d,i){
       	 var data = [
 	        {
 	            key: "Cumulative Return",
 	            values: [
 	                {
 	                    "label" : "A" ,
 	                    "value" : Math.floor((Math.random() * 100) + 1)
 	                } ,
 	                {
 	                    "label" : "B" ,
 	                    "value" : Math.floor((Math.random() * 100) + 1)
 	                } ,
 	                {
 	                    "label" : "C" ,
 	                    "value" : Math.floor((Math.random() * 100) + 1)
 	                } ,
 	                {
 	                    "label" : "D" ,
 	                    "value" : Math.floor((Math.random() * 100) + 1)
 	                } ,
 	                {
 	                    "label" : "E" ,
 	                    "value" : Math.floor((Math.random() * 100) + 1)
 	                } ,
 	                {
 	                    "label" : "F" ,
 	                    "value" : Math.floor((Math.random() * 100) + 1)
 	                } ,
 	                {
 	                    "label" : "G" ,
 	                    "value" : Math.floor((Math.random() * 100) + 1)
 	                } ,
 	                {
 	                    "label" : "H" ,
 	                    "value" : Math.floor((Math.random() * 100) + 1)
 	                }
 	            ]
 	        }
 	    ];

 	    // Update the SVG with the new data and call chart
 	    chartData.datum(data).transition().duration(500).call(chart);
 	    nv.utils.windowResize(chart.update);

            });
       });
</script>
</body>
</html>