<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DataTable</title>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="js/dataTable.js"></script>
<link type="text/css" href="css/dataTable.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script> 
<script type="text/javascript">
function getDataTable(){
	
	 $("#example").dataTable({
			/* "bProcessing": true,
		//"bServerSide": true,
		"bSortable": false,
		"sScrollY": "100%",
		"sScrollX": "100%",
		"sPaginationType": "full_numbers",
		//Work only for WorkSchedule Table */
		"sAjaxSource": 'data_getStringData.action',
		"sAjaxDataProp" : '',
		"aoColumns": [
		              { "mData": "shiftCode" },
		              { "mData": "shiftName" },
		              { "mData": "shiftStartTime" },
		              { "mData": "shiftEndTime" },
		              { "mData": "shiftTotalTime" },
		              { "mData": "shiftWorkHours" },
		              { "mData": "shiftTotalBreakHours" },
		              { "mData": "shiftActions" }
		            ],
	});

}
</script>
</head>
<body>
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
            <th>shiftCode</th>
                <th>shiftName</th>
                <th>shiftStartTime</th>
                <th>shiftEndTime</th>
                <th>shiftTotalTime</th>
                <th>shiftWorkHours</th>
                <th>shiftTotalBreakHours</th>
                <th>shiftActions</th>
                </tr>
        </thead>
    </table>
    <a onclick="getDataTable();">sdsdasd</a>
</body>
</html>