<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix = "s" uri = "/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script>
  $(window).on('load', function(){
	  $("table").each(function() {
	        var $this = $(this);
	        var newrows = [];
	        $this.find("tr").each(function(){
	            var i = 0;
	            $(this).find("td").each(function(){
	                i++;
	                if(newrows[i] === undefined) { newrows[i] = $("<tr></tr>"); }
	                newrows[i].append($(this));
	            });
	        });
	        $this.find("tr").remove();
	        $.each(newrows, function(){
	            $this.append(this);
	        });
	    });
  });
</script>
</head>
<body>
<table>
<thead>
<h2>Head</h2>
</thead>
<tbody>
	 <s:iterator var="list" value="%{listOfListOfString}">
	 <tr>
	        <s:iterator var="list" value="#list" status="stat">
	             <td> <s:property value="#list"/><td/>
	        </s:iterator>
	    </tr>    
	   </s:iterator>
	</tbody>
<tfoot>
<h2>Foot</h2>
</tfoot>
</table>
</body>
</html>