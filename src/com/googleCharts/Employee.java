package com.googleCharts;

public class Employee {

	private String name;
	private Integer salary;
	private Integer age;
	
	public Employee()
	{
		super();
	}
	public Employee(String name,Integer salary,Integer age)
	{
		this.name=name;
		this.salary=salary;
		this.age=age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getSalary() {
		return salary;
	}
	public void setSalary(Integer salary) {
		this.salary = salary;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public static Employee getEmployee()
	{
		return new Employee();
	}
	@Override
	public String toString() {
		return "Employee [name=" + name + ", salary=" + salary + ", age=" + age + "]";
	}
}
