package com.dataTable;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DataTableAction {

	DataTableHelper helper=new DataTableHelper();
	public String getEmployeeData()
{
	HttpServletResponse response=ServletActionContext.getResponse();
	JSONArray jsonArray = new JSONArray();
	String actions;
	PrintWriter writer;
	try {
		writer = response.getWriter();
		response.setContentType("application/json");
		DataTableHelper dataTableHelper=new DataTableHelper();
		List<Employee> empList=dataTableHelper.getEmployeeList();
		Iterator<Employee> itr=empList.iterator();
		while(itr.hasNext())
		{
			Employee emp=itr.next();
			JSONObject jsonObject = getJsonObjectsToPutInArray(emp);
			jsonArray.put(jsonObject);
		}
		writer.print(jsonArray);
	} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}
	private JSONObject getJsonObjectsToPutInArray(Employee emp) throws JSONException {
		String actions;
		JSONObject jsonObject=new JSONObject();
		jsonObject.put("employeeId", emp.getEmployeeId());
		jsonObject.put("firstName", emp.getFirstName());
		jsonObject.put("lastName", emp.getLastName());
		jsonObject.put("workEmailId", emp.getWorkEmailId());
		jsonObject.put("date",  emp.getDate());
		actions="<a onclick='getDataTable("+ emp.getEmployeeId()+");'>clickHere</a>";
		jsonObject.put("actions", actions);
		return jsonObject;
	}
public String getStringData()
{
	HttpServletResponse response=ServletActionContext.getResponse();
	HttpServletRequest request=ServletActionContext.getRequest();
	PrintWriter out = null;
	JSONArray jsonArray = new JSONArray();
		try{
			out = response.getWriter();
			JSONObject json = getJsonObject2();
			jsonArray.put(json);
			response.setContentType("application/json");
			response.setHeader("Cache-Control", "no-store");
			out.print(jsonArray);
	} catch(Exception e) {
		e.printStackTrace();
	} finally {
		out.flush();
		out.close();
	}
	return null;
}
private JSONObject getJsonObject2() throws JSONException {
	JSONObject json = new JSONObject();
	json.put("shiftCode", "123");
	json.put("shiftName","456");
	json.put("shiftStartTime", "00:00");
	json.put("shiftEndTime", "2345");
	json.put("shiftTotalTime", "324");
	json.put("shiftWorkHours", "sdfg");
	json.put("shiftTotalBreakHours", "dasfgb");
	json.put("shiftActions", "asdfg");
	return json;
}
public String getFullEmployeeData() throws IOException
{
	HttpServletResponse response=ServletActionContext.getResponse();
	HttpServletRequest request=ServletActionContext.getRequest();
	PrintWriter out=response.getWriter();
	JSONObject jsonObject=new JSONObject();
	JSONArray jsonArray=new JSONArray();
	String empId=request.getParameter("empId");
	Object[] returnedObject=helper.getFullEmployeeDetails(Integer.parseInt(empId));
	try {
		jsonObject.put("gender",returnedObject[0]);
		jsonObject.put("dateOfBirth",returnedObject[1]);
		jsonObject.put("employeeMentType",returnedObject[2]);
		jsonArray.put(jsonObject);
		out.print(jsonArray);
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return null;
}

}
