package com.dataTable;

public class Person {

	private String id;
	private String name;
	private String place;
	private String city;
	private String state;
	private String phone;
	public Person() {
		super();
	}
	public Person(String id, String name, String place, String city, String state,String phone) {
		super();
		this.id = id;
		this.name = name;
		this.place = place;
		this.city = city;
		this.state = state;
		this.setPhone(phone);
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", place=" + place + ", city=" + city + ", state=" + state
				+ ", phone=" + phone + "]";
	}
}
