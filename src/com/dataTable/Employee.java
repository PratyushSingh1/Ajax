package com.dataTable;

import java.util.Comparator;
import java.util.Date;

public class Employee{

	private Integer employeeId;
	private String firstName;
	private String lastName;
	private String workEmailId;
	private Date date;
	public Employee()
	{
		super();
	}
	public Employee(Integer employeeId, String firstName, String lastName, String workEmailId, Date date) {
		super();
		this.employeeId = employeeId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.workEmailId = workEmailId;
		this.date = date;
	}
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getWorkEmailId() {
		return workEmailId;
	}
	public void setWorkEmailId(String workEmailId) {
		this.workEmailId = workEmailId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", workEmailId=" + workEmailId + ", date=" + date + "]";
	}
	/**
	 * public static Employee comperator class to Compare two Employee object bases on Firstname
	 *  
	 */
	public static class FirstNameComparator implements Comparator<Employee>
	{
		@Override
		public int compare(Employee o1, Employee o2) {
			return o1.getFirstName().compareTo(o2.getFirstName());
		}
	}
	/**
	 * public static Employee comperator class to Compare two Employee object bases on Lastname
	 */
	public static class LastNameComparator implements Comparator<Employee>
	{
		@Override
		public int compare(Employee o1, Employee o2) {
			return o1.getLastName().compareTo(o2.getLastName());
		}
	}
	/**
	 * public static Employee comperator class to Compare two Employee object bases on name
	 */
	public static class DateComparator implements Comparator<Employee>
	{
		@Override
		public int compare(Employee o1, Employee o2) {
			return o1.getDate().compareTo(o2.getDate());
		}
	}
}
