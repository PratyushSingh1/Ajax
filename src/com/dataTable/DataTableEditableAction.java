package com.dataTable;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.JSONUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONML;
import org.json.JSONObject;

public class DataTableEditableAction {

	DataTableHelper helper=new DataTableHelper();
	public String getEmployeeData()
{
	HttpServletResponse response=ServletActionContext.getResponse();
	HttpServletRequest request=ServletActionContext.getRequest();
	JSONArray jsonArray = new JSONArray();
	String actions;
	PrintWriter writer;
	try {
		writer = response.getWriter();
		response.setContentType("application/json");
		DataTableHelper dataTableHelper=new DataTableHelper();
		List<Employee> empList=dataTableHelper.getEmployeeList();
		Iterator<Employee> itr=empList.iterator();
		while(itr.hasNext())
		{
			Employee emp=itr.next();
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("employeeId", emp.getEmployeeId());
			jsonObject.put("firstName", emp.getFirstName());
			jsonObject.put("lastName", emp.getLastName());
			jsonObject.put("workEmailId", emp.getWorkEmailId());
			jsonObject.put("date",  emp.getDate());
			jsonArray.put(jsonObject);
		}
		writer.print(jsonArray);
	} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}
public String getStringData()
{
	HttpServletResponse response=ServletActionContext.getResponse();
	HttpServletRequest request=ServletActionContext.getRequest();
	PrintWriter out = null;
	JSONArray jsonArray = new JSONArray();
		try{
			out = response.getWriter();
			JSONObject json = new JSONObject();
			json.put("shiftCode", "123");
			json.put("shiftName","456");
			json.put("shiftStartTime", "00:00");
			json.put("shiftEndTime", "2345");
			json.put("shiftTotalTime", "324");
			json.put("shiftWorkHours", "sdfg");
			json.put("shiftTotalBreakHours", "dasfgb");
			json.put("shiftActions", "asdfg");
			jsonArray.put(json);
			response.setContentType("application/json");
			response.setHeader("Cache-Control", "no-store");
			out.print(jsonArray);
	} catch(Exception e) {
		e.printStackTrace();
	} finally {
		out.flush();
		out.close();
	}
	return null;
}
public String getFullEmployeeData() throws IOException
{
	HttpServletResponse response=ServletActionContext.getResponse();
	HttpServletRequest request=ServletActionContext.getRequest();
	PrintWriter out=response.getWriter();
	JSONObject jsonObject=new JSONObject();
	JSONArray jsonArray=new JSONArray();
	String empId=request.getParameter("empId");
	Object[] returnedObject=helper.getFullEmployeeDetails(Integer.parseInt(empId));
	try {
		jsonObject.put("gender",returnedObject[0]);
		jsonObject.put("dateOfBirth",returnedObject[1]);
		jsonObject.put("employeeMentType",returnedObject[2]);
		jsonArray.put(jsonObject);
		out.print(jsonArray);
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return null;
}
public String getEmployeeDataEditable() throws Exception
{
	HttpServletResponse response=ServletActionContext.getResponse();
	HttpServletRequest request=ServletActionContext.getRequest();
	final DataTableRequestParam param = DataTablesParamUtility.getParam(request);
	
	String sEcho = param.sEcho;
	int iTotalRecords; // total number of records (unfiltered)
	int iTotalDisplayRecords;//value will be set when code filters companies by keyword
	JSONArray data = new JSONArray(); //data that will be shown in the table

	iTotalRecords = UtilForEditableDatatable.getCompany().size();
	List<Company> companies = new LinkedList<Company>();
	/**
	 * first fetch the list of companies either all or the matching companies to the text 
	 * entered in search box in list format then sort those using comparator.
	 * 
	 * Here it is being fetched based on entered data in text box
	 */
	for(Company c :UtilForEditableDatatable.getCompany()){
		//Cannot search by column 0 (id)
		if(	param.bSearchable[1] &&
			c.getName().toLowerCase().contains(param.sSearchKeyword.toLowerCase())
			||
			param.bSearchable[2] &&
			c.getCountry().toLowerCase().contains(param.sSearchKeyword.toLowerCase()))
		{
			companies.add(c); // Add a company that matches search criterion
		}
	}
	iTotalDisplayRecords = companies.size();//Number of companies that matches search criterion should be returned
	/**
	 * here it is being sorted and send to front end.
	 */
	
	Collections.sort(companies, new Comparator<Company>(){
		@Override
		public int compare(Company c1, Company c2) {
			int result = 0;
			for(int i=0; i<param.iSortingCols; i++){
				int sortBy = param.iSortCol[i];
				if(param.bSortable[sortBy]){
					switch(sortBy){
						case 0:
							result =	0; //sort by id is not allowed
						break;
						case 1:
							result =	c1.getName().compareToIgnoreCase(c2.getName()) * (param.sSortDir[i].equals("asc") ? -1 : 1);
							break;
						case 2:
							result =	c1.getCountry().compareToIgnoreCase(c2.getCountry()) * 
										(param.sSortDir[i].equals("asc") ? -1 : 1);
							break;
					}
				}
				if(result!=0)
					return result;
				else
					continue;
			}
			return result;
		}
	});
	if(companies.size()< param.iDisplayStart + param.iDisplayLength)
		companies = companies.subList(param.iDisplayStart, companies.size());
	else
		companies = companies.subList(param.iDisplayStart, param.iDisplayStart + param.iDisplayLength);
	try {
		JSONObject jsonResponse = new JSONObject();
		
		jsonResponse.put("sEcho", sEcho);
		jsonResponse.put("iTotalRecords", iTotalRecords);
		jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
		
		for(Company c : companies){
			JSONArray row = new JSONArray();
			row.put(c.getId()).put(c.getName()).put(c.getCountry());
			data.put(row);
		}
		jsonResponse.put("aaData", data);
		
		response.setContentType("application/json");
		response.getWriter().print(jsonResponse.toString());
	} catch (JSONException e) {
		e.printStackTrace();
		response.setContentType("text/html");
		response.getWriter().print(e.getMessage());
	}
	return null;
}
public String updateData()
{
	HttpServletRequest request=ServletActionContext.getRequest();
	HttpServletResponse response=ServletActionContext.getResponse();
try{
	int id = Integer.parseInt(request.getParameter("id"));
	int columnId = Integer.parseInt(request.getParameter("columnId"));
	int columnPosition = Integer.parseInt(request.getParameter("columnPosition"));
	int rowId = Integer.parseInt(request.getParameter("rowId"));
	String value = request.getParameter("value");
	String columnName = request.getParameter("columnName");
}catch(Exception exception)
{
	exception.printStackTrace();
}
	return null;
}
public String getPersonData()
{
	HttpServletRequest request=ServletActionContext.getRequest();
	HttpServletResponse response=ServletActionContext.getResponse();
	LinkedList<Person> personsLinkedList=new LinkedList<>();
	DataTableRequestParam requestParam=DataTablesParamUtility.getParam(request);
	String sEcho=requestParam.sEcho;
	int iTotalRecords;
	int iTotalDisplayRecords;
	JSONArray personsJsonArray=new JSONArray();
try{
	List<Person> personList=UtilForEditableDatatable.getPersonsData();
	personsLinkedList=(LinkedList<Person>) personList;
	iTotalRecords=personsLinkedList.size();
	
}catch(Exception e)
{
	e.printStackTrace();
}
return null;
}
/**
 * 
 * @return employee data from apps2 database
 */
public String getEmployeeDynamicData()
{
	HttpServletRequest request=ServletActionContext.getRequest();
	HttpServletResponse response=ServletActionContext.getResponse();
	DataTableRequestParam requestParam=DataTablesParamUtility.getParam(request);
	String sEcho=requestParam.sEcho;
	int iTotalRecords;
	int iTotalDisplayRecords;
	JSONArray employeesJsonArray=new JSONArray();
try{
	iTotalRecords = UtilForEditableDatatable.getEmployeeDetails().size();
	List<Employee> empSortedAndSearchedList= new LinkedList<Employee>();
for(Employee e:UtilForEditableDatatable.getEmployeeDetails())
{
	//empty string occurs between every string  and at start and end of string so it returns true 
	//even though sSearch keyword is empty
			if(	requestParam.bSearchable[1] &&
				e.getFirstName().toLowerCase().contains(requestParam.sSearchKeyword.toLowerCase())
				||
				requestParam.bSearchable[2] &&
				e.getLastName().toLowerCase().contains(requestParam.sSearchKeyword.toLowerCase())
				||
				requestParam.bSearchable[3] &&
				e.getWorkEmailId().toLowerCase().contains(requestParam.sSearchKeyword.toLowerCase()))
			{
				empSortedAndSearchedList.add(e);
			}
}
			iTotalDisplayRecords=empSortedAndSearchedList.size();
			if(empSortedAndSearchedList.size()< requestParam.iDisplayStart + requestParam.iDisplayLength)
				empSortedAndSearchedList = empSortedAndSearchedList.subList(requestParam.iDisplayStart, empSortedAndSearchedList.size());
			else
				empSortedAndSearchedList = empSortedAndSearchedList.subList(requestParam.iDisplayStart, requestParam.iDisplayStart + requestParam.iDisplayLength);
				JSONObject jsonResponse = new JSONObject();
				
				jsonResponse.put("sEcho", sEcho);
				jsonResponse.put("iTotalRecords", iTotalRecords);
				jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
				for(Employee emp : empSortedAndSearchedList){
					JSONArray row = new JSONArray();
					row.put(emp.getEmployeeId()).put(emp.getFirstName())
							.put(emp.getLastName()).put(emp.getWorkEmailId()).put(emp.getDate());
					employeesJsonArray.put(row);
				}
					jsonResponse.put("aaData",employeesJsonArray);
					response.setContentType("application/json");
					response.getWriter().print(jsonResponse.toString());
}catch(Exception e)
{
	e.printStackTrace();
}
return null;
}
/**
 * updateDataForEmployee to update data of employee from apps2 database
 * @throws Exception 
 */
public String updateDataForEmployee() throws Exception
{
	HttpServletRequest request=ServletActionContext.getRequest();
	HttpServletResponse response=ServletActionContext.getResponse();
	int id = Integer.parseInt(request.getParameter("id"));
	int columnId = Integer.parseInt(request.getParameter("columnId"));
	int columnPosition = Integer.parseInt(request.getParameter("columnPosition"));
	int rowId = Integer.parseInt(request.getParameter("rowId"));
	String value = request.getParameter("value");
	String columnName=request.getParameter("columnName");
	UtilForEditableDatatable.update(value,id,columnName);
	response.getWriter().println("DONE");
	return null;
}
}
