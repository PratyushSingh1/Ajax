package com.dataTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DataTableHelper {
	Connection connection=null;
	
	public List<Employee> getEmployeeList()
	{
		ResultSet employeeResultSet;
		List<Employee> employeeList=new ArrayList<>();
		try {
			Statement statement=getConnection().createStatement();
			String query="select employee.emp_id,employee.first_name,\r\n" + 
					"employee.last_name,employee.work_email_id,employee.date_of_hiring from employee";
			employeeResultSet=statement.executeQuery(query);
			while(employeeResultSet.next())
			{
				Employee employee=new Employee();
				employee.setEmployeeId(employeeResultSet.getInt("emp_id"));
				employee.setFirstName(employeeResultSet.getString("first_name"));
				employee.setLastName(employeeResultSet.getString("last_name"));
				employee.setWorkEmailId(employeeResultSet.getString("work_email_id"));
				employee.setDate(employeeResultSet.getDate("date_of_hiring"));
				employeeList.add(employee);
			}
		} 
		catch(NullPointerException exception)
		{
			System.out.println("#####NullPointerOCCURED#####");
			exception.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return employeeList;
	}
	public Object[] getFullEmployeeDetails(Integer empId)
	{
		Object[] mixObjectArrayOfEmployee=new Object[3];
		try {
			Statement statement=getConnection().createStatement();
			String query="select employee.gender,employee.date_birth,employee.employment_type "
					+ " from employee where employee.emp_id="+empId;
			ResultSet rs=statement.executeQuery(query);
			while(rs.next())
			{
				String gender=rs.getString("gender");
				Date dateOfBirth=rs.getDate("date_birth");
				String employmentType=rs.getString("employment_type");
				mixObjectArrayOfEmployee[0]=gender;
				mixObjectArrayOfEmployee[1]=dateOfBirth;
				mixObjectArrayOfEmployee[2]=employmentType;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mixObjectArrayOfEmployee;
	}
	private Connection getConnection()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection=DriverManager.getConnection
					("jdbc:mysql://192.168.0.16:3306/sutihr_apps2_dev","hvm","hvm");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connection;
	}
}
