package com.dataTable;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import java.sql.Connection;

public class UtilForEditableDatatable {

	static{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	private static Connection connection;
	private static List<Company> companylist;
	private static List<Person> personslist;
	private static List<Employee> employeelist;
	public static List<Company> getCompany()
	{
		companylist=new ArrayList<Company>();
		companylist.add(new Company(1,"Sutisoft","India"));
		companylist.add(new Company(2,"Tcs","India"));
		companylist.add(new Company(3,"Infosys","India"));
		companylist.add(new Company(4,"Google","Usa"));
		companylist.add(new Company(5,"Alibaba","China"));
		companylist.add(new Company(6,"Dell","Usa"));
		companylist.add(new Company(7,"Tesla","Canada"));
		return companylist;
	}
	public static List<Person> getPersonsData()
	{
		personslist=new ArrayList<Person>();
		ResultSet rs;
		try{
			String query="select * from person";
			Statement statement=getConnection().createStatement();
			rs=statement.executeQuery(query);
			while(rs.next())
			{
				Person p=new Person();
				p.setId(rs.getString("id"));
				p.setName(rs.getString("name"));
				p.setPlace(rs.getString("place"));
				p.setCity(rs.getString("city"));
				p.setState(rs.getString("state"));
				p.setPhone(rs.getString("phone"));
				personslist.add(p);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return personslist;
	}
	public static List<Employee> getEmployeeDetails()
	{
		employeelist=new ArrayList<Employee>();
		ResultSet rs;
		try{
			String query="select emp_id,first_name,last_name,work_email_id,date_of_hiring from employee order by employee.last_name";
			Statement statement=getConnection().createStatement();
			rs=statement.executeQuery(query);
			while(rs.next())
			{
				Employee employee=new Employee();
				employee.setEmployeeId(rs.getInt("emp_id"));
				employee.setFirstName(rs.getString("first_name"));
				employee.setLastName(rs.getString("last_name"));
				employee.setDate(rs.getDate("date_of_hiring"));
				employee.setWorkEmailId(rs.getString("work_email_id"));
				employeelist.add(employee);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return employeelist;
	}
private static Connection getConnection()
{
	try{
		connection=DriverManager.getConnection("jdbc:mysql://192.168.0.16:3306/sutihr_apps2_dev","hvm","hvm");
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	return connection;
}
public static boolean update(String updatedColumnValue, int primaryKey, String columnName)
{
	Connection conn=null;
	StringBuffer query=new StringBuffer();
	Statement statement=null;
	try {
	query.append("update employee set "+columnName+"='"+updatedColumnValue+"' where emp_id="+primaryKey);
	conn=getConnection();
	statement=conn.createStatement();
	statement.executeUpdate(query.toString());
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	return false;
}
}
