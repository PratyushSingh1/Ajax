package com.dataTable;

public class Company {

	private Integer Id;
	private String name;
	private String country;
	public Company(Integer id, String name, String country) {
		super();
		Id = id;
		this.name = name;
		this.country = country;
	}
	public Company() {
		super();
	}
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	@Override
	public String toString() {
		return "Company [Id=" + Id + ", name=" + name + ", country=" + country + "]";
	}
}
