package com.jsTree;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONObject;
/**
 * 
 * @author pratyushs
 *
 */
public class JstreeAction   {

	HttpServletRequest request=null;
	HttpServletResponse response=null;
	JSONArray jstreeJsonArray=null;
	
	/**
	 * 
	 * @return
	 */
	public String getJsTreeData()
	{
		request=ServletActionContext.getRequest();
		response=ServletActionContext.getResponse();
		jstreeJsonArray=new JSONArray();
		try {
			JSONObject jstreeJsonObject=new JSONObject();
			jstreeJsonObject.put("id","1");
			jstreeJsonObject.put("parent","#");
			jstreeJsonObject.put("text","Simple root node");
			JSONObject jstreeJsonObject1=new JSONObject();
			jstreeJsonObject1.put("id","2");
			jstreeJsonObject1.put("parent","#");
			jstreeJsonObject1.put("text","Root node 2");
			JSONObject jstreeJsonObject2=new JSONObject();
			jstreeJsonObject2.put("id","3");
			jstreeJsonObject2.put("parent","2");
			jstreeJsonObject2.put("text","Child 1");
			JSONObject jstreeJsonObject3=new JSONObject();
			jstreeJsonObject3.put("id","4");
			jstreeJsonObject3.put("parent","2");
			jstreeJsonObject3.put("text","Child 2");
			jstreeJsonArray.put(jstreeJsonObject);
			jstreeJsonArray.put(jstreeJsonObject1);
			jstreeJsonArray.put(jstreeJsonObject2);
			jstreeJsonArray.put(jstreeJsonObject3);
			response.getWriter().print(jstreeJsonArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
