package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class CompanyHelper {
	/*public static JSONObject getListOfEmployees(Integer unitId, Integer deptId, Integer statusId, Integer reportId) {
		LOGGER.info("Enter into DynamicQueryHelper...");
		net.sf.json.JSONArray jsonArray = new net.sf.json.JSONArray();
		net.sf.json.JSONArray jsonArray2 = new net.sf.json.JSONArray();
		net.sf.json.JSONObject jsonObj = new net.sf.json.JSONObject();
		List<Integer> hrTypeIds=new ArrayList<Integer>();
		try {
			Session session = HibernateSessionFactory.getSession();
			Criteria criteria =  session.createCriteria(EmpJobHistory.class)
										.createAlias("employeeByEmpId", "emp")
										.createAlias("emp.unit", "unit")
										.createAlias("emp.statusMain", "status")
										.add(Restrictions.eq("statusMain.statusId", statusId))
										.add(Restrictions.in("status.statusId", new Integer[] {1,9}))
										.add(Restrictions.eq("unit.unitId", unitId));
			
			Criteria reportCriteria = session.createCriteria(QbEmpReport.class)
											.add(Restrictions.eq("status.statusId", statusId))
											.add(Restrictions.eq("report.id", reportId));
			//updated
			UnitDAO unitdao=new UnitDAO();
			Integer companyID=null;
			if(unitId != null) {
				companyID=unitdao.getCompanyId(unitId);
			}		
			Criteria usercriteria=session.createCriteria(UserRegistration.class)
					.add(Restrictions.eq("companyId",companyID)).add(Restrictions.eq("hrFlag", "true"))
					.add(Restrictions.eq("statusMain.statusId", StatusMap.ACTIVE));
			@SuppressWarnings("unchecked")
			List<UserRegistration> userRegistrations= usercriteria.list();
			for(UserRegistration user:userRegistrations)
			{
				hrTypeIds.add(user.getTypeId());
			}
			//
			if(deptId != 0) {
				criteria.createAlias("department", "department")
						.createAlias("department.statusMain", "deptStatus")
						.createAlias("department.unit", "deptUnit")
						.add(Restrictions.eq("department.departmentId", deptId))
						.add(Restrictions.eq("deptStatus.statusId", statusId))
						.add(Restrictions.eq("deptUnit.unitId", unitId));
				
				reportCriteria.createAlias("department", "department")
								.createAlias("department.statusMain", "deptStatus")
								.createAlias("department.unit", "deptUnit")
								.add(Restrictions.eq("department.departmentId", deptId))
								.add(Restrictions.eq("deptStatus.statusId", statusId))
								.add(Restrictions.eq("deptUnit.unitId", unitId));
			} else {
				criteria.createAlias("department", "department")
						.createAlias("department.statusMain", "deptStatus")
						.createAlias("department.unit", "deptUnit")
						.add(Restrictions.eq("deptStatus.statusId", statusId))
						.add(Restrictions.eq("deptUnit.unitId", unitId));
				
				reportCriteria.createAlias("department", "department")
								.createAlias("department.statusMain", "deptStatus")
								.createAlias("department.unit", "deptUnit")
								.add(Restrictions.eq("deptStatus.statusId", statusId))
								.add(Restrictions.eq("deptUnit.unitId", unitId));
			}
			
			Map<Integer, QbEmpReport> selectEmpMap = new HashMap<Integer, QbEmpReport>();
			@SuppressWarnings("unchecked")
			List<QbEmpReport> selectedList = reportCriteria.list();
			for(QbEmpReport emp : selectedList) {
				selectEmpMap.put(emp.getEmployee().getEmpId(), emp);
			}
			criteria.setProjection( Projections.distinct( Projections.projectionList()
					.add(Projections.property("employeeByEmpId"), "employeeByEmpId")));
			@SuppressWarnings("unchecked")
			List<Employee> list = criteria.addOrder(Order.asc("emp.firstName")).list();
			for(Employee e : list) {
				//Employee e = emp.getEmployeeByEmpId();
				QbEmpReport qbEmpReport = selectEmpMap.get(e.getEmpId());
				if(qbEmpReport == null) {					
					JSONObject json = new JSONObject();
					//updated
					if(!hrTypeIds.contains(e.getEmpId()))
					{
						json.put("empId", e.getEmpId());
						json.put("empName", e.getFirstName()+" "+e.getLastName());
						jsonArray.put(json);
					}
					//
				} else {
					if(!hrTypeIds.contains(e.getEmpId()))
					{
						JSONObject json = new JSONObject();
						json.put("empId", e.getEmpId());
						json.put("empName", e.getFirstName()+" "+e.getLastName());
						jsonArray2.put(json);
					}
				}
			}
			jsonObj.put("empJson", jsonArray);
			jsonObj.put("selectedEmp", jsonArray2);
		} catch(Exception e) {
			LOGGER.error(e);
		}
		LOGGER.info("End of DynamicQueryHelper...");
		return jsonObj;
	}
	public static boolean saveReportToEmployee(Integer unitId,Set<Integer> emp, Integer deptId, Integer reportId, Integer userId, Integer statusId, Set<Integer> deleteEmpList) {
		LOGGER.info("Enter into SaveReportToEmployee...");
		boolean flag = false;
		Session session = HibernateSessionFactory.getSession();
		Transaction tr = session.beginTransaction();
		Map<Integer, Integer> empDeptIdMap = new HashMap<Integer,Integer>();
		try {
			tr.begin();
			List<EmpJobHistory> list = session.createCriteria(EmpJobHistory.class)
												.createAlias("employeeByEmpId", "emp")
												.createAlias("emp.statusMain", "statusM")
												.createAlias("emp.unit", "unit")
												.add(Restrictions.eq("unit.unitId", unitId))
												.add(Restrictions.eq("statusMain.statusId", statusId))
												.add(Restrictions.in("statusM.statusId", new Integer[] {1,9}))
												.list();
			for(EmpJobHistory employee : list) {
				empDeptIdMap.put(employee.getEmployeeByEmpId().getEmpId(), employee.getDepartment().getDepartmentId());
			}
			if(!emp.isEmpty()) {				
				for(Integer empId : emp) {				
					deptId = empDeptIdMap.get(empId);
					Criteria qbEmpReportCriteria = session.createCriteria(QbEmpReport.class)
							.createAlias("employee", "emp")
							.createAlias("emp.unit", "unit")
							.createAlias("emp.statusMain", "empStatus")
							.add(Restrictions.eq("emp.empId", empId))
							.add(Restrictions.in("empStatus.statusId",new Integer[] {1,9}))
							.add(Restrictions.eq("unit.unitId", unitId))
							.createAlias("department", "dept")
							.createAlias("dept.unit", "unitD")
							.createAlias("dept.statusMain", "deptStatus")
							.add(Restrictions.eq("dept.departmentId", deptId))
							.add(Restrictions.eq("unitD.unitId", unitId))
							.add(Restrictions.eq("deptStatus.statusId", statusId))
							.add(Restrictions.eq("report.id", reportId))
							.add(Restrictions.eq("status.statusId", statusId));
					List<QbEmpReport> qbReportList = qbEmpReportCriteria.list();
					if(qbReportList.isEmpty()) {					
						Reportstable reportstable = (Reportstable) session.load(Reportstable.class, reportId);
						Employee employee = (Employee) session.load(Employee.class, empId);
						Department department = (Department) session.load(Department.class, deptId);
						UserRegistration user = (UserRegistration) session.load(UserRegistration.class, userId);
						StatusMain status = new StatusMain();
						status.setStatusId(statusId);
						QbEmpReport qbEmpReport = new QbEmpReport();
						qbEmpReport.setDepartment(department);
						qbEmpReport.setEmployee(employee);
						qbEmpReport.setReport(reportstable);
						qbEmpReport.setCreatedBy(user);
						qbEmpReport.setModifiedBy(user);
						qbEmpReport.setStatus(status);
						qbEmpReport.setCreatedDate(new Date());
						qbEmpReport.setModifiedDate(new Date());
						session.save(qbEmpReport);
					}
				}
			}else {
				flag = false;
				return flag;
			}
			if(!deleteEmpList.isEmpty()) {				
				for(Integer empId : deleteEmpList) {
					deptId = empDeptIdMap.get(empId);
					StatusMain inActiveStatus = new StatusMainDAO().findById(StatusMap.INACTIVE);
					UserRegistration userReg = new UserRegistrationDAO().findById(userId);
					Criteria qbEmpReportCriteria = session.createCriteria(QbEmpReport.class)
							.createAlias("employee", "emp")
							.createAlias("emp.unit", "unit")
							.createAlias("emp.statusMain", "empStatus")
							.add(Restrictions.eq("emp.empId", empId))
							.add(Restrictions.in("empStatus.statusId",new Integer[] {1,9}))
							.add(Restrictions.eq("unit.unitId", unitId))
							.createAlias("department", "dept")
							.createAlias("dept.unit", "unitD")
							.createAlias("dept.statusMain", "deptStatus")
							.add(Restrictions.eq("dept.departmentId", deptId))
							.add(Restrictions.eq("unitD.unitId", unitId))
							.add(Restrictions.eq("deptStatus.statusId", statusId))
							.add(Restrictions.eq("report.id", reportId))
							.add(Restrictions.eq("status.statusId", statusId));
					List<QbEmpReport> qbReportList = qbEmpReportCriteria.list();
					for(QbEmpReport qbReport : qbReportList) {
						qbReport.setStatus(inActiveStatus);
						qbReport.setModifiedBy(userReg);
						qbReport.setModifiedDate(new Date());
						session.update(qbReport);
					}
				}
			}
			tr.commit();
			if(tr.wasCommitted()) {
				flag = true;
			}
		} catch(Exception e) {
			e.printStackTrace();
			LOGGER.error(e);
		} finally {
			session.clear();
			session.close();
		}
		LOGGER.info("End of SaveReportToEmployee...");
		return flag;
	}
	*
	*/
	public Map<Integer,String> getDepartmentList()
	{
		Map<Integer,String> departmentMap=new HashMap<>();
		try{
			Statement statement = getStatement();
			ResultSet rs=statement.executeQuery(Queries.GET_DEPARTMENT);
			while(rs.next())
			{
				departmentMap.put(rs.getInt("department_id"),rs.getString("name"));
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return departmentMap;
	}

	public List<String> getEmployeeList(String deptName) throws ClassNotFoundException, SQLException
	{
		List<String> employeeList=new ArrayList<>();
		Statement statement=getStatement();
		ResultSet rs=statement.executeQuery(Queries.GET_EMPLOYEES_LIST+"'"+deptName+"'");
		while(rs.next())
		{
			employeeList.add(rs.getString("NAME"));
		}
		return employeeList;
	}
	private Statement getStatement() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Statement statement=DriverManager.getConnection("jdbc:mysql://192.168.0.7:3306/sutihvm50_dev","hvm","hvm").createStatement();
		return statement;
	}
}
