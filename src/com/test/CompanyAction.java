package com.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import org.json.JSONObject;

public class CompanyAction {
	/*public String getListOfEmployees() {
		LOGGER.info("Method Starts - getListOfEmployees() of DynamicQueryAction.java");
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpServletResponse response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		PrintWriter out = null;
		try {
			out = response.getWriter();
			response.setContentType("application/json");
			Integer unit_id = (Integer) session.getAttribute("loginUnitId");
			Integer deptId = Integer.parseInt(request.getParameter("deptId"));
			Integer reportId = Integer.parseInt(request.getParameter("reportId"));
			JSONObject empListMap = DynamicQueryHelper.getListOfEmployees(unit_id, deptId, StatusMap.ACTIVE, reportId);
			out.print(empListMap);
		} catch (Exception e) {
			LOGGER.error(e);
		} finally {
			out.flush();
			out.close();
		}
		LOGGER.info("Method Ends - getListOfEmployees() of DynamicQueryAction.java");
		return null;
	}
	public String addReportToEmpAction() {
		LOGGER.info("Enter into addReportToEmpAction");
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();
		try {
			Integer unitId = (Integer) session.getAttribute("loginUnitId");
			Integer deptId = Integer.parseInt(request.getParameter("dept2"));
			String[] emp = request.getParameter("selectedEmp").split("\\,");
			String[] deleteAssignedReportEmp = request.getParameter("deleteReportEmp").split("\\,");
			Set<Integer> empList = new HashSet<Integer>();
			Set<Integer> deleteEmpList = new HashSet<Integer>();
			Integer userId = (Integer) session.getAttribute("loginUserId");
			Integer reportId = Integer.parseInt(request.getParameter("reportId"));
			
			for (String s : emp) {
				if(!s.isEmpty()) {
					empList.add(Integer.parseInt(s));
				}
			}
			for (String e : deleteAssignedReportEmp) {
				if (!e.isEmpty()) {
					deleteEmpList.add(Integer.parseInt(e));
				}
			}
			if (userId != null) {
				boolean status = DynamicQueryHelper.saveReportToEmployee(unitId, empList, deptId, reportId, userId, StatusMap.ACTIVE,deleteEmpList);
				if(status) {
					session.setAttribute("addReportToEmp", "Report shared to employees succesfully.");
				}
				else {
					session.setAttribute("addReportToEmpErr", "Failed to share report.");
				}
			} else {
				session.setAttribute("addReportToEmpErr", "Failed to share report.");
			}
		} catch (Exception e) {
			LOGGER.error(e);
			session.setAttribute("addReportToEmpErr", "Failed to share report.");
		}
		LOGGER.info("End of addReportToEmpAction");
		return "assignedReportToEmployee";
	}
	*
	*/
	public String getListOfEmployees() throws IOException, ClassNotFoundException, SQLException, JSONException
	{
		HttpServletResponse response=ServletActionContext.getResponse();
		HttpServletRequest request=ServletActionContext.getRequest();
		response.setContentType("application/json");
		PrintWriter writer=response.getWriter();
		String departmentName=request.getParameter("deptName");
		CompanyHelper helperClass=new CompanyHelper();
		List<String> empList=helperClass.getEmployeeList(departmentName);
		JSONUtil.serialize(writer,empList);
		return null;
	}
	public String getDepartmentsList()
	{
		HttpServletResponse response=ServletActionContext.getResponse();
		PrintWriter writer=null;
		Map<Integer,String> deptMap=new HashMap<>();
		CompanyHelper helperClass=new CompanyHelper();
		try {
			response.setContentType("application/json");
			writer=response.getWriter();
			/*System.out.println(Arrays.asList(helperClass.getDepartmentList()));*/
			deptMap=helperClass.getDepartmentList();
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("deptList",deptMap);
			JSONUtil.serialize(writer,deptMap);
			/*writer.println(jsonObject);*/
			/*JSONUtil.serialize(writer,helperClass.getDepartmentList());*/
		} catch (Exception exception) {
			System.out.println("Exception 1 occured");
			exception.printStackTrace();
		}return null;
	}
}
