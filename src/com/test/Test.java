package com.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.JSONUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONML;
import org.json.JSONObject;

public class Test {

	Employee employee;
	Employee employee1;
	JSONObject jsonObject;
	List<Employee> employeeList;
	HttpSession session;
	HttpServletResponse response;
	PrintWriter writer;
	
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public Employee getEmployee1() {
		return employee1;
	}
	public void setEmployee1(Employee employee1) {
		this.employee1 = employee1;
	}
	public String getEmployeeDetails()
	{
		session= ServletActionContext.getRequest().getSession();
		String SUCCESS="success";
		employeeList=new ArrayList<Employee>();
		employee=Employee.getEmployee();
		employee1=Employee.getEmployee();
		employee.setName("Pratyush");
		employee.setAge(22);
		employee.setSalary(10000);
		employee1.setName("Ranjan");
		employee1.setAge(25);
		employee1.setSalary(20000);
		employeeList.add(employee);
		employeeList.add(employee1);
		//session.setAttribute("emp",employee);
		return SUCCESS;
	}
	public String getEmployeeJsonData() throws JSONException, IOException
	{
		String SUCCESS="successJson";
		response = ServletActionContext.getResponse();
		writer=response.getWriter();
		//response.setContentType("application/json");
		employee=Employee.getEmployee();
		employee.setName("Pratyush");
		employee.setAge(22);
		employee.setSalary(10000);
		/*jsonObject=new JSONObject();
		jsonObject.put("emp",employee);*/
		//writer.println(jsonObject);
		return SUCCESS;
	}
	public String getSamePageJsonData() throws IOException, JSONException, org.apache.struts2.json.JSONException
	{
		
		response= ServletActionContext.getResponse();
		writer=response.getWriter();
		response.setContentType("application/json");
		employee=Employee.getEmployee();
		employee.setName("Ram");
		employee.setAge(26);
		employee.setSalary(15000);
		Employee[] empArray=new Employee[1];
		empArray[0]=employee;
		JSONArray jsonArray=new JSONArray();
		jsonArray.put(employee);
		JSONUtil.serialize(writer, employee);
		return null;
	}
	
public List<Employee> getEmployeeList() {
		return employeeList;
	}
	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}
	public String  getDataInTreeFormat() {
		try {
			HttpServletRequest request=ServletActionContext.getRequest();
			HttpServletResponse response = ServletActionContext.getResponse();
			PrintWriter writer=response.getWriter();
			response.setContentType("application/json");
			JSONObject jsonObject=new JSONObject();
			JSONArray jsonArray =new JSONArray();
			jsonObject.put("one","one");
			jsonObject.put("two","two");
			jsonObject.put("three","three");
			jsonArray.put(jsonObject);
			writer.print(jsonArray);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public String  newJsTree() {
		try {
			HttpServletRequest request=ServletActionContext.getRequest();
			HttpServletResponse response = ServletActionContext.getResponse();
			PrintWriter writer=response.getWriter();
			response.setContentType("application/json");
			JSONObject jsonObject=new JSONObject();
			JSONArray jsonArray =new JSONArray();
			jsonObject.put("one","one");
			jsonObject.put("two","two");
			jsonObject.put("three","three");
			jsonArray.put(jsonObject);
			writer.print(jsonArray);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
